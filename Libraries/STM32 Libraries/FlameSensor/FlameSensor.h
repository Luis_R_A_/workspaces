
/*
      Input - PA4 - TIM14 CH1

*/
#ifndef FlameSensor_h
#define FlameSensor_h


#include "stm32f0xx.h"


void FlameSensor_Init(void);
uint32_t FlameSensor_GetPulse(void);

void TIM14_IRQHandler(void);


extern volatile uint32_t FlameSensor_Pulse, FlameSensor_FirstValue, FlameSensor_SecondValue;
extern volatile uint8_t FlameSensor_Busy;
#endif