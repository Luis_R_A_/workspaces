

#include "FlameSensor.h"

volatile uint32_t FlameSensor_Pulse, FlameSensor_FirstValue, FlameSensor_SecondValue;
volatile uint8_t FlameSensor_Busy;
void FlameSensor_Init(){
      TIM_ICInitTypeDef  TIM_ICInitStructure;
      TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
      GPIO_InitTypeDef GPIO_InitStructure;
      NVIC_InitTypeDef NVIC_InitStructure;
      
      /* TIM2 clock enable */
      RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);
      
      /* GPIOB clock enable */
      RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
      
      /* TIM2 channel 2 pin (PA1) configuration */
      GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_4;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_Init(GPIOA, &GPIO_InitStructure);
      
      /* Connect PA1 pin to AF2 (TIM2 Ch2) */
      GPIO_PinAFConfig(GPIOA, GPIO_PinSource4, GPIO_AF_4);
      
      /* TIM2 configuration: Input Capture mode ---------------------
      The external signal is connected to TIM2 CH2 pin (PA.06)  
      The Rising edge is used as active edge,
      The TIM2 CCR2 is used to compute the frequency value 
      ------------------------------------------------------------ */
      
      /* Time Base configuration */
      TIM_TimeBaseStructure.TIM_Prescaler = (48*5)-1;
      TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
      TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
      TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
      TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
      
      TIM_TimeBaseInit(TIM14, &TIM_TimeBaseStructure);
      
      TIM_ICInitStructure.TIM_Channel = TIM_Channel_1;
      TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_BothEdge;
      TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
      TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
      TIM_ICInitStructure.TIM_ICFilter = 0x0;
      
      TIM_ICInit(TIM14, &TIM_ICInitStructure);
      
      /* TIM enable counter */
      //TIM_Cmd(TIM15, ENABLE); 
      
      /* Enable the CC2 Interrupt Request */
      TIM_ITConfig(TIM14, TIM_IT_CC1, ENABLE);
      
      /* Enable the TIM2 global Interrupt */
      NVIC_InitStructure.NVIC_IRQChannel = TIM14_IRQn;
      NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
      NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
      NVIC_Init(&NVIC_InitStructure);
}

uint32_t FlameSensor_GetPulse(){
   
      while(FlameSensor_Busy);
      
      TIM_SetCounter(TIM14,0);
      FlameSensor_FirstValue=0;
      FlameSensor_Busy = 1;
      TIM_Cmd(TIM14, ENABLE);
      
      while(FlameSensor_Busy);
      
      return FlameSensor_Pulse;
}

void TIM14_IRQHandler(void)
{ 
      if(TIM_GetITStatus(TIM14, TIM_IT_CC1) == SET) 
      {
            /* Clear TIM1 Capture compare interrupt pending bit */
            TIM_ClearITPendingBit(TIM14, TIM_IT_CC1);
            if(FlameSensor_FirstValue == 0 && GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_4) )
            {
                  /* Get the Input Capture value */
                  FlameSensor_FirstValue = TIM_GetCapture1(TIM14);
            }
            else if(FlameSensor_FirstValue != 0)
            {
                  
                  /* Get the Input Capture value */
                  FlameSensor_SecondValue = TIM_GetCapture1(TIM14);
                  FlameSensor_Pulse = (FlameSensor_SecondValue-FlameSensor_FirstValue); 
                  TIM_Cmd(TIM14, DISABLE);
                  FlameSensor_Busy=0;
            }
      }
}