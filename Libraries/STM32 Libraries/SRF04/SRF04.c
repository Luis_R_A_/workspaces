

#include "SRF04.h"

volatile uint32_t FirstValue[SRF04_Numb], SecondValue[SRF04_Numb], PulseMicros[SRF04_Numb];
volatile uint8_t Busy[SRF04_Numb];
volatile uint8_t NewReading[SRF04_Numb];


/**
* @brief  Configure the TIM2 Pins.
* @param  None
* @retval None
*/
void SRF04_Echo1_Init(void)
{
      TIM_ICInitTypeDef  TIM_ICInitStructure;
      TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
      GPIO_InitTypeDef GPIO_InitStructure;
      NVIC_InitTypeDef NVIC_InitStructure;
      
      /* TIM2 clock enable */
      RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM15, ENABLE);
      
      /* GPIOA clock enable */
      RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
      
      /* TIM2 channel 2 pin (PA1) configuration */
      GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_15;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_Init(GPIOB, &GPIO_InitStructure);
      
      /* Connect PA1 pin to AF2 (TIM2 Ch2) */
      GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_1);
      
      /* TIM2 configuration: Input Capture mode ---------------------
      The external signal is connected to TIM2 CH2 pin (PA.06)  
      The Rising edge is used as active edge,
      The TIM2 CCR2 is used to compute the frequency value 
      ------------------------------------------------------------ */
      
      /* Time Base configuration */
      TIM_TimeBaseStructure.TIM_Prescaler = 48-1;
      TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
      TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
      TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
      TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
      
      TIM_TimeBaseInit(TIM15, &TIM_TimeBaseStructure);
      
      TIM_ICInitStructure.TIM_Channel = TIM_Channel_2;
      TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_BothEdge;
      TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
      TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
      TIM_ICInitStructure.TIM_ICFilter = 0x0;
      
      TIM_ICInit(TIM15, &TIM_ICInitStructure);
      
      /* TIM enable counter */
      //TIM_Cmd(TIM14, ENABLE); no need for now.
      
      /* Enable the CC2 Interrupt Request */
      TIM_ITConfig(TIM15, TIM_IT_CC2, ENABLE);
      
      /* Enable the TIM2 global Interrupt */
      NVIC_InitStructure.NVIC_IRQChannel = TIM15_IRQn;
      NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
      NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
      NVIC_Init(&NVIC_InitStructure);
      
}

/**
* @brief  Configure the TIM2 Pins.
* @param  None
* @retval None
*/
void SRF04_Echo2_Init(void)
{
      TIM_ICInitTypeDef  TIM_ICInitStructure;
      TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
      GPIO_InitTypeDef GPIO_InitStructure;
      NVIC_InitTypeDef NVIC_InitStructure;
      
      /* TIM2 clock enable */
      RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM17, ENABLE);
      
      /* GPIOA clock enable */
      RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
      
      /* TIM2 channel 2 pin (PA1) configuration */
      GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_Init(GPIOB, &GPIO_InitStructure);
      
      /* Connect PA1 pin to AF2 (TIM2 Ch2) */
      GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_2);
      
      /* TIM2 configuration: Input Capture mode ---------------------
      The external signal is connected to TIM2 CH2 pin (PA.06)  
      The Rising edge is used as active edge,
      The TIM2 CCR2 is used to compute the frequency value 
      ------------------------------------------------------------ */
      
      /* Time Base configuration */
      TIM_TimeBaseStructure.TIM_Prescaler = 48-1;
      TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
      TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
      TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
      TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
      
      TIM_TimeBaseInit(TIM17, &TIM_TimeBaseStructure);
      
      TIM_ICInitStructure.TIM_Channel = TIM_Channel_1;
      TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_BothEdge;
      TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
      TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
      TIM_ICInitStructure.TIM_ICFilter = 0x0;
      
      TIM_ICInit(TIM17, &TIM_ICInitStructure);
      
      /* TIM enable counter */
      //TIM_Cmd(TIM17, ENABLE); no need for now.
      
      /* Enable the CC2 Interrupt Request */
      TIM_ITConfig(TIM17, TIM_IT_CC1, ENABLE);
      
      /* Enable the TIM2 global Interrupt */
      NVIC_InitStructure.NVIC_IRQChannel = TIM17_IRQn;
      NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
      NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
      NVIC_Init(&NVIC_InitStructure);
      
}



void SRF04_Trigger_Init(){
      
      TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
      TIM_OCInitTypeDef  TIM_OCInitStructure;
      GPIO_InitTypeDef GPIO_InitStructure;
      EXTI_InitTypeDef EXTI_InitStructure;
       NVIC_InitTypeDef NVIC_InitStructure;
      
      /* GPIOA clock enable */
      RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
      
      /* TIM3 channel 1 pin (PA6) configuration */
      GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_Init(GPIOA, &GPIO_InitStructure);
      
      /* Connect PA6 pin to AF1 */
      GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_1);
 
      /* Setup EXTI line 6 interrupt (PA6 interrupt) at rising edge*/
      EXTI_InitStructure.EXTI_Line=EXTI_Line6;
      EXTI_InitStructure.EXTI_LineCmd=ENABLE;
      EXTI_InitStructure.EXTI_Mode=EXTI_Mode_Interrupt;
      EXTI_InitStructure.EXTI_Trigger=EXTI_Trigger_Rising;
      EXTI_Init(&EXTI_InitStructure);
      
      /* Enable the EXTI line 6 global Interrupt */
      NVIC_InitStructure.NVIC_IRQChannel = EXTI4_15_IRQn;
      NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
      NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
      NVIC_Init(&NVIC_InitStructure);
      
      /*
      =========================================================
      Set TIM3 with 20Hz frequency (50ms intervals)
      
      Remember: Timer 3 is in APB1, use RCC_APB1PeriphClockCmd
      note that the function is diferent 
      
      Timer 3 also doesn't have complementary output (OCN) so since the same
      struct is being use, remember to always set to disable the complementary output 
      to avoid problems
      =========================================================
      */      
      /* Divide by 50 because the counter is just 16bits.*/
      uint32_t Prescaler = 50;
      /* Get the 20Hz frequency (after dividing the system clock by the prescaler)*/
      uint32_t TimerPeriod = SystemCoreClock/Prescaler/20;
      /* This is for the 10us positive pulse */
      uint32_t Pulse =  (uint32_t)(TimerPeriod*0.0002); 
      /* TIM3 clock enable */
      RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 , ENABLE);
      
      /* Time Base configuration */
      TIM_TimeBaseStructure.TIM_Prescaler = Prescaler;
      TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
      TIM_TimeBaseStructure.TIM_Period = TimerPeriod;
      TIM_TimeBaseStructure.TIM_ClockDivision = 0;
      TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
      
      TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
      
      
      /* Channel 1 Configuration in PWM mode */
      TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
      TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
      TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
      TIM_OCInitStructure.TIM_Pulse = Pulse; 
      TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
      TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
      TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
      TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset;
      
      
      /*Configure Channel 1 */
      TIM_OC1Init(TIM3, &TIM_OCInitStructure);
      
      /* TIM3 counter enable */
      TIM_Cmd(TIM3, ENABLE);
      
      /* TIM3 Main Output Enable */
      TIM_CtrlPWMOutputs(TIM3, ENABLE);   
}

uint32_t GetMeasure(uint8_t _number){
      
      if(_number > SRF04_Numb || _number == 0)
            return 0;
      _number--;
      
      if(NewReading[_number]==0)
            return 0;
      
      return PulseMicros[_number];
}



/**
* @brief  This function handles TIM2 Capture Compare interrupt request.
* @param  None
* @retval None
*
*     This will allow to compute a positive pulse legth
*/
void TIM15_IRQHandler(void)
{ 
      if(TIM_GetITStatus(TIM15, TIM_IT_CC2) == SET) 
      {
            /* Clear TIM1 Capture compare interrupt pending bit */
            TIM_ClearITPendingBit(TIM15, TIM_IT_CC1);
            if(FirstValue[0] == 0 && GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_15) )
            {
                  /* Get the Input Capture value */
                  FirstValue[0] = TIM_GetCapture2(TIM15);
            }
            else if(FirstValue[0] != 0)
            {
                  
                  /* Get the Input Capture value */
                  SecondValue[0] = TIM_GetCapture2(TIM15);
                  PulseMicros[0] = (SecondValue[0]-FirstValue[0]); 
                  TIM_Cmd(TIM15, DISABLE);
                  Busy[0]=0;
            }
      }
}

void TIM17_IRQHandler(void)
{ 
      if(TIM_GetITStatus(TIM17, TIM_IT_CC1) == SET) 
      {
            /* Clear TIM1 Capture compare interrupt pending bit */
            TIM_ClearITPendingBit(TIM17, TIM_IT_CC1);
            if(FirstValue[1] == 0 && GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_9) )
            {
                  /* Get the Input Capture value */
                  FirstValue[1] = TIM_GetCapture1(TIM17);
                  
            }
            else if(FirstValue[1] != 0)
            {
                  
                  /* Get the Input Capture value */
                  SecondValue[1] = TIM_GetCapture1(TIM17);
                  PulseMicros[1] = (SecondValue[1]-FirstValue[1]); 
                  TIM_Cmd(TIM17, DISABLE);
                  Busy[1]=0;
            }
      }
}


/**
* @brief  This function handles EXTI 4 to 15 Capture Compare interrupt request.
* @param  None
* @retval None
*
*     This will request a new pulse measure each time a PWM rising edge occurs
*  (that's the beggining of the cycle.
*/
void EXTI4_15_IRQHandler(){
      uint32_t flags = EXTI->PR;
      EXTI_ClearITPendingBit(EXTI_Line6);
      
      if(flags & EXTI_Line6){
            TIM_SetCounter(TIM15,0);
            TIM_SetCounter(TIM17,0);
            
            FirstValue[0] = 0;
            PulseMicros[0] = 0;
            NewReading[0] = 1;
 
            FirstValue[1] = 0;
            PulseMicros[1] = 0;
            NewReading[1] = 1;
            
            TIM_Cmd(TIM15, ENABLE);
            TIM_Cmd(TIM17, ENABLE);
   
      }
                 
}
