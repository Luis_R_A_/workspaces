
/*

      Echo1 - PB15 - TIM15 CH2
      Echo2 - PB9 - TIM17 CH1

      Trigger - PA6 - TIM3 CH1

*/


#ifndef SRF04_h
#define SRF04_h
#include "stm32f0xx.h"

#define SRF04_Numb 2

void SRF04_Echo1_Init(void);
void SRF04_Echo2_Init(void);
void SRF04_Trigger_Init(void);
uint32_t GetMeasure(uint8_t _number);

void TIM15_IRQHandler(void);
void TIM17_IRQHandler(void);
void EXTI4_15_IRQHandler(void);

extern volatile uint32_t FirstValue[SRF04_Numb], SecondValue[SRF04_Numb], PulseMicros[SRF04_Numb];
extern volatile uint8_t Busy[SRF04_Numb];
extern volatile uint8_t NewReading[SRF04_Numb];
#endif