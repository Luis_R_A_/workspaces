
#include "TCS3200.h"

#define S_Pins_Periph RCC_AHBPeriph_GPIOC
#define S_Pins_GPIO GPIOC
#define Pin_S2 GPIO_Pin_2
#define Pin_S3 GPIO_Pin_3


volatile uint32_t Color_Pulse, Color_FirstValue, Color_SecondValue;
volatile uint8_t Color_Busy;
uint32_t RGBWraw[4], RGBW[4];

/*
**
* @brief  Configure the TIM2 Pins.
* @param  None
* @retval None
*/
void TCS3200_Init(void)
{
      TIM_ICInitTypeDef  TIM_ICInitStructure;
      TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
      GPIO_InitTypeDef GPIO_InitStructure;
      NVIC_InitTypeDef NVIC_InitStructure;
      
      /* TIM2 clock enable */
      RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM16, ENABLE);
      
      /* GPIOB clock enable */
      RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
      
      /* TIM2 channel 2 pin (PA1) configuration */
      GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_8;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_Init(GPIOB, &GPIO_InitStructure);
      
      /* Connect PA1 pin to AF2 (TIM2 Ch2) */
      GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_2);
      
      /* TIM2 configuration: Input Capture mode ---------------------
      The external signal is connected to TIM2 CH2 pin (PA.06)  
      The Rising edge is used as active edge,
      The TIM2 CCR2 is used to compute the frequency value 
      ------------------------------------------------------------ */
      
      /* Time Base configuration */
      TIM_TimeBaseStructure.TIM_Prescaler = 0;
      TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
      TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
      TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
      TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
      
      TIM_TimeBaseInit(TIM16, &TIM_TimeBaseStructure);
      
      TIM_ICInitStructure.TIM_Channel = TIM_Channel_1;
      TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_BothEdge;
      TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
      TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
      TIM_ICInitStructure.TIM_ICFilter = 0x0;
      
      TIM_ICInit(TIM16, &TIM_ICInitStructure);
      
      /* TIM enable counter */
      //TIM_Cmd(TIM15, ENABLE); 
      
      /* Enable the CC2 Interrupt Request */
      TIM_ITConfig(TIM16, TIM_IT_CC1, ENABLE);
      
      /* Enable the TIM2 global Interrupt */
      NVIC_InitStructure.NVIC_IRQChannel = TIM16_IRQn;
      NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
      NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
      NVIC_Init(&NVIC_InitStructure);
      
      
      
      
      /* GPIOC clock enable */
      RCC_AHBPeriphClockCmd(S_Pins_Periph, ENABLE);
     
      GPIO_InitStructure.GPIO_Pin =  Pin_S2|Pin_S3;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
      GPIO_Init(S_Pins_GPIO, &GPIO_InitStructure); 
}

void TCS3200_ColorReading(uint32_t _M[4]){
      GPIO_WriteBit(S_Pins_GPIO,(Pin_S2)|(Pin_S3),Bit_RESET);
     // for(int i = 0; i < 100; i++);
	RGBWraw[0]  = TCS3200_GetPulse();

      GPIO_WriteBit(S_Pins_GPIO,(Pin_S2),Bit_RESET);
      GPIO_WriteBit(S_Pins_GPIO,(Pin_S3),Bit_SET);
     // for(int i = 0; i < 100; i++);
	RGBWraw[2] = TCS3200_GetPulse();

      GPIO_WriteBit(S_Pins_GPIO,(Pin_S2),Bit_SET);
      GPIO_WriteBit(S_Pins_GPIO,(Pin_S3),Bit_SET);
      //for(int i = 0; i < 100; i++);
	RGBWraw[1] = TCS3200_GetPulse();
      
      GPIO_WriteBit(S_Pins_GPIO,(Pin_S2),Bit_SET);
      GPIO_WriteBit(S_Pins_GPIO,(Pin_S3),Bit_RESET);
     // for(int i = 0; i < 100; i++);
	RGBWraw[3]  = TCS3200_GetPulse();

	int i;
	for(i=0; i <4; i++){
		RGBW[i]=RGBWraw[i];
	}
	ColorMath_ConvertRGB(RGBW,1);

	for(int i=0; i <4; i++){
		_M[i]=RGBW[i];
	}

}
void TCS3200_Calibrate(){

	TCS3200_ColorReading(RGBW);
      
      /*RGBWraw[0] = 2248;
       RGBWraw[1] = 2475;
        RGBWraw[2] = 838;
         RGBWraw[3] = 1925;*/
	ColorMath_Calibrate(RGBWraw);

	/*int i;
	for(i=0; i<4 ;i++)
		RGBWwhite[i] = RGBWraw[i];
*/


}


uint32_t TCS3200_GetPulse(){
   
      while(Color_Busy);
      
      TIM_SetCounter(TIM16,0);
      Color_FirstValue=0;
      Color_Busy = 1;
      TIM_Cmd(TIM16, ENABLE);
      
      while(Color_Busy);
      
      return Color_Pulse;
}

bool TCS3200_StartColorReading(uint32_t Color){

	if(Color_Busy)
		return 1;

	if(Color == RED_Color)
		GPIO_WriteBit(S_Pins_GPIO,(Pin_S2)|(Pin_S3),Bit_RESET);
	else if(Color == GREEN_Color){
		      GPIO_WriteBit(S_Pins_GPIO,(Pin_S2),Bit_SET);
      GPIO_WriteBit(S_Pins_GPIO,(Pin_S3),Bit_SET);
      }
	else if(Color == BLUE_Color){
		      GPIO_WriteBit(S_Pins_GPIO,(Pin_S2),Bit_RESET);
      GPIO_WriteBit(S_Pins_GPIO,(Pin_S3),Bit_SET);
      }
	else if(Color == WHITE_Color){
		      GPIO_WriteBit(S_Pins_GPIO,(Pin_S2),Bit_SET);
      GPIO_WriteBit(S_Pins_GPIO,(Pin_S3),Bit_RESET);
      }

	TIM_SetCounter(TIM16,0);
      Color_FirstValue=0;
      Color_Busy = 1;
      TIM_Cmd(TIM16, ENABLE);

	return 0;

}

uint32_t TCS3200_GetColorReading(uint32_t Color){
	if(Color_Busy)
		return 0;

		RGBWraw[Color]=Color_Pulse;
		RGBW[Color]=RGBWraw[Color];
            ColorMath_ConvertRGB(RGBW,1);
		if(RGBW[Color]>255)
			RGBW[Color] = 255;


	 return RGBW[Color];
}


void TIM16_IRQHandler(void)
{ 
      if(TIM_GetITStatus(TIM16, TIM_IT_CC1) == SET) 
      {
            /* Clear TIM1 Capture compare interrupt pending bit */
            TIM_ClearITPendingBit(TIM16, TIM_IT_CC1);
            if(Color_FirstValue == 0 && GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_8) )
            {
                  /* Get the Input Capture value */
                  Color_FirstValue = TIM_GetCapture1(TIM16);
            }
            else if(Color_FirstValue != 0)
            {
                  
                  /* Get the Input Capture value */
                  Color_SecondValue = TIM_GetCapture1(TIM16);
                  Color_Pulse = (Color_SecondValue-Color_FirstValue); 
                  TIM_Cmd(TIM16, DISABLE);
                  Color_Busy=0;
            }
      }
}