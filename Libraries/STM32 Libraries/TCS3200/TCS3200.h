/*

      Color Output - PB8 - TIM16 CH1
      S0 - 5V
      S1 - GND
      S2 - PC2
      S3 - PC3

*/
#ifndef TCS3200_h
#define TCS3200_h

#define RED_Color 0
#define GREEN_Color 1
#define BLUE_Color 2
#define WHITE_Color 3

#include "stm32f0xx.h"
#include "ColorMath.h"
void TIM15_IRQHandler(void);
void TCS3200_Init(void);
void TCS3200_ColorReading(uint32_t _M[4]);
void TCS3200_Calibrate(void);
uint32_t TCS3200_GetPulse(void);
bool TCS3200_StartColorReading(uint32_t Color);
uint32_t TCS3200_GetColorReading(uint32_t Color);

void TIM16_IRQHandler(void);

extern volatile uint32_t Color_Pulse, Color_FirstValue, Color_SecondValue;
extern volatile uint8_t Color_Busy;

extern uint32_t RGBWraw[4];

#endif