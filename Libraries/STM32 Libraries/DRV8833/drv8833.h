/*
 * drv8833.h
 *
 * TIM1 : CH1, CH11N, CH2, CH2N
 * AIN1 - PA8 -castanho
 * AIN2 - PA7 -cinzento
 * BIN1 - PA9 -branco
 * BIN2 - PB0 -laranja
 * 
 *  Created on: 09/06/2015
 *      Author: Lu�s Afonso
 */

#ifndef DRV8833_H_
#define DRV8833_H_

#define DRV8833_GPIO_Periph RCC_AHBPeriph_GPIOA
#define DRV8833_GPIO GPIOA
#define DRV8833_AIN1 8
#define DRV8833_AIN2 7
#define DRV8833_BIN1 9
#define DRV8833_BIN2 0
#define DRV8833_AIN1_Source GPIO_PinSource8
#define DRV8833_AIN2_Source GPIO_PinSource7
#define DRV8833_BIN1_Source GPIO_PinSource9
#define DRV8833_BIN2_Source GPIO_PinSource0
#define DRV8833_AIN1_Pin GPIO_Pin_8
#define DRV8833_AIN2_Pin GPIO_Pin_7
#define DRV8833_BIN1_Pin GPIO_Pin_9
#define DRV8833_BIN2_Pin GPIO_Pin_0 //(GPIOB)

#include "stm32f0xx.h"
#include <stdint.h>
#include <stdbool.h>

void DRV8833_InitMotorA(void); 
void DRV8833_InitMotorB(void);
void DRV8833_SetDigital(uint32_t _pin, bool _b);
void DRV8833_SetPWM(uint32_t _pin, uint32_t _duty);

extern void DRV8833_InitMotorsAB(void);
extern void DRV8833_MotorA(int32_t _speed);
extern void DRV8833_MotorB(int32_t _speed);
extern void DRV8833_MotorsDuty(int32_t _speedA, int32_t speedB);

#endif /* DRV8833_H_ */
