/*
* drv8833.c
*
*  Created on: 09/06/2015
*      Author: Lu�s Afonso
*/


#include "drv8833.h"



/*
* Add definitions for the Timers/PWM modules/GPIO if needed
*/
/*================================================
Start of user drivers
================================================*/
const uint32_t PWM_freq = (48000000 / 24000)-1; // 3200 values.


void DRV8833_InitMotorA(){
	
	/*
      * Initalize 2 PWM outputs
      */
      TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
      TIM_OCInitTypeDef  TIM_OCInitStructure;
      GPIO_InitTypeDef GPIO_InitStructure;
      
      /* GPIO Clock enable */
      RCC_AHBPeriphClockCmd( DRV8833_GPIO_Periph, ENABLE);
      
      /* GPIOA Configuration: Channel 3 as alternate function push-pull */
      GPIO_InitStructure.GPIO_Pin =  DRV8833_AIN1_Pin|DRV8833_AIN2_Pin;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
      GPIO_Init(DRV8833_GPIO, &GPIO_InitStructure);
      
      
      GPIO_PinAFConfig(DRV8833_GPIO, DRV8833_AIN1_Source, GPIO_AF_2);	
      GPIO_PinAFConfig(DRV8833_GPIO, DRV8833_AIN2_Source, GPIO_AF_2);
      
      
      uint32_t TimerPeriod = PWM_freq-1;
      /* TIM1 clock enable */
      RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 , ENABLE);
      
      /* Time Base configuration */
      TIM_TimeBaseStructure.TIM_Prescaler = 0;
      TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
      TIM_TimeBaseStructure.TIM_Period = TimerPeriod;
      TIM_TimeBaseStructure.TIM_ClockDivision = 0;
      TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
      
      TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);
      
      /* Channel 1, 2,3 and 4 Configuration in PWM mode */
      TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
      TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
      TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
      TIM_OCInitStructure.TIM_Pulse = 1;
      TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
      TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCPolarity_Low;
      TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
      TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Set;
      
      TIM_OC1Init(TIM1, &TIM_OCInitStructure);
     // TIM_OC2Init(TIM1, &TIM_OCInitStructure);
      
      /* TIM1 counter enable */
      TIM_Cmd(TIM1, ENABLE);
      
      /* TIM1 Main Output Enable */
      TIM_CtrlPWMOutputs(TIM1, ENABLE);
}

void DRV8833_InitMotorB(){
	/*
      * Initialize 2 PWM outputs
      */
	/*
      * Initalize 2 PWM outputs
      */
      TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
      TIM_OCInitTypeDef  TIM_OCInitStructure;
      GPIO_InitTypeDef GPIO_InitStructure;
      
      /* GPIO Clock enable */
      RCC_AHBPeriphClockCmd( DRV8833_GPIO_Periph, ENABLE);
      
      /* GPIOA Configuration: Channel 3 as alternate function push-pull */
      GPIO_InitStructure.GPIO_Pin =  DRV8833_BIN1_Pin;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
      GPIO_Init(DRV8833_GPIO, &GPIO_InitStructure);
      
         /* GPIO Clock enable */
      RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE);
      
      /* GPIOA Configuration: Channel 3 as alternate function push-pull */
      GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
      GPIO_Init(GPIOB, &GPIO_InitStructure);
      
      GPIO_PinAFConfig(DRV8833_GPIO, DRV8833_BIN1_Source, GPIO_AF_2);	
      GPIO_PinAFConfig(GPIOB, GPIO_PinSource0, GPIO_AF_2);
      
      
      uint32_t TimerPeriod = PWM_freq-1;
      /* TIM1 clock enable */
      RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 , ENABLE);
      
      /* Time Base configuration */
      TIM_TimeBaseStructure.TIM_Prescaler = 0;
      TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
      TIM_TimeBaseStructure.TIM_Period = TimerPeriod;
      TIM_TimeBaseStructure.TIM_ClockDivision = 0;
      TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
      
      TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);
      
      /* Channel 1, 2,3 and 4 Configuration in PWM mode */
      TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
      TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
      TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
      TIM_OCInitStructure.TIM_Pulse = 1;
      TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
      TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCPolarity_Low;
      TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
      TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Set;
      
      TIM_OC2Init(TIM1, &TIM_OCInitStructure);

      
      /* TIM1 counter enable */
      TIM_Cmd(TIM1, ENABLE);
      
      /* TIM1 Main Output Enable */
      TIM_CtrlPWMOutputs(TIM1, ENABLE);
}

void DRV8833_SetDigital(uint32_t _pin, bool _b){
     
      //TIM_CtrlPWMOutputs(TIM1, DISABLE);
      if(_pin == DRV8833_AIN1){
            TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC1E));    
            TIM1->CCER |= TIM_OutputState_Disable;
      }
      if(_pin == DRV8833_AIN2){
            TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC1NE));    
            TIM1->CCER |= TIM_OutputNState_Disable;
      }
      if(_pin == DRV8833_BIN1){
           TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC2E));    
            TIM1->CCER |= (TIM_OutputState_Disable<< 4);
      }
      if(_pin == DRV8833_BIN2){
            TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC2NE));    
            TIM1->CCER |= (TIM_OutputNState_Disable<< 4);
      }
    //  TIM_CtrlPWMOutputs(TIM1, ENABLE);
	
}
void DRV8833_SetPWM(uint32_t _pin, uint32_t _duty){
      
     // TIM_CtrlPWMOutputs(TIM1, DISABLE);
      
      if(_pin == DRV8833_AIN1){
            TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC1E));    
            TIM1->CCER |= TIM_OutputState_Enable;
            TIM_SetCompare1(TIM1,_duty);
      }
      else if(_pin == DRV8833_AIN2){
            TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC1NE));    
            TIM1->CCER |= TIM_OutputNState_Enable;
            TIM_SetCompare1(TIM1,PWM_freq-_duty);
      }
      else if(_pin == DRV8833_BIN1)  {
            TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC2E));    
            TIM1->CCER |= (TIM_OutputState_Enable<< 4);
            TIM_SetCompare2(TIM1,_duty);
      }
      else if(_pin == DRV8833_BIN2){
            TIM1->CCER &= (uint16_t)(~((uint16_t)TIM_CCER_CC2NE));    
            TIM1->CCER |= (TIM_OutputNState_Enable<< 4);
            TIM_SetCompare2(TIM1,PWM_freq-_duty);
      }

	  //TIM_CtrlPWMOutputs(TIM1, ENABLE);
}

/*================================================
End of user drivers
================================================*/



void DRV8833_InitMotorsAB(){
	DRV8833_InitMotorA();
	DRV8833_InitMotorB();
}



/*
* Controls Motor1 speed and direction.
*
* Duty goes from 0 to 1024
* The bigger the Inverseduty value, the faster the speed.
* Remember, bigger match value, lower duty. Since in fast decay the digital pin
* is at 1, the lower the duty, the faster the speed. Hence why Inverseduty is used.
* (no need to make freq-Inverseduty since less is faster).
*
* Possibily I have this wrong as I don't remember if the pulse starts high or low.
*
*/
void DRV8833_MotorA(int32_t _speed){
      
	if(_speed ==0){
		DRV8833_SetDigital(DRV8833_AIN1,0);
		DRV8833_SetDigital(DRV8833_AIN2,0);
		return;
	}
      
	uint8_t backwards=0;
	if(_speed < 0){
		_speed = _speed*(-1);
		backwards=1;
	}
	if(_speed > 1023)
		_speed = 1023;
      
      
      
      
	uint32_t duty = _speed*PWM_freq/1023;
      
	if(!backwards){
		DRV8833_SetDigital(DRV8833_AIN1,1);
		DRV8833_SetPWM(DRV8833_AIN2,duty);
	}
	else{
		DRV8833_SetDigital(DRV8833_AIN2,1);
            DRV8833_SetPWM(DRV8833_AIN1,duty);
		
	}
}

void DRV8833_MotorB(int32_t _speed){
      
	if(_speed ==0){
		
		DRV8833_SetDigital(DRV8833_BIN1,0);
		DRV8833_SetDigital(DRV8833_BIN2,0);
		return;
	}
      
	uint8_t backwards=0;
	if(_speed < 0){
		_speed = _speed*(-1);
		backwards=1;
	}
	if(_speed > 1023)
		_speed = 1023;
      
      
      
      
	uint32_t duty = _speed*PWM_freq/1024;
      
	if(!backwards){
		DRV8833_SetDigital(DRV8833_BIN1,1);
		if(_speed == 1024){
			DRV8833_SetDigital(DRV8833_BIN2,1);
		}
		else{
			DRV8833_SetPWM(DRV8833_BIN2,duty);
		}
	}
	else{
		DRV8833_SetDigital(DRV8833_BIN2,1);
		if(_speed == 1024){
			DRV8833_SetDigital(DRV8833_BIN1,1);
		}
		else{
			DRV8833_SetPWM(DRV8833_BIN1,duty);
		}
	}
      
}


void DRV8833_MotorsDuty(int32_t _speedA, int32_t _speedB){
	DRV8833_MotorA(_speedA);
	DRV8833_MotorB(_speedB);
}

