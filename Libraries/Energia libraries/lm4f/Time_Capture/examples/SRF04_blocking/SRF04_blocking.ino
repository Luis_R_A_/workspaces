/*
  SRF04_Blocking for Time_Capture library
  
  The example shows how to use the library with a SRF04 sensor with a block function.
  The function works much like pulseIn() but assures 1 micro-second precision by using a wide timer.
  From the time the function is called until the end of the pulse, the maximum time is about 53 seconds.
  
  Another example using a non-blocking funtion is provided in the examples
  
  Possible pins to use are:
  PC4, PC5, PC6, PC7 and from PD0 to PD7, including making a total of 12 inputs.
  Using for example PC_4 for PC4 in the library functions will not work. This was to 
 be easier to port the library to Energia


*/

#include "Time_Capture.h"

void setup()
{
  //Start the serial
  Serial.begin(9600);
  
  //Initialize the time capture in PC5
  WideTimer_Init(PC5);
  
  //Use PC_4 for the Trigger pin.
  pinMode(PC_4,OUTPUT);
}
uint32_t value;
void loop()
{
  //Start a reading by pulsing the pin for 10us
  digitalWrite(PC_4,HIGH);
  delayMicroseconds(10);
  digitalWrite(PC_4,LOW);
  
  //Get the positive pulse length
   value = WideTimer_MeasureMicros(PC5,HIGH)/58;
   
   //In case you need there is a function with timeout
   //value = WideTimer_MeasureMicros(PC5,HIGH,5000)/58;
   
  Serial.println(value);
  delay(100);
}

