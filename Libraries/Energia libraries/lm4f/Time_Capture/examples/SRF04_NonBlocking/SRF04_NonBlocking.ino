/*
  SRF04_NonBlocking for Time_Capture library
  
  The example shows how to use the library with a SRF04 sensor with a non-block function.
  The function unlike pulseIn() allows for the reading to be done in the background and assures 1 micro-second precision by using a wide timer.
  From the time the function is called until the end of the pulse, the maximum time is about 53 seconds.
  
  Another example using a blocking funtion is provided in the examples
  
  Possible pins to use are:
  PC4, PC5, PC6, PC7 and from PD0 to PD7, including making a total of 12 inputs.
  Using for example PC_4 for PC4 in the library functions will not work. This was to 
 be easier to port the library to Energia


*/

#include "Time_Capture.h"

void setup()
{
  //Start the serial
  Serial.begin(9600);
  
  //Initialize the time capture in PC5
  WideTimer_Init(PC5);
  
  //Use PC_4 for the Trigger pin.
  pinMode(PC_4,OUTPUT);
}
uint32_t value;
void loop()
{
  //Start a reading by pulsing the pin for 10us
  digitalWrite(PC_4,HIGH);
  delayMicroseconds(10);
  digitalWrite(PC_4,LOW);
  
  //Get the positive pulse length
   WideTimer_StartReading(PC5,HIGH);
   
   while(WideTimer_IsBusy(PC5)){
    //do stuff 
    //In here I have gotten readings from a TCS230/TCS3200 sensor which also has a pulse interface
    //While the reading of the SRF04 was being made, many more of the TCS3200 were made since it gave pulses
    //at higher frequencies.
    //So it's very well possible to, not only execute other pieces of code, but also get other readings in other pins while waiting for the first
    //reading to be ready.
   }
   
   //This will get the reading in microseconds. If the reading was not ready it will return 0
   //There is not timeout right now so you should check if the reading is ready.
   //There should be in the future a cancel reading function so for now carefull, it's possible to the timer to get stuck if the pulse never finishes
   value = WideTimer_GetMicros(PC5)/58;
   
  Serial.println(value);
  delay(100);
}

