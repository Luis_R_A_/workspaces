/*
 * TimerCapture.c
 *
 *	Check TimerCapture.h for more info
 *
 *  Created on: 27/05/2015
 *      Author: Lu�s Afonso
 */



#include "Time_Capture.h"

/*
 *  Private variables
 */
volatile uint32_t WidePulseValue[12];
volatile uint32_t WideFirstPulse[12];
uint32_t WideTimerPolarity[12];
volatile uint32_t WideBusy[12];
uint32_t SystemFrequency;

/*
 * Private arrays
 * These arrays are to aid configuring the timers and pins
 * They make possible, in conjuction with defines in the header to just require a pin as parameter
 * to select the timer.
 */
//Interrupts
uint32_t WideTimerInt[]={
		INT_WTIMER0A,INT_WTIMER0B,
		INT_WTIMER1A,INT_WTIMER1B,
		INT_WTIMER2A,INT_WTIMER2B,
		INT_WTIMER3A,INT_WTIMER3B,
		INT_WTIMER4A,INT_WTIMER4B,
		INT_WTIMER5A,INT_WTIMER5B
};

//Timer macros
uint32_t WideTimerPeriph[][2] = {
		{SYSCTL_PERIPH_WTIMER0,WTIMER0_BASE},
		{SYSCTL_PERIPH_WTIMER1,WTIMER1_BASE},
		{SYSCTL_PERIPH_WTIMER2,WTIMER2_BASE},
		{SYSCTL_PERIPH_WTIMER3,WTIMER3_BASE},
		{SYSCTL_PERIPH_WTIMER4,WTIMER4_BASE},
		{SYSCTL_PERIPH_WTIMER5,WTIMER5_BASE},
};

//GPIO macros
uint32_t WideTimerGPIO[][4] = {
		{SYSCTL_PERIPH_GPIOC,GPIO_PORTC_BASE,GPIO_PIN_4,GPIO_PC4_WT0CCP0},
		{SYSCTL_PERIPH_GPIOC,GPIO_PORTC_BASE,GPIO_PIN_5,GPIO_PC5_WT0CCP1},
		{SYSCTL_PERIPH_GPIOC,GPIO_PORTC_BASE,GPIO_PIN_6,GPIO_PC6_WT1CCP0},
		{SYSCTL_PERIPH_GPIOC,GPIO_PORTC_BASE,GPIO_PIN_7,GPIO_PC7_WT1CCP1},
		{SYSCTL_PERIPH_GPIOD,GPIO_PORTD_BASE,GPIO_PIN_0,GPIO_PD0_WT2CCP0},
		{SYSCTL_PERIPH_GPIOD,GPIO_PORTD_BASE,GPIO_PIN_1,GPIO_PD1_WT2CCP1},
		{SYSCTL_PERIPH_GPIOD,GPIO_PORTD_BASE,GPIO_PIN_2,GPIO_PD2_WT3CCP0},
		{SYSCTL_PERIPH_GPIOD,GPIO_PORTD_BASE,GPIO_PIN_3,GPIO_PD3_WT3CCP1},
		{SYSCTL_PERIPH_GPIOD,GPIO_PORTD_BASE,GPIO_PIN_4,GPIO_PD4_WT4CCP0},
		{SYSCTL_PERIPH_GPIOD,GPIO_PORTD_BASE,GPIO_PIN_5,GPIO_PD5_WT4CCP1},
		{SYSCTL_PERIPH_GPIOD,GPIO_PORTD_BASE,GPIO_PIN_6,GPIO_PD6_WT5CCP0},
		{SYSCTL_PERIPH_GPIOD,GPIO_PORTD_BASE,GPIO_PIN_7,GPIO_PD7_WT5CCP1},
};

void User_TimerConfigure(uint32_t _base, uint32_t _timer, uint32_t _config){
	//
	// Disable the timers.
	//
	HWREG(_base + TIMER_O_CTL) &= ~(TIMER_CTL_TAEN | TIMER_CTL_TBEN);

	//
	// Set the global timer configuration.
	//
	HWREG(_base + TIMER_O_CFG) = _config >> 24;

	/*
	 * Configure the timers
	 * Allows to configure both at the time as well only 1
	 */
	if( (_timer & 0xff) == TIMER_A)
		HWREG(_base + TIMER_O_TAMR) = ((_config & 0xff) |
				TIMER_TAMR_TAPWMIE);
	if( (_timer & 0xff00) == TIMER_B)
		HWREG(_base + TIMER_O_TBMR) = (((_config >> 8) & 0xff) |
				TIMER_TBMR_TBPWMIE);
}

/*
 * Initializes a Wide Timer half to be used in time capture mode
 *
 * Configures the corresponding pin and wide timer.
 * It also configures the interrupt but to complete the interrupt config
 * you need to had the handlers to the vector table in the startup file.
 *
 */
bool WideTimer_Init(uint32_t _pin){

	uint32_t _Mhz = 80;
	SystemFrequency = _Mhz;
	uint8_t ConvertTimer = (_pin%2==0) ? 0 : 1;

	if( !SysCtlPeripheralReady(WideTimerGPIO[ (_pin)][0]) ){
		SysCtlPeripheralEnable(WideTimerGPIO[ (_pin)][0]);
		SysCtlDelay(3);
	}
	if(_pin == 11){
		  HWREG(GPIO_PORTD_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
		  HWREG(GPIO_PORTD_BASE + GPIO_O_CR)   |= 0x81;
	}
	GPIOPinConfigure(WideTimerGPIO[_pin][3]);
	GPIOPinTypeTimer(WideTimerGPIO[_pin][1], WideTimerGPIO[_pin][2]);

	if( !SysCtlPeripheralReady(WideTimerPeriph[_pin/2][0]) ){
		SysCtlPeripheralEnable(WideTimerPeriph[_pin/2][0]);
		SysCtlDelay(3);
	}
	TimerDisable(WideTimerPeriph[_pin/2][1], (ConvertTimer==0) ? TIMER_A:TIMER_B );
	//TimerConfigure(WideTimerPeriph[_pin/2][1], TIMER_CFG_SPLIT_PAIR|( (ConvertTimer==0) ? TIMER_CFG_A_CAP_TIME_UP : TIMER_CFG_B_CAP_TIME_UP));
	User_TimerConfigure(WideTimerPeriph[_pin/2][1], (ConvertTimer==0) ? TIMER_A:TIMER_B,
			TIMER_CFG_SPLIT_PAIR|( (ConvertTimer==0) ? TIMER_CFG_A_CAP_TIME_UP : TIMER_CFG_B_CAP_TIME_UP) );
	TimerControlEvent(WideTimerPeriph[_pin/2][1], (ConvertTimer==0) ? TIMER_A:TIMER_B, TIMER_EVENT_BOTH_EDGES);

	if(WideTimerPeriph[_pin/2][1]==WTIMER0_BASE)
		TimerIntRegister(WTIMER0_BASE, (ConvertTimer==0) ? TIMER_A:TIMER_B,WideTimerHandler0);
	else if(WideTimerPeriph[_pin/2][1]==WTIMER1_BASE)
		TimerIntRegister(WTIMER1_BASE, (ConvertTimer==0) ? TIMER_A:TIMER_B,WideTimerHandler1);
	else if(WideTimerPeriph[_pin/2][1]==WTIMER2_BASE)
		TimerIntRegister(WTIMER2_BASE, (ConvertTimer==0) ? TIMER_A:TIMER_B,WideTimerHandler2);
	else if(WideTimerPeriph[_pin/2][1]==WTIMER3_BASE)
		TimerIntRegister(WTIMER3_BASE, (ConvertTimer==0) ? TIMER_A:TIMER_B,WideTimerHandler3);
	else if(WideTimerPeriph[_pin/2][1]==WTIMER4_BASE)
		TimerIntRegister(WTIMER4_BASE, (ConvertTimer==0) ? TIMER_A:TIMER_B,WideTimerHandler4);
	else if(WideTimerPeriph[_pin/2][1]==WTIMER5_BASE)
		TimerIntRegister(WTIMER5_BASE, (ConvertTimer==0) ? TIMER_A:TIMER_B,WideTimerHandler5);
	
	TimerIntClear(WideTimerPeriph[_pin/2][1],0xFF);
	TimerIntEnable(WideTimerPeriph[_pin/2][1],((ConvertTimer==0) ? (TIMER_CAPA_EVENT) : (TIMER_CAPB_EVENT)) );
	IntMasterEnable();
	//IntEnable(WideTimerInt[_pin]);

	return 0;

}

/*
 * Starts and reading and returns the raw value of the pulse.
 * The timeout value sets the maximum value the timer can reach before stoping the reading.
 * if the timeout is reached 0 is returned.
 */
uint32_t WideTimer_MeasureRaw(uint32_t _pin, uint8_t _Polarity,uint32_t _timeout){

	/*if(_Number > 5)
		return 0;
	if( (_TimerHalf != TIMER_A) && (_TimerHalf != TIMER_B) )
		return 0;
	if(_Polarity > 1)
		return 0;
*/
	uint8_t ConvertTimer = (_pin%2==0) ? 0 : 1;

	WideTimerPolarity[_pin]= ( (_Polarity == 0) ? 0 : WideTimerGPIO[_pin][2]);
	WideBusy[_pin]=1;
	WideFirstPulse[_pin] = 0;
	HWREG(WideTimerPeriph[_pin/2][1]+ ( (ConvertTimer==0) ?TIMER_O_TAV:TIMER_O_TBV) )=0;
	TimerEnable(WideTimerPeriph[_pin/2][1], (ConvertTimer==0) ? TIMER_A:TIMER_B);
    while(WideBusy[_pin]==1 &&
    		(HWREG(WideTimerPeriph[_pin/2][1] + ( (ConvertTimer==0) ?TIMER_O_TAV:TIMER_O_TBV) ) < _timeout) );


    /*if( HWREG(WideTimerPeriph[_pin/2][1]+ ( (ConvertTimer==0) ?TIMER_O_TAV:TIMER_O_TBV) ) > _timeout){
    	TimerDisable(WideTimerPeriph[_pin/2][1], (ConvertTimer==0) ? TIMER_A:TIMER_B);
    	WideBusy[_pin]=0;
    	return 0;
    }*/

    return WidePulseValue[(_pin)];
}

/*
 * Same as WideTimer_MeasureRaw_t but it does not have timeout as a parameter
 * Instead it always sets the maximum value of the timer as timeout (a but lower to make sure it gets it)
 */
uint32_t WideTimer_MeasureRaw(uint32_t _pin, uint8_t _Polarity){
	return WideTimer_MeasureRaw(_pin,_Polarity,4294960000);
}
/*
 * Same as WideTimer_MeasureRaw_t but it returns the value in nanoseconds
 */
uint32_t WideTimer_MeasureNanos(uint32_t _pin, uint8_t _Polarity, uint32_t _timeout){
	_timeout = _timeout/1000.0/ SystemFrequency;
	 uint32_t reading = WideTimer_MeasureRaw(_pin,_Polarity, _timeout);
	 return( reading * ( 1000.0/ SystemFrequency ) );
}
/*
 * Same as WideTimer_MeasureRaw but it returns the value in nanoseconds
 */
uint32_t WideTimer_MeasureNanos(uint32_t _pin, uint8_t _Polarity){
	 uint32_t reading = WideTimer_MeasureRaw(_pin,_Polarity);
	 return( reading * ( 1000.0/ SystemFrequency ) );
}
/*
 * Same as WideTimer_MeasureRaw_t but it returns the value in microseconds
 */
uint32_t WideTimer_MeasureMicros(uint32_t _pin, uint8_t _Polarity,uint32_t _timeout){
	_timeout = _timeout/ 1.0/ SystemFrequency;
	 uint32_t reading = WideTimer_MeasureRaw(_pin,_Polarity,_timeout);
	 return( reading * ( 1.0/ SystemFrequency ) );
}
/*
 * Same as WideTimer_MeasureRaw but it returns the value in microseconds
 */
uint32_t WideTimer_MeasureMicros(uint32_t _pin, uint8_t _Polarity){
	 uint32_t reading = WideTimer_MeasureRaw(_pin,_Polarity);
	 return( reading * ( 1.0/ SystemFrequency ) );
}


/*
 * Starts a reading for the specified pin.
 * To later get the reading one of the following functions should be used:
 *  WideTimer_GetReading
 *  WideTimer_GetNanos
 *  WideTimer_GetMicros
 *
 *
 * WideTimer_IsBusy can be used to check if the reading is done.
 *
 */
bool WideTimer_StartReading(uint32_t _pin, uint8_t _Polarity){
	if(WideBusy[_pin]==1)
		return 1;

	uint8_t ConvertTimer = (_pin%2==0) ? 0 : 1;

	WideTimerPolarity[_pin]= ( (_Polarity == 0) ? 0 : WideTimerGPIO[_pin][2]);
	WideBusy[_pin]=1;
	WideFirstPulse[_pin] = 0;
	HWREG(WideTimerPeriph[_pin/2][1]+ ( (ConvertTimer==0) ?TIMER_O_TAV:TIMER_O_TBV) )=0;
	TimerEnable(WideTimerPeriph[_pin/2][1], (ConvertTimer==0) ? TIMER_A:TIMER_B);

	return 0;
}
/*
 * Returns the reading of the timer associated with the pin selected.
 * To be used with WideTimer_StartReading. If the reading is ready it returns the raw value of it.
 * If the reading is not ready it will return 0
 */
uint32_t WideTimer_GetReading(uint32_t _pin){
	if(WideBusy[_pin]==1)
		return 0;

	return WidePulseValue[(_pin)];
}
/*
 * Returns the reading of the timer associated with the pin selected.
 * To be used with WideTimer_StartReading. If the reading is ready it is returned in nanoseconds.
 * If the reading is not ready it will return 0
 */
uint32_t WideTimer_GetNanos(uint32_t _pin){
	if(WideBusy[_pin]==1)
		return 0;

	return (WidePulseValue[(_pin)] * ( 1000.0/ SystemFrequency ));
}
/*
 * Returns the reading of the timer associated with the pin selected.
 * To be used with WideTimer_StartReading. If the reading is ready it is returned in microseconds.
 * If the reading is not ready it will return 0
 */
uint32_t WideTimer_GetMicros(uint32_t _pin){
	if(WideBusy[_pin]==1)
		return 0;

	return (WidePulseValue[(_pin)] * ( 1.0/ SystemFrequency ));
}

/*
 * Returns if the timer associated with the pin is still preforming a reading.
 */
bool WideTimer_IsBusy(uint32_t _pin){

	return(WideBusy[_pin]);

}




void WideTimerHandler0 ()
{
	uint32_t flags =  HWREG(WTIMER0_BASE + TIMER_O_MIS);
	HWREG(WTIMER0_BASE + TIMER_O_ICR) = flags;

	if(flags & TIMER_CAPA_EVENT){
		if (HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + (GPIO_PIN_4 << 2))) == WideTimerPolarity[0])
		{
			WideFirstPulse[0] = HWREG(WTIMER0_BASE + TIMER_O_TAR);
		}
		else if(WideFirstPulse[0] != 0)
		{
			HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TAEN); //TimerDisable(WTIMER0_BASE, TIMER_A);

			WidePulseValue[0] = ( HWREG(WTIMER0_BASE + TIMER_O_TAR) )- WideFirstPulse[0];
			WideBusy[0] = 0;
		}
	}
	if(flags & TIMER_CAPB_EVENT){
		if (HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + (GPIO_PIN_5 << 2))) == WideTimerPolarity[1])
		{
			WideFirstPulse[1] = HWREG(WTIMER0_BASE + TIMER_O_TBR);
		}
		else if(WideFirstPulse[1] != 0)
		{
			HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TBEN); //TimerDisable(WTIMER0_BASE, TIMER_A);

			WidePulseValue[1] = ( HWREG(WTIMER0_BASE + TIMER_O_TBR) )- WideFirstPulse[1];
			WideBusy[1] = 0;
		}
	}

}
void WideTimerHandler1 ()
{
	uint32_t flags =  HWREG(WTIMER1_BASE + TIMER_O_MIS);
	HWREG(WTIMER1_BASE + TIMER_O_ICR) = flags;

	if(flags & TIMER_CAPA_EVENT){
		if (HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + (GPIO_PIN_6 << 2))) == WideTimerPolarity[2])
		{
			WideFirstPulse[2] = HWREG(WTIMER1_BASE + TIMER_O_TAR);
		}
		else if(WideFirstPulse[2] != 0)
		{
			HWREG(WTIMER1_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TAEN); //TimerDisable(WTIMER1_BASE, TIMER_A);

			WidePulseValue[2] = ( HWREG(WTIMER1_BASE + TIMER_O_TAR) )- WideFirstPulse[2];
			WideBusy[2] = 0;
		}
	}
	if(flags & TIMER_CAPB_EVENT){
		if (HWREG(GPIO_PORTC_BASE + (GPIO_O_DATA + (GPIO_PIN_7 << 2))) == WideTimerPolarity[3])
		{
			WideFirstPulse[3] = HWREG(WTIMER1_BASE + TIMER_O_TBR);
		}
		else if(WideFirstPulse[3] != 0)
		{
			HWREG(WTIMER1_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TBEN); //TimerDisable(WTIMER1_BASE, TIMER_A);

			WidePulseValue[3] = ( HWREG(WTIMER1_BASE + TIMER_O_TBR) )- WideFirstPulse[3];
			WideBusy[3] = 0;
		}
	}
}

void WideTimerHandler2()
{
	uint32_t flags =  HWREG(WTIMER2_BASE + TIMER_O_MIS);
	HWREG(WTIMER2_BASE + TIMER_O_ICR) = flags;

	if(flags & TIMER_CAPA_EVENT){
		if (HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + (GPIO_PIN_0 << 2))) == WideTimerPolarity[4])
		{
			WideFirstPulse[4] = HWREG(WTIMER2_BASE + TIMER_O_TAR);
		}
		else if(WideFirstPulse[4] != 0)
		{
			HWREG(WTIMER2_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TAEN); //TimerDisable(WTIMER2_BASE, TIMER_A);

			WidePulseValue[4] = ( HWREG(WTIMER2_BASE + TIMER_O_TAR) )- WideFirstPulse[4];
			WideBusy[4] = 0;
		}
	}
	if(flags & TIMER_CAPB_EVENT){
		if (HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + (GPIO_PIN_1 << 2))) == WideTimerPolarity[5])
		{
			WideFirstPulse[5] = HWREG(WTIMER2_BASE + TIMER_O_TBR);
		}
		else if(WideFirstPulse[5] != 0)
		{
			HWREG(WTIMER2_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TBEN); //TimerDisable(WTIMER2_BASE, TIMER_A);

			WidePulseValue[5] = ( HWREG(WTIMER2_BASE + TIMER_O_TBR) )- WideFirstPulse[5];
			WideBusy[5] = 0;
		}
	}
}

void WideTimerHandler3()
{
	uint32_t flags =  HWREG(WTIMER3_BASE + TIMER_O_MIS);
	HWREG(WTIMER3_BASE + TIMER_O_ICR) = flags;

	if(flags & TIMER_CAPA_EVENT){
		if (HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + (GPIO_PIN_2 << 2))) == WideTimerPolarity[6])
		{
			WideFirstPulse[6] = HWREG(WTIMER3_BASE + TIMER_O_TAR);
		}
		else if(WideFirstPulse[6] != 0)
		{
			HWREG(WTIMER3_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TAEN); //TimerDisable(WTIMER2_BASE, TIMER_A);

			WidePulseValue[6] = ( HWREG(WTIMER3_BASE + TIMER_O_TAR) )- WideFirstPulse[6];
			WideBusy[6] = 0;
		}
	}
	if(flags & TIMER_CAPB_EVENT){
		if (HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + (GPIO_PIN_3 << 2))) == WideTimerPolarity[7])
		{
			WideFirstPulse[7] = HWREG(WTIMER3_BASE + TIMER_O_TBR);
		}
		else if(WideFirstPulse[7] != 0)
		{
			HWREG(WTIMER3_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TBEN); //TimerDisable(WTIMER2_BASE, TIMER_A);

			WidePulseValue[7] = ( HWREG(WTIMER3_BASE + TIMER_O_TBR) )- WideFirstPulse[7];
			WideBusy[7] = 0;
		}
	}
}

void WideTimerHandler4()
{
	uint32_t flags =  HWREG(WTIMER4_BASE + TIMER_O_MIS);
	HWREG(WTIMER4_BASE + TIMER_O_ICR) = flags;

	if(flags & TIMER_CAPA_EVENT){
		if (HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + (GPIO_PIN_4 << 2))) == WideTimerPolarity[8])
		{
			WideFirstPulse[8] = HWREG(WTIMER4_BASE + TIMER_O_TAR);
		}
		else if(WideFirstPulse[8] != 0)
		{
			HWREG(WTIMER4_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TAEN); //TimerDisable(WTIMER2_BASE, TIMER_A);

			WidePulseValue[8] = ( HWREG(WTIMER4_BASE + TIMER_O_TAR) )- WideFirstPulse[8];
			WideBusy[8] = 0;
		}
	}
	if(flags & TIMER_CAPB_EVENT){
		if (HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + (GPIO_PIN_5 << 2))) == WideTimerPolarity[9])
		{
			WideFirstPulse[9] = HWREG(WTIMER4_BASE + TIMER_O_TBR);
		}
		else if(WideFirstPulse[9] != 0)
		{
			HWREG(WTIMER4_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TBEN); //TimerDisable(WTIMER2_BASE, TIMER_A);

			WidePulseValue[9] = ( HWREG(WTIMER4_BASE + TIMER_O_TBR) )- WideFirstPulse[9];
			WideBusy[9] = 0;
		}
	}
}

void WideTimerHandler5()
{
	uint32_t flags =  HWREG(WTIMER5_BASE + TIMER_O_MIS);
	HWREG(WTIMER5_BASE + TIMER_O_ICR) = flags;

	if(flags & TIMER_CAPA_EVENT){
		if (HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + (GPIO_PIN_6 << 2))) == WideTimerPolarity[8])
		{
			WideFirstPulse[8] = HWREG(WTIMER5_BASE + TIMER_O_TAR);
		}
		else if(WideFirstPulse[10] != 0)
		{
			HWREG(WTIMER5_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TAEN); //TimerDisable(WTIMER2_BASE, TIMER_A);

			WidePulseValue[10] = ( HWREG(WTIMER5_BASE + TIMER_O_TAR) )- WideFirstPulse[10];
			WideBusy[10] = 0;
		}
	}
	if(flags & TIMER_CAPB_EVENT){
		if (HWREG(GPIO_PORTD_BASE + (GPIO_O_DATA + (GPIO_PIN_7 << 2))) == WideTimerPolarity[11])
		{
			WideFirstPulse[11] = HWREG(WTIMER5_BASE + TIMER_O_TBR);
		}
		else if(WideFirstPulse[11] != 0)
		{
			HWREG(WTIMER5_BASE + TIMER_O_CTL) &= ~(TIMER_CTL_TBEN); //TimerDisable(WTIMER2_BASE, TIMER_A);

			WidePulseValue[11] = ( HWREG(WTIMER5_BASE + TIMER_O_TBR) )- WideFirstPulse[11];
			WideBusy[11] = 0;
		}
	}

}
