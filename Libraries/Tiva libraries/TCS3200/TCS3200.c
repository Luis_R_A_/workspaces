/*
 * TCS3200.c
 *
 *	Check TCS3200.h for more info
 *
 *  Created on: 30/05/2015
 *      Author: Lu�s Afonso
 */



#include "TCS3200.h"

/*
 *  RGB values - Raw values from the timer, saved white color values in microseconds and
 * 	last read RGB values in 255 from 0-255 (defaul 0-255).
 * 	All three arrays are acessible outside the file.
 */
uint32_t RGBWraw[4];
uint32_t RGBWwhite[4];
uint32_t RGBW[4];

/*
 * Private variables
 */
uint32_t Neutral; //Holds if last values checked are neutral or not.
uint32_t CapPin; //Pin used for the sensor


/*
 * Initialization function for the sensor.
 * It configures the 2 color select pins - PE2 and PE3
 * Configures the PF1, PF2, PF3 to use the RGB LED
 * PF4 is configured to be a input for the button (for calibration)
 *
 * Enables the FPU
 *
 * Configures the WideTimer associated with the CapPin selected
 * Available pins are:
 * 	PC4, PC5, PC6, PC7, PD0, PD1, PD2, PD3, PD4, PD5, PD6, PD7.
 *
 * 	Also sets default value for 255 and NeutralLimit
 *
 * 	Parameters:
 * 	-uint32_t _CapPin - pin to use with the sensor
 * 	-uint32_t _Mhz - processor frequency in Mhz
 *
 * 	Returns:
 * 	-Nothing
 *
 */
void TCS3200_Init(uint32_t _CapPin,uint32_t _Mhz){

	WideTimerInit(_CapPin,_Mhz);
	FPUEnable();
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE,GPIO_PIN_2|GPIO_PIN_3);


	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
	GPIOPinTypeGPIOInput(GPIO_PORTF_BASE,GPIO_PIN_4);
	GPIOPadConfigSet(GPIO_PORTF_BASE,GPIO_PIN_4,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);

	CapPin = _CapPin;
	ColorMath_SetNeutralLimit(10);
}

/*
 * Gets each filter reading, 1 by 1, then puts it in the array passed as parameter.
 * The values returned by parameter range from 0-255
 * A Calibration should be done before calling this function. Use TCS3200_Calibrate
 * This function execution time depnds praticaly only in the sum of the 4 pulses periods.
 *
 * Parameters:
 * -uint32_t _M[4] - Array that will hold the RGBW values. Requires at least size 4.
 *
 * Returns:
 * -Nothing
 */
void TCS3200_ColorReading(uint32_t _M[4]){
	GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_3|GPIO_PIN_2,0);
	RGBWraw[0]  = WideTimer_MeasureMicros(CapPin,POSITIVE_PULSE);

	GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_3|GPIO_PIN_2,GPIO_PIN_3);
	RGBWraw[2] = WideTimer_MeasureMicros(CapPin,POSITIVE_PULSE);

	GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_3|GPIO_PIN_2,GPIO_PIN_2|GPIO_PIN_3);
	RGBWraw[1] = WideTimer_MeasureMicros(CapPin,POSITIVE_PULSE);

	GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_3|GPIO_PIN_2,GPIO_PIN_2);
	RGBWraw[3]  = WideTimer_MeasureMicros(CapPin,POSITIVE_PULSE);

	int i;
	for(i=0; i <4; i++){
		RGBW[i]=RGBWraw[i];
	}
	ColorMath_ConvertRGB(RGBW,1);

	for(i=0; i <4; i++){
		_M[i]=RGBW[i];
	}

}

/*
 * Configures the RGBWwhite values.
 * Call this before making any reading.
 *
 * When this function is called it will flash white in the RGB LED of the launchpad, signaling it's
 * awaiting a reading.
 * The sensor should be placed over the color white at the desired distance. Then just press the left button
 * on the launchpad to calibrate. The calibration is done when the LED blinks Red, Green, Blue and White
 *
 * Parameters:
 * -None
 *
 * Returns:
 * -Nothing
 */
void TCS3200_Calibrate(){
	while(GPIOPinRead(GPIO_PORTF_BASE,GPIO_PIN_4) !=0){
		GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
		SysCtlDelay(2666666);
		GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,0);
		SysCtlDelay(2666666);
	}
	TCS3200_ColorReading(RGBW);

	ColorMath_Calibrate(RGBWraw);

	int i;
	for(i=0; i<4 ;i++)
		RGBWwhite[i] = RGBWraw[i];


	GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,GPIO_PIN_1);
	SysCtlDelay(2666666);
	GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,GPIO_PIN_3);
	SysCtlDelay(2666666);
	GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,GPIO_PIN_2);
	SysCtlDelay(2666666);
	GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
	SysCtlDelay(2666666);
	GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,0);
	SysCtlDelay(2666666);

}

/*
 * Calculates how much time the readtime was for the last reading made.
 *
 * Parameters:
 * -None
 *
 * Returns:
 * -uint32_t readTime - sum of all values in RGBWraw, which gives total read time in microseconds
 *
 */
uint32_t TCS3200_readTime(){
	int i=0;
	int readTime = 0;
	for(i=0;i<4;i++)
	readTime +=RGBWraw[i];

	return readTime;
}


/*
 * Sets the limit for Neutral Colors
 *
 * Parameters:
 * -uint32_t _value - the new maximum distance from the neutral line for a color to be neutral.
 *
 * Returns:
 * -Nothing
 */
void TCS3200_SetNeutralLimit(uint32_t _value){
	ColorMath_SetNeutralLimit(_value);
}
/*
 * Returns the current limit
 *
 * Parameters:
 * -None
 *
 * Returns:
 * -uint32_t NeutralLimit - the current value for NeutralLimit
 */
uint32_t TCS3200_GetNeutralLimit(){
	return ColorMath_GetNeutralLimit();
}

/*
 * This function will return, based on the values in RGBW, if they represent
 * a neutral color depending on NeutralLimit.
 *
 * It uses the RGB cube.
 * It calculates the distance from the color point (RGB coordinates) to the neutral line.
 * The neutral line is the line that goes from 0,0,0 to 255,255,255.
 *
 * Parameters:
 * -None
 *
 * Returns:
 * bool Neutral - returns if the color is Neutral or not
 */
bool TCS3200_Neutral(){

	Neutral = ColorMath_IsNeutral(RGBW);
	return Neutral;

}

/*
 * This function shows the color last read when called.
 *
 * Parameters:
 * -None
 *
 * Returns:
 * -Nothing
 */
void TCS3200_ShowColors(){
	if(Neutral && RGBW[3] > 80 )
		GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
	else if(Neutral) //&& RGBW[3] < 40)
		GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,0);

	else if( (RGBW[0] > RGBW[1]) && (RGBW[0] > RGBW[2]) )
		GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,GPIO_PIN_1);
	else if( (RGBW[1] > RGBW[0]) && (RGBW[1] > RGBW[2]) )
		GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,GPIO_PIN_3);
	else if( (RGBW[2] > RGBW[1]) && (RGBW[2] > RGBW[0]))
		GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3,GPIO_PIN_2);
}

/*
 * This function will start a reading for a specific color
 * The value should be obtain later by calling TCS3200_GetColorReading
 *
 * 	Parameters:
 * 	- uint32_t Color -The parameter color can be:
 * 						RED_FILTER
 * 						GREEN_FILTER
 * 						BLUE_FILTER
 * 						WHITE_FILTER
 *
 * 	Returns:
 * 	-1 if the timer is busy and cannot make a reading right now
 * 	-0 if it was sucessfull in starting a reading
 */
bool TCS3200_StartColorReading(uint32_t Color){

	if(WideTimer_IsBusy(CapPin))
		return 1;

	if(Color == RED_FILTER)
		GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_3|GPIO_PIN_2,0);
	else if(Color == GREEN_FILTER)
		GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_3|GPIO_PIN_2,GPIO_PIN_2|GPIO_PIN_3);
	else if(Color == BLUE_FILTER)
		GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_3|GPIO_PIN_2,GPIO_PIN_3);
	else if(Color == WHITE_FILTER)
		GPIOPinWrite(GPIO_PORTE_BASE,GPIO_PIN_3|GPIO_PIN_2,GPIO_PIN_2);

	WideTimer_StartReading(CapPin,POSITIVE_PULSE);

	return 0;

}

/*
 * This function returns the last reading for the color passed as parameter.
 * It should be used with TCS3200_StartColorReading
 * If the reading is not ready yet it will return 0
 *
 * 	Parameters:
 * 	- uint32_t Color -The parameter color can be:
 * 						RED_FILTER
 * 						GREEN_FILTER
 * 						BLUE_FILTER
 * 						WHITE_FILTER
 *
 * 	Returns:
 * 	-uint32_t RGBW[Color] - returns the last reading of the specific color asked
 * 	-returns 0 if the reading is not ready yet.
 */
uint32_t TCS3200_GetColorReading(uint32_t Color){
	if(WideTimer_IsBusy(CapPin))
		return 0;

		RGBWraw[Color]=WideTimer_GetMicros(CapPin);
		RGBW[Color]=255*RGBWwhite[Color]/RGBWraw[Color];
		if(RGBW[Color]>255)
			RGBW[Color] = 255;


	 return RGBW[Color];
}
