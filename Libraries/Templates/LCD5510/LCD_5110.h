///
/// @file 	LCD_5110.h
/// @brief	Library for Nokia 5110 display
/// @details Ported to LM4F120 by Bryan Schremp (bajabug@gmail.com) 11/10/2012
/// @n		Uses GLCD fonts
/// @n		Push button 2 to turn backlight on / off
///
/// @a 		Developed with [embedXcode](http://embedXcode.weebly.com)
///
/// @author 	Rei VILO
/// @author 	http://embeddedcomputing.weebly.com
/// @date	Dec 13, 2012
/// @version	release 104
/// @n
/// @copyright 	© Rei VILO, 2010-2012
/// @copyright 	CC = BY NC SA
/// @n		http://embeddedcomputing.weebly.com
///
/// @see
/// *		Ported to LM4F120 by Bryan Schremp (bajabug@gmail.com) 11/10/2012
/// @n		http://forum.stellarisiti.com/topic/330-lcd-5110-lm4f120-sample-sketch-stellarpad-energia-branch/?p=1333
/// * 		Fonts generated with MikroElektronika GLCD Font Creator 1.2.0.0
/// @n		http://www.mikroe.com
///
// 2015-02-07 Rei Vilo
// Pins numbers instead of pins names


// Core library - IDE-based

#ifndef LCD_5110_h
#define LCD_5110_h

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <cstring>


#include "Terminal6.h"
#include "Terminal12.h"

#define _commandLCD 0x00
#define _dataLCD 0x01


/*=================
Driver functions
Should be added in a .c specific to the platform.
*/
extern void LCD5510_begin(void);
extern void LCD5510_write(uint8_t dataCommand, uint8_t c);
extern void LCD5510_setBacklight(bool flag);
void LCD5110_shiftOut(bool bitOrder,uint8_t val);
//===============


extern void LCD5510_WhoAmI(char a[], uint32_t size);
extern void LCD5510_clear(void);
extern void LCD5510_setFont(uint8_t font);
extern uint8_t LCD5510_fontX();
extern uint8_t LCD5510_fontY();
extern void LCD5510_text(uint8_t x, uint8_t y, char a[]);
extern void LCD5510_setXY(uint8_t x, uint8_t y);


extern uint8_t _font;

extern uint8_t _pinReset;
extern uint8_t _pinSerialData;
extern uint8_t _pinBacklight;
extern uint8_t _pinChipSelect;
extern uint8_t _pinDataCommand;
extern uint8_t _pinSerialClock;
extern uint8_t _pinPushButton;

#endif
