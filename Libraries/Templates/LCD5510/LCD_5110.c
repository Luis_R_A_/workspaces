//
//  LCD_5110.cpp
//  Library C++ code
//  ----------------------------------
//  Developed with embedXcode
//
//  Project LCD 5110
//  Created by Rei VILO on 28/05/12
//  Copyright (c) 2012 http://embeddedcomputing.weebly.com
//  Licence CC = BY SA NC
//

#include "LCD_5110.h"
  

uint8_t _font;

void LCD5510_setXY(uint8_t x, uint8_t y) {
  LCD5510_write(_commandLCD, 0x40 | y);
  LCD5510_write(_commandLCD, 0x80 | x);
}


void  LCD5510_WhoAmI(char a[], uint32_t size) {
  //return "LCD Nokia 5110";
}

void  LCD5510_clear(void) {
  LCD5510_setXY(0, 0);
  uint16_t i;
  for (i=0; i<6*84; i++) LCD5510_write(_dataLCD, 0x00);
  LCD5510_setXY(0, 0);
}



void  LCD5510_setFont(uint8_t font) {
  _font = font;
}

void  LCD5510_text(uint8_t x, uint8_t y,  char a[]) {
  uint8_t i;
  uint8_t j;
  uint32_t size = strlen(a);
  
  if (_font==0) {
    LCD5510_setXY(6*x, y);
    for (j=0; j<size; j++) {
      for (i=0; i<5; i++) LCD5510_write(_dataLCD, Terminal6x8[a[j]-' '][i]);
      LCD5510_write(_dataLCD, 0x00);
    }
  }
  else if (_font==1) {
    LCD5510_setXY(6*x, y);
    for (j=0; j<size; j++) {
      for (i=0; i<11; i++) LCD5510_write(_dataLCD, Terminal11x16[a[j]-' '][2*i]);
      LCD5510_write(_dataLCD, 0x00);
    }
    
    LCD5510_setXY(6*x, y+1);
    for (j=0; j<size; j++) {
      for (i=0; i<11; i++) LCD5510_write(_dataLCD, Terminal11x16[a[j]-' '][2*i+1]);
      LCD5510_write(_dataLCD, 0x00);
    }
  }
}


