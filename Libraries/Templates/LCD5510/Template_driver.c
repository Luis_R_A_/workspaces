



#include "LCD_5110.h"

#define CHIP_SELECT GPIO_Pin_6
#define RESET GPIO_Pin_10
#define DATA_COMMAND GPIO_Pin_4
#define SERIAL_DATA GPIO_Pin_5
#define SERIAL_CLOCK GPIO_Pin_3
#define BACKLIGHT GPIO_Pin_13

void ASMDelay(uint32_t _time);

/*
  This can is the function to send the data by serial
  it can be done by bit-banging or SPI.
*/
void LCD5110_shiftOut(bool bitOrder,uint8_t val){  
  for(int i = 0; i < 8; i++){
    if (bitOrder == 0){
		//SERIAL_DATA = (val & (1 << i)) ? 1:0 //this is by bit-banging
    }
       
    else{
		//SERIAL_DATA =(val & (1 << (7 - i))) ? 1:0 //this is by bit-banging
    }

	//Set SERIAL_CLOCK to 1
	//Set SERIAL_CLOCK to 0
   
  } 
}

void LCD5510_write(uint8_t dataCommand, uint8_t c) {
  
  
  if(dataCommand==_dataLCD){
    //TODO: Set DATA_COMMAND output to 1
  }
  else{
    //TODO: Set DATA_COMMAND output to 0
  }
    
  //TODO: Set CHIP_SELECT to 0
  
  //Remember to implement internal function shiftOut.
  LCD5110_shiftOut(1, c);
  
  //Set CHIP_SELECT output to 1
 
  
}

void LCD5510_begin(){
   /*
  TODO:
  Set CHIP_SELECT, RESET, DATA_COMMAND, SERIAL_DATA, SERIAL_CLOCK and
  BACKLIGHT pins as outputs
  */

	/*
	TODO: Add systemclock frequency
	the 0 should be the systemclock, use any function or value that is more convenient
	*/
  uint32_t Systemfreq = 0; 
  //TODO: Clear DATA_COMMAND output (set to 0)

  //Wait 30ms
  ASMDelay(Systemfreq/102);

  // TODO:Clear RESET output (set to 0)

  //Wait 100ms
	ASMDelay(Systemfreq/30);
  //TODO:Set RESET output (set to 1)


  
  LCD5510_write(_commandLCD, 0x21); // chip is active, horizontal addressing, use extended instruction set
  LCD5510_write(_commandLCD, 0xc8); // write VOP to register: 0xC8 for 3V � try other values
  LCD5510_write(_commandLCD, 0x12); // set Bias System 1:48
  LCD5510_write(_commandLCD, 0x20); // chip is active, horizontal addressing, use basic instruction set
  LCD5510_write(_commandLCD, 0x09); // temperature control
  LCD5510_write(_commandLCD, 0x0c); // normal mode
  
  //Wait 10ms
	ASMDelay(Systemfreq/300);
  
    
  LCD5510_clear();
  _font = 0;
  LCD5510_setBacklight(false);
}

void LCD5510_setBacklight(bool b) {
  //TODO:set BACKLIGHT output to "b"

}
  
  
/*
this works in ARM-M in IAR
This function implements a 3 cycle delay for each _time value.
*/
void ASMDelay(uint32_t _time){
    __asm("    subs    r0, #1\n"
          "    bne.n   ASMDelay\n"
          "    bx      lr");
}
