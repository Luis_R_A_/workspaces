/*
 * drv8833.h
 *
 *  Created on: 09/06/2015
 *      Author: Lu�s Afonso
 */

#ifndef DRV8833_H_
#define DRV8833_H_

#include <stdint.h>
#include <stdbool.h>

void DRV8833_InitMotorA(); 
void DRV8833_InitMotorB();
void DRV8833_SetDigital(uint32_t _pin, bool _b);
void DRV8833_SetPWM(uint32_t _pin, uint32_t _duty);

extern DRV8833_InitMotorsAB();
extern void DRV8833_MotorA(int32_t _speed);
extern void DRV8833_MotorB(int32_t _speed);
extern void DRV8833_MotorsDuty(int32_t _speedA, int32_t speedB);

#endif /* DRV8833_H_ */
