/*
 * drv8833.c
 *
 *  Created on: 09/06/2015
 *      Author: Lu�s Afonso
 */


#include "drv8833.h"

#define DRV8833_AIN1
#define DRV8833_AIN2
#define DRV8833_BIN1
#define DRV8833_BIN2

/*
 * Add definitions for the Timers/PWM modules/GPIO if needed
 */
/*================================================
				Start of user drivers
================================================*/
uint32_t PWM_freq = (80000000 / 25000)-1; // 3200 values.


void DRV8833_InitMotorA(){
	
	/*
	 * Initalize 2 PWM outputs
	 */
	
}

void DRV8833_InitMotorB(){
	/*
	 * Initialize 2 PWM outputs
	 */
}

void DRV8833_SetDigital(uint32_t _pin, bool _b){
	
	//Set pin to digital function if needed
	//Set pin to state "_b"
	
}
void DRV8833_SetPWM(uint32_t _pin, uint32_t _duty){
	
	//Set pin to PWM function if needed
	//Set PWM duty with "_duty"
	
}

/*================================================
				End of user drivers
================================================*/



void DRV8833_InitMotorsAB(){
	DRV8833_InitMotorA();
	DRV8833_InitMotorB();
}



/*
 * Controls Motor1 speed and direction.
 *
 * Duty goes from 0 to 1024
 * The bigger the Inverseduty value, the faster the speed.
 * Remember, bigger match value, lower duty. Since in fast decay the digital pin
 * is at 1, the lower the duty, the faster the speed. Hence why Inverseduty is used.
 * (no need to make freq-Inverseduty since less is faster).
 *
 * Possibily I have this wrong as I don't remember if the pulse starts high or low.
 *
 */
void DRV8833_MotorA(int32_t _speed){

	if(_speed ==0){
		
		DRV8833_SetDigital(DRV8833_AIN1,0);
		DRV8833_SetDigital(DRV8833_AIN2,0);
		return;
	}

	uint8_t backwards=0;
	if(_speed < 0){
		_speed = _speed*(-1);
		backwards=1;
	}
	if(_speed > 1024)
		_speed = 1024;




	uint32_t Inverseduty = _speed*PWM_freq/1024;

	if(!backwards){
		DRV8833_SetDigital(DRV8833_AIN1,1);
		if(_speed == 1024){
			DRV8833_SetDigital(DRV8833_AIN2,1);
		}
		else{
			DRV8833_SetPWM(DRV8833_AIN2,_speed);
		}
	}
	else{
		DRV8833_SetDigital(DRV8833_AIN2,1);
		if(_speed == 1024){
			DRV8833_SetDigital(DRV8833_AIN1,1);
		}
		else{
			DRV8833_SetPWM(DRV8833_AIN1,_speed);
		}
	}
}

void DRV8833_MotorB(int32_t _speed){

	if(_speed ==0){
		
		DRV8833_SetDigital(DRV8833_AIN1,0);
		DRV8833_SetDigital(DRV8833_AIN2,0);
		return;
	}

	uint8_t backwards=0;
	if(_speed < 0){
		_speed = _speed*(-1);
		backwards=1;
	}
	if(_speed > 1024)
		_speed = 1024;




	uint32_t Inverseduty = _speed*PWM_freq/1024;

	if(!backwards){
		DRV8833_SetDigital(DRV8833_BIN1,1);
		if(_speed == 1024){
			DRV8833_SetDigital(DRV8833_BIN2,1);
		}
		else{
			DRV8833_SetPWM(DRV8833_BIN2,_speed);
		}
	}
	else{
		DRV8833_SetDigital(DRV8833_BIN2,1);
		if(_speed == 1024){
			DRV8833_SetDigital(DRV8833_BIN1,1);
		}
		else{
			DRV8833_SetPWM(DRV8833_BIN1,_speed);
		}
	}

}


void DRV8833_MotorsDuty(int32_t _speedA, int32_t _speedB){
	DRV8833_MotorA(_speedA);
	DRV8833_MotorB(_speedB);
}
