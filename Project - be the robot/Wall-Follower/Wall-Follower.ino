/*
  Code to follow left wall using left and front sonar.

  Code for bot olympics 2016 kit

*/

/*
  Conectar:
  Arduino UNO - DRV8833
      5(AIN1) -   AIN1
      3(AIN2) -   AIN2
      9(BIN1) -   BIN1
     6(BIN2) -   BIN2
*/
#define AIN1 5
#define AIN2 3
#define BIN1 9
#define BIN2 6


#define ECHO_PIN_LEFT 12
#define TRIG_PIN_LEFT 11

#define ECHO_PIN_FRONT 4
#define TRIG_PIN_FRONT 2

#define ECHO_PIN_RIGHT 7
#define TRIG_PIN_RIGHT 10

#define BUTTON_PIN 8


void Motors_Init();
void MotorEsqSpeed(int Speed);
void MotorDirSpeed(int Speed);
void MotorsSpeed(int Vel_Esq, int Vel_Dir);

void setup() {
  // put your setup code here, to run once:

  pinMode(ECHO_PIN_LEFT, INPUT);
  pinMode(TRIG_PIN_LEFT, OUTPUT);
  pinMode(ECHO_PIN_FRONT, INPUT);
  pinMode(TRIG_PIN_FRONT, OUTPUT);
  pinMode(ECHO_PIN_RIGHT, INPUT);
  pinMode(TRIG_PIN_RIGHT, OUTPUT);

  pinMode(BUTTON_PIN, INPUT);

  while (digitalRead(BUTTON_PIN) != 1);

  Motors_Init();

  Serial.begin(115200);

}

void loop() {

  int temp = millis();
  int distance_L = Get_Left_Distance();
  delay(10);
  int distance_F = Get_Front_Distance();
  delay(10);
  int distance_R = Get_Right_Distance();
  delay(10);
  if (distance_L == 0)
    distance_L = 60;
  if (distance_F == 0)
    distance_F = 60;
  if (distance_R == 0)
    distance_R = 60;


  while (millis() - temp < 50);
  //delay(50);
  Serial.write(distance_L);
  Serial.write(distance_F);
  Serial.write(distance_R);

  Serial.write(255); Serial.write(1); Serial.write(1);
  
  while(Serial.available() < 2);
  int esq = Serial.read();
  int dir = Serial.read();
  
  esq = esq - 125;
  dir = dir - 125;
  MotorsSpeed(esq,dir);
  
}


int Get_Left_Distance() {

  digitalWrite(TRIG_PIN_LEFT, LOW);                   // Set the trigger pin to low for 2uS
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN_LEFT, HIGH);                  // Send a 10uS high to trigger ranging
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_LEFT, LOW);                   // Send pin low again
  int distance = pulseIn(ECHO_PIN_LEFT, HIGH, 3000);        // Read in times pulse
  distance = distance / 58;                     // Calculate distance from time of pulse

  return distance;
}

int Get_Front_Distance() {

  digitalWrite(TRIG_PIN_FRONT, LOW);                   // Set the trigger pin to low for 2uS
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN_FRONT, HIGH);                  // Send a 10uS high to trigger ranging
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_FRONT, LOW);                   // Send pin low again
  int distance = pulseIn(ECHO_PIN_FRONT, HIGH, 3000);        // Read in times pulse
  distance = distance / 58;                     // Calculate distance from time of pulse

  return distance;
}

int Get_Right_Distance() {

  digitalWrite(TRIG_PIN_RIGHT, LOW);                   // Set the trigger pin to low for 2uS
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN_RIGHT, HIGH);                  // Send a 10uS high to trigger ranging
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_RIGHT, LOW);                   // Send pin low again
  int distance = pulseIn(ECHO_PIN_RIGHT, HIGH, 3000);        // Read in times pulse
  distance = distance / 58;                     // Calculate distance from time of pulse

  return distance;
}
/*
  Chamem isto para configurar os pins.
*/
void Motors_Init() {
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
}
/*
  Muda a velocidade de só do motor da esquerda
*/
void MotorDirSpeed(int Speed) {
  if (Speed == 0) {
    digitalWrite(AIN1, 1);
    digitalWrite(AIN2, 1);
  }
  bool forwards = 1;

  if (Speed < 0) {
    Speed = Speed * (-1);
    forwards = 0;
  }
  if (Speed > 255)
    Speed = 255;

  Speed = 255 - Speed;

  if (forwards) {
    digitalWrite(AIN1, HIGH);
    analogWrite(AIN2, Speed);
  }
  else {
    analogWrite(AIN1, Speed);
    digitalWrite(AIN2, HIGH);
  }
}

/*
  Muda a velocidade de só do motor da direita
*/
void MotorEsqSpeed(int Speed) {
  if (Speed == 0) {
    digitalWrite(BIN1, 1);
    digitalWrite(BIN2, 1);
  }
  bool forwards = 1;

  if (Speed < 0) {
    Speed = Speed * (-1);
    forwards = 0;
  }
  if (Speed > 255)
    Speed = 255;

  Speed = 255 - Speed;
  if (forwards) {
    digitalWrite(BIN1, HIGH);
    analogWrite(BIN2, Speed);
  }
  else {
    analogWrite(BIN1, Speed);
    digitalWrite(BIN2, HIGH);
  }
}

/*
  Muda a velocidade de ambos os motores
*/
void MotorsSpeed(int Vel_Esq, int Vel_Dir) {
  MotorEsqSpeed(Vel_Esq);
  MotorDirSpeed(Vel_Dir);
}

