/*

 */

import processing.serial.*;

Serial myPort;  // Create object from Serial class

int robot_size = 200;
int sonar_distance_max = 60;
int sonar_scale_max = displayHeight;
int Mouse_Pressed = 0;
int invertY = -1, invertX = 1;
void setup() {

  //size(1300, 720);
  fullScreen();
  frameRate(60);
  sonar_scale_max = (displayHeight);
  String portName = Serial.list()[0];
  myPort = new Serial(this, portName, 115200);
} 

int e, f, d;
int r, g, b;
float s_e=0, s_f=0, s_d=0;
float last_error_e=0, last_error_f=0 ,last_error_d=0;
void draw() {

  /*
    Read serial port if there are 6 values available
   It should be in this order:
   -Left sonar distance
   -Front sonar distance
   -Right sonar distance
   -Red sensor
   -Green sensor
   -Blue sensor
   
   After that it will send the motors speed
   */
  if ( myPort.available() >= 6) {  // If data is available,
    e = myPort.read();         // read it and store it in val
    f = myPort.read();         // read it and store it in val
    d = myPort.read();         // read it and store it in val

    r = myPort.read();         // read it and store it in val
    g = myPort.read();         // read it and store it in val
    b = myPort.read();         // read it and store it in val

    int esq[] = {125}, dir[] = {125};
    Mouse_control(esq, dir);
    myPort.write(esq[0]);
    myPort.write(dir[0]);
  }

  //background to black
  background(0);
  //center cordinates
  translate(width/2, height/2);

  /* Draw mouse control division line and arrows for visualization*/
  stroke(255, 0, 0);
  strokeWeight(1);
  line(0, robot_size/2, 0, height/2);
  circArrow(50, (height/2)-50, 20, PI/2, 2*PI, 10); 
  scale(-1, 1);
  circArrow(50, (height/2)-50, 20, PI/2, 2*PI, 10); 
  scale(-1, 1);

  /* Draw robot */
  Robot();
  
  float error = e-s_e;
  float kp = 0.3,kd = 0.1;
  float pid = error * kp + kd* (error-last_error_e);
  s_e += pid;
  last_error_e = error;

  error = f-s_f;
  pid = error * kp + kd* (error-last_error_f);
  s_f += pid;
  last_error_f = error;

  error = d-s_d;
  pid = error * kp + kd* (error-last_error_d);
  s_d += pid;
  last_error_d = error;
  /* Draw sonars cones */
  Sonars((int)s_e, (int)s_f, (int)s_d);
  //Color_Sensor(r,g,b);
}

void Mouse_control(int _esq[], int _dir[]) {
  if ( Mouse_Pressed == 1) {
    int mouseY_centered = (invertY)*(mouseY - height/2);
    int mouseX_centered = (invertX)*(mouseX - width/2);


    float mouseY_percentage = (float)mouseY_centered / (height / 2); 
    float mouseX_percentage = (float)mouseX_centered / (width / 2);
    float module = sqrt(pow(mouseY_percentage, 2) + pow(mouseX_percentage, 2));

    if (mouseY_percentage >= 0) {
      _esq[0] = (int)(100*module);
      _dir[0] = (int)(100*module);
    } else {
      _esq[0] = -(int)(100*module);
      _dir[0] = -(int)(100*module);
    }
    _esq[0] = _esq[0] + (int)(200 * mouseX_percentage);
    _dir[0] = _dir[0] - (int)(200 * mouseX_percentage);
    /*if (mouseX_centered > 0) {
     _dir[0] = (int)(_dir[0] * (1.0 - mouseX_percentage));
     } else if (mouseX_centered < 0) {
     _esq[0] = (int)(_esq[0] * (1.0 + mouseX_percentage));
     }  */
    _esq[0] += 125;
    _dir[0] += 125;
    if (_esq[0] > 255) 
      _esq[0] = 254;
    else if (_esq[0] < 0)
      _esq[0] = 0;
    if (_dir[0] > 255) 
      _dir[0] = 254;
    else if (_dir[0] < 0)
      _dir[0] = 0;
  } else {
    _esq[0] = 125;
    _dir[0] = 125;
  }
}
void mousePressed() {
  Mouse_Pressed = 1;
}
void mouseReleased() {
  Mouse_Pressed = 0;
}

void Robot() {
  noFill();
  stroke(0, 255, 255);
  strokeWeight(5);
  ellipseMode(CENTER);
  ellipse(0, 0, robot_size, robot_size);
}

void Sonars(int _e, int _f, int _d) {

  textSize(32);
  fill(255);
  text("Distance(cm)\nLeft = " + _e + "\nFront = " + _f + "\nRight = " + _d, 400, 30); 

  float multiplier = (float)sonar_scale_max / (float)sonar_distance_max;
  _e *= multiplier;
  _f *= multiplier;
  _d *= multiplier;

  fill(255);
  noStroke();
  rotate(-HALF_PI);
  arc(robot_size/2, 0, _f, _f, -HALF_PI/4, HALF_PI/4);

  rotate(-HALF_PI/2);
  arc(robot_size/2, 0, _e, _e, -HALF_PI/4, HALF_PI/4);

  rotate(HALF_PI);
  arc(robot_size/2, 0, _d, _d, -HALF_PI/4, HALF_PI/4);

  rotate(HALF_PI/2);
}

void Color_Sensor(int _r, int _g, int _b) {
  fill(_r, _g, _b);
  noStroke();
  rectMode(CENTER);
  rect(0, 0, robot_size/2, robot_size/2);
}

void circArrow(float x, float y, float radius, float start, float stop, float arrowSize) {
  ellipseMode(CENTER);
  noFill();

  arc(x, y, radius * 2, radius * 2, start, stop);

  float arrowX = x + cos(stop) * radius;
  float arrowY = y + sin(stop) * radius;

  float point1X = x + (cos(stop) * radius) + (cos(stop - radians(arrowSize * 5)) * (arrowSize));
  float point1Y = y + (sin(stop) * radius) + (sin(stop - radians(arrowSize * 5)) * (arrowSize));

  float point2X = x + (cos(stop) * radius) + (cos(stop - radians(-arrowSize * 5)) * (-arrowSize));
  float point2Y = y + (sin(stop) * radius) + (sin(stop - radians(-arrowSize * 5)) * (-arrowSize));

  line(arrowX, arrowY, point1X, point1Y);
  line(arrowX, arrowY, point2X, point2Y);
}