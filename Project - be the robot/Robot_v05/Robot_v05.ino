//#include <Wire.h>
#include "Adafruit_TCS34725.h"


/*
  Conectar:
  Arduino UNO - DRV8833
      5(AIN1) -   AIN1
      3(AIN2) -   AIN2
      9(BIN1) -   BIN1
     6(BIN2) -   BIN2
*/

#define PROCESSING_INTERFACE
#define Serial_DEBUG
#define Serial1_BAUDRATE 9600


#define AIN1 5
#define AIN2 3
#define BIN1 9
#define BIN2 6

#define MAX_DISTANCE 40
#define SONAR_TIMEOUT 500//2320

#define ECHO_PIN_LEFT 12
#define TRIG_PIN_LEFT 11

#define ECHO_PIN_FRONT 4
#define TRIG_PIN_FRONT 2

#define ECHO_PIN_RIGHT 10
#define TRIG_PIN_RIGHT 7

#define BUTTON_PIN 8

#define FLAME_SENSOR_PIN A0

#define FAN_PIN 13
#define FAN_ON 0
#define FAN_OFF 1

void Motors_Init();
void MotorEsqSpeed(int Speed);
void MotorDirSpeed(int Speed);
void MotorsSpeed(int Vel_Esq, int Vel_Dir);


/* Initialise with specific int time and gain values */
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_24MS, TCS34725_GAIN_16X);

int r_white = 4787, g_white = 5535, b_white = 4821;
void setup() {
  // put your setup code here, to run once:

  pinMode(ECHO_PIN_LEFT, INPUT);
  pinMode(TRIG_PIN_LEFT, OUTPUT);
  pinMode(ECHO_PIN_FRONT, INPUT);
  pinMode(TRIG_PIN_FRONT, OUTPUT);
  pinMode(ECHO_PIN_RIGHT, INPUT);
  pinMode(TRIG_PIN_RIGHT, OUTPUT);

  pinMode(BUTTON_PIN, INPUT);

  pinMode(FAN_PIN, OUTPUT);
  digitalWrite(FAN_PIN, FAN_OFF);

  while (digitalRead(BUTTON_PIN) != 1);

  Motors_Init();

  tcs.begin();
  Serial1.begin(Serial1_BAUDRATE);

#ifdef PROCESSING_INTERFACE
  Serial1.write('\n');
  Serial1.print("123");
  Serial1.write('\n');


  char a = 0;
  while (a != 'L') {
    while (Serial1.available() < 1);
    a = Serial1.read();
  }
#endif
#ifdef Serial_DEBUG
  Serial.begin(Serial1_BAUDRATE);
  Serial.println("Starting");
#endif

}

int last_cmd = 1;
void loop() {

  int temp = millis();
  int distance_L = Get_Left_Distance();
  delay(10);
  int distance_F = Get_Front_Distance();
  delay(10);
  int distance_R = Get_Right_Distance();
  delay(10);
  if (distance_L == 0)
    distance_L = MAX_DISTANCE;
  if (distance_F == 0)
    distance_F = MAX_DISTANCE;
  if (distance_R == 0)
    distance_R = MAX_DISTANCE;

  /* Get flame sensor raw data */
  int Flame_Sensor_Value = analogRead(FLAME_SENSOR_PIN);

  /* Get color sensor raw data */
  uint16_t r, g, b, c;
  tcs.getRawData(&r, &g, &b, &c);


  unsigned int r1 = (unsigned long)r * 255 / r_white;
  unsigned int g1 = (unsigned long)g * 255 / g_white;
  unsigned int b1 = (unsigned long)b * 255 / b_white;
  if (r1 > 255)
    r1 = 255;
  if (g1 > 255)
    g1 = 255;
  if (b1 > 255)
    b1 = 255;
  while (millis() - temp < 50);
  //delay(50);

#ifdef PROCESSING_INTERFACE
  Serial1.write(distance_L);
  Serial1.write(distance_F);
  Serial1.write(distance_R);

  Serial1.write(r1); Serial1.write(g1); Serial1.write(b1);

  if (Flame_Sensor_Value == 0)
    Flame_Sensor_Value++;
  Serial1.write(Flame_Sensor_Value / 4);

  // Serial1.write('\n');

  while (Serial1.available() < 3);
  int esq = Serial1.read();
  int dir = Serial1.read();
  int cmd = Serial1.read();

  if (cmd == 1 && last_cmd != cmd) {
    digitalWrite(FAN_PIN, FAN_OFF);
  }
  else if (cmd == 2 && last_cmd != cmd) {
    digitalWrite(FAN_PIN, FAN_ON);
  }
  last_cmd = cmd;

  esq = esq - 125;
  dir = dir - 125;
  MotorsSpeed(esq*2, dir*2);

#endif


#ifdef Serial_DEBUG
  Serial.print("Red: " ); Serial.print(r1); Serial.print("; Green: "); Serial.print(g1); Serial.print("; Blue: "); Serial.println(b1);
  delay(100);
#endif

}


int Get_Left_Distance() {

  digitalWrite(TRIG_PIN_LEFT, LOW);                   // Set the trigger pin to low for 2uS
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN_LEFT, HIGH);                  // Send a 10uS high to trigger ranging
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_LEFT, LOW);                   // Send pin low again
  int distance = pulseIn(ECHO_PIN_LEFT, HIGH, SONAR_TIMEOUT);        // Read in times pulse
  distance = distance / 58;                     // Calculate distance from time of pulse

  return distance;
}

int Get_Front_Distance() {

  digitalWrite(TRIG_PIN_FRONT, LOW);                   // Set the trigger pin to low for 2uS
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN_FRONT, HIGH);                  // Send a 10uS high to trigger ranging
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_FRONT, LOW);                   // Send pin low again
  int distance = pulseIn(ECHO_PIN_FRONT, HIGH, SONAR_TIMEOUT);        // Read in times pulse
  distance = distance / 58;                     // Calculate distance from time of pulse

  return distance;
}

int Get_Right_Distance() {

  digitalWrite(TRIG_PIN_RIGHT, LOW);                   // Set the trigger pin to low for 2uS
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN_RIGHT, HIGH);                  // Send a 10uS high to trigger ranging
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_RIGHT, LOW);                   // Send pin low again
  int distance = pulseIn(ECHO_PIN_RIGHT, HIGH, SONAR_TIMEOUT);        // Read in times pulse
  distance = distance / 58;                     // Calculate distance from time of pulse

  return distance;
}
/*
  Chamem isto para configurar os pins.
*/
void Motors_Init() {
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
}
/*
  Muda a velocidade de só do motor da esquerda
*/
void MotorDirSpeed(int Speed) {
  if (Speed == 0) {
    digitalWrite(AIN1, 1);
    digitalWrite(AIN2, 1);
  }
  bool forwards = 1;

  if (Speed < 0) {
    Speed = Speed * (-1);
    forwards = 0;
  }
  if (Speed > 255)
    Speed = 255;

  Speed = 255 - Speed;

  if (forwards) {
    digitalWrite(AIN1, HIGH);
    analogWrite(AIN2, Speed);
  }
  else {
    analogWrite(AIN1, Speed);
    digitalWrite(AIN2, HIGH);
  }
}

/*
  Muda a velocidade de só do motor da direita
*/
void MotorEsqSpeed(int Speed) {
  if (Speed == 0) {
    digitalWrite(BIN1, 1);
    digitalWrite(BIN2, 1);
  }
  bool forwards = 1;

  if (Speed < 0) {
    Speed = Speed * (-1);
    forwards = 0;
  }
  if (Speed > 255)
    Speed = 255;

  Speed = 255 - Speed;
  if (forwards) {
    digitalWrite(BIN1, HIGH);
    analogWrite(BIN2, Speed);
  }
  else {
    analogWrite(BIN1, Speed);
    digitalWrite(BIN2, HIGH);
  }
}

/*
  Muda a velocidade de ambos os motores
*/
void MotorsSpeed(int Vel_Esq, int Vel_Dir) {
  MotorEsqSpeed(Vel_Esq);
  MotorDirSpeed(Vel_Dir);
}

