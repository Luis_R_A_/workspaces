void run_app(){
    /*
    Read serial port if there are 6 values available
   It should be in this order:
   -Left sonar distance
   -Front sonar distance
   -Right sonar distance
   -Red sensor
   -Green sensor
   -Blue sensor
   
   After that it will send the motors speed
   */
  if ( myPort.available() >= 7) {  // If data is available,
    e = myPort.read();         // read it and store it in val
    f = myPort.read();         // read it and store it in val
    d = myPort.read();         // read it and store it in val

    r = myPort.read();         // read it and store it in val
    g = myPort.read();         // read it and store it in val
    b = myPort.read();         // read it and store it in val

    flame = myPort.read();

    int esq[] = {125}, dir[] = {125};
    //Mouse_control(esq, dir);
    KeyControl(esq, dir);
    myPort.write(esq[0]);
    myPort.write(dir[0]);
    myPort.write(fan);
  }

  //background to black
  background(0);

  on_button.Draw();
  if (fan == 2) {
    noFill();  
    strokeWeight(16);
    stroke(255, 0, 0);
    rect(width-470, height-200, button_size_x, button_size_y);
  }
  if (on_button.MouseIsOver()) {
    noFill();
    stroke(255, 255, 0);
    strokeWeight(5);
    rect(width-470, height-200, button_size_x, button_size_y);
  }

  // draw the button in the window


  //center cordinates
  translate(width/2, height/2);




  /* Draw mouse control division line and arrows for visualization*/
  stroke(255, 0, 0);
  strokeWeight(1);
  line(0, robot_size/2, 0, height/2);
  circArrow(50, (height/2)-50, 20, PI/2, 2*PI, 10); 
  scale(-1, 1);
  circArrow(50, (height/2)-50, 20, PI/2, 2*PI, 10); 
  scale(-1, 1);

  /* Draw robot */
  Robot();

  float error = e-s_e;
  float kp = 0.5;
  float pid = error * kp;
  s_e += pid;

  error = f-s_f;
  pid = error * kp;
  s_f += pid;

  error = d-s_d;
  pid = error * kp;
  s_d += pid;
  /* Draw sonars cones */
  Sonars((int)s_e, (int)s_f, (int)s_d);
  
  kp = 0.5;
  error = r-s_r;
  pid = error * kp;
  s_r += pid;
  error = g-s_g;
  pid = error * kp;
  s_g += pid;
  error = b-s_b;
  pid = error * kp;
  s_b += pid;
  Color_Sensor((int)s_r, (int)s_g, (int)s_b);  
  
  error = flame-s_flame;
  pid = error * kp;
  s_flame += pid;
  Flame_Sensor((int)s_flame);
  
  
 // delay(10);

}