/*

 */

import processing.serial.*;

Serial myPort;  // Create object from Serial class

Button on_button;

int robot_size = 200;
int sonar_distance_max = 40;
int sonar_scale_max = displayHeight;
int Mouse_Pressed = 0;
int invertY = -1, invertX = 1;
int key_UP = 0, key_DOWN = 0, key_LEFT = 0, key_RIGHT = 0;
void setup() {

  //size(1300, 720);
  fullScreen();
  frameRate(60);
  sonar_scale_max = (displayHeight);
  String portName = Serial.list()[1];
  myPort = new Serial(this, portName, 115200);


  on_button = new Button("Fan On", width-470, height-200, button_size_x, button_size_y);
} 



int e, f, d;
int r, g, b;
float s_r=0, s_g=0, s_b=0;
int flame, s_flame;
float s_e=0, s_f=0, s_d=0;
int fan = 1;
int first_run = 1;

void draw() {

  if(first_run == 1) {
    background(0);
    textSize(32);
    text("Please press the blue button on the robot", 400, height/2); 
    delay(100);
    if (myPort.available() >= 1) {
      String inBuffer = myPort.readStringUntil('\n');
      if (inBuffer!= null)
        if (inBuffer.equals("123\n") == true) {
          first_run = 0 ;
          myPort.write('L');
        }
    }
  }
  if(first_run == 0){
    run_app();
  }

}




void Robot() {
  noFill();
  stroke(0, 255, 255);
  strokeWeight(5);
  ellipseMode(CENTER);
  ellipse(0, 0, robot_size, robot_size);
}

void Sonars(int _e, int _f, int _d) {

  textSize(32);
  fill(255);
  text("Distance(cm)\nLeft = " + _e + "\nFront = " + _f + "\nRight = " + _d, 400, -50); 

  float multiplier = (float)sonar_scale_max / (float)sonar_distance_max;
  _e *= multiplier;
  _f *= multiplier;
  _d *= multiplier;

  fill(255);
  noStroke();
  rotate(-HALF_PI);
  arc(robot_size/2, 0, _f, _f, -HALF_PI/4, HALF_PI/4);

  rotate(-HALF_PI/2);
  arc(robot_size/2, 0, _e, _e, -HALF_PI/4, HALF_PI/4);

  rotate(HALF_PI);
  arc(robot_size/2, 0, _d, _d, -HALF_PI/4, HALF_PI/4);

  rotate(HALF_PI/2);
}

void Color_Sensor(int _r, int _g, int _b) {
  fill(_r, _g, _b);
  noStroke();
  rectMode(CENTER);
  rect(0, 0, robot_size/2, robot_size/2);
}

void Flame_Sensor(int _f) {
  fill(255, 200, 0);
  noStroke(); 
  rectMode(CORNER);
  text("Flame sensor: ", width/3-100, height/2-270);
  rect(width/3, height/2, 50, -_f);
}

void circArrow(float x, float y, float radius, float start, float stop, float arrowSize) {
  ellipseMode(CENTER);
  noFill();

  arc(x, y, radius * 2, radius * 2, start, stop);

  float arrowX = x + cos(stop) * radius;
  float arrowY = y + sin(stop) * radius;

  float point1X = x + (cos(stop) * radius) + (cos(stop - radians(arrowSize * 5)) * (arrowSize));
  float point1Y = y + (sin(stop) * radius) + (sin(stop - radians(arrowSize * 5)) * (arrowSize));

  float point2X = x + (cos(stop) * radius) + (cos(stop - radians(-arrowSize * 5)) * (-arrowSize));
  float point2Y = y + (sin(stop) * radius) + (sin(stop - radians(-arrowSize * 5)) * (-arrowSize));

  line(arrowX, arrowY, point1X, point1Y);
  line(arrowX, arrowY, point2X, point2Y);
}