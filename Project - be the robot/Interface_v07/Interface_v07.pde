/*

 */

import processing.serial.*;
import controlP5.*;
import java.util.*;
ControlP5 cp5;

int GUI_Serial = 255;

Serial myPort;  // Create object from Serial class

Button on_button;

int FRAMERATE = 60;
float SONAR_KP = 0.5;

int robot_size = 200;
int sonar_distance_max = 40;
int sonar_scale_max = displayHeight;
int Mouse_Pressed = 0;
int invertY = -1, invertX = 1;
int key_UP = 0, key_DOWN = 0, key_LEFT = 0, key_RIGHT = 0;

int Serial_Box_x = 100, Serial_Box_y = 100;
void setup() {

  //size(800, 600);
  fullScreen();
  noSmooth();
  frameRate(FRAMERATE);
  sonar_scale_max = (displayHeight);
  //String portName = Serial.list()[1];
  //myPort = new Serial(this, "COM6", 115200);


  on_button = new Button("Fan On", width-470, height-200, button_size_x, button_size_y);
  
  PFont font_label1 = createFont("arial", 15);
  List serial = Arrays.asList(Serial.list());
    cp5 = new ControlP5(this);
  cp5.addScrollableList("Serial")
    .setPosition(Serial_Box_x, Serial_Box_y)
    .setSize(100, 400)
    .setBarHeight(30)
    .setItemHeight(30)
    .addItem("None",0)
    .addItems(serial)
    .setFont(font_label1)
    .setLabel("Serial")
    // .setType(ScrollableList.LIST) // currently supported DROPDOWN and LIST
    ;  
} 

int serial_selected = 0;
void Serial(int n) {
  
  if(n == 0){
    try{
      GUI_Serial= 255;
      myPort.stop();
     }catch(Exception e){
     
     }
    return;
  }
  n--;
  // println("serial " + n);
  if(serial_selected == 1){
    serial_selected = 0;
    myPort.stop();
  }
  GUI_Serial = n;
  String portName = Serial.list()[GUI_Serial]; 
  println("Selected port: " + portName);
  try{
    myPort = new Serial(this, portName, 9600);
    println("Port opened");
    serial_selected = 1;
  }catch(Exception e){
    println("Error opening port");
    serial_selected = 0;
     GUI_Serial= 255;
  } 
  
}

int e, f, d;
int r, g, b;
float s_r=0, s_g=0, s_b=0;
int flame, s_flame;
float s_e=0, s_f=0, s_d=0;
int fan = 1;
int first_run = 1;

void draw() {
   background(GUI_Serial);
    
  if(first_run == 1 && GUI_Serial != 255) {

    textSize(32);
    text("Please press the blue button on the robot", 400, height/2); 
    delay(100);
    if (myPort.available() >= 1) {
      String inBuffer = myPort.readStringUntil('\n');
      if (inBuffer!= null)
        if (inBuffer.equals("123\n") == true) {
          first_run = 0 ;
          myPort.write('L');
        }
    }
  }
  else if(GUI_Serial == 255){
    
  }
  if(first_run == 0){
    run_app();
  }

}




void Robot() {
  noFill();
  stroke(0, 255, 255);
  strokeWeight(5);
  ellipseMode(CENTER);
  ellipse(0, 0, robot_size, robot_size);
}

void Sonars(int _e, int _f, int _d) {

  textSize(32);
  fill(255);
 // text("Distance(cm)\nLeft = " + _e + "\nFront = " + _f + "\nRight = " + _d, 400, -50); 

  float multiplier = (float)sonar_scale_max / (float)sonar_distance_max;
  _e *= multiplier;
  _f *= multiplier;
  _d *= multiplier;

  fill(255);
  noStroke();
  rotate(-HALF_PI);
  arc(robot_size/2, 0, _f, _f, -HALF_PI/4, HALF_PI/4);

  rotate(-HALF_PI/2);
  arc(robot_size/2, 0, _e, _e, -HALF_PI/4, HALF_PI/4);

  rotate(HALF_PI);
  arc(robot_size/2, 0, _d, _d, -HALF_PI/4, HALF_PI/4);

  rotate(HALF_PI/2);
}

void Color_Sensor(int _r, int _g, int _b) {
  fill(_r, _g, _b);
  noStroke();
  rectMode(CENTER);
  rect(0, 0, robot_size/2, robot_size/2);
}

void Flame_Sensor(int _f) {
  fill(255, 200, 0);
  noStroke(); 
  rectMode(CORNER);
  //text("Flame sensor: ", width/3-100, height/2-270);
  rect(width/3, height/2, 50, -_f);
  println(_f);  
}

void circArrow(float x, float y, float radius, float start, float stop, float arrowSize) {
  ellipseMode(CENTER);
  noFill();

  arc(x, y, radius * 2, radius * 2, start, stop);

  float arrowX = x + cos(stop) * radius;
  float arrowY = y + sin(stop) * radius;

  float point1X = x + (cos(stop) * radius) + (cos(stop - radians(arrowSize * 5)) * (arrowSize));
  float point1Y = y + (sin(stop) * radius) + (sin(stop - radians(arrowSize * 5)) * (arrowSize));

  float point2X = x + (cos(stop) * radius) + (cos(stop - radians(-arrowSize * 5)) * (-arrowSize));
  float point2Y = y + (sin(stop) * radius) + (sin(stop - radians(-arrowSize * 5)) * (-arrowSize));

  line(arrowX, arrowY, point1X, point1Y);
  line(arrowX, arrowY, point2X, point2Y);
}