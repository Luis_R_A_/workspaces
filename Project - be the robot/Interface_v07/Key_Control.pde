void KeyControl(int _esq[], int _dir[]) {
  int _t_esq = 100, _t_dir = 100;
  int _stop  = 125;
  if (key_UP==1 && key_LEFT==1) {
    _esq[0] = _stop + 50;
    _dir[0] = _stop + _t_esq;
  } else if (key_UP==1 && key_RIGHT==1) {
    _esq[0] = _stop + _t_dir;
    _dir[0] = _stop + 50;
  } else if (key_DOWN==1 && key_LEFT==1) {
    _esq[0] = _stop;
    _dir[0] = _stop - _t_dir;
  } else if (key_DOWN==1 && key_RIGHT==1) {
    _esq[0] = _stop - _t_dir;
    _dir[0] = _stop;
  } else if (key_UP==1) {
    _esq[0] = _stop+_t_esq;
    _dir[0] = _stop+_t_dir;
  } else if (key_DOWN==1) {
    _esq[0] = _stop-_t_esq;
    _dir[0] = _stop-_t_dir;
  } else if (key_LEFT==1) {
    _esq[0] = _stop - (_t_esq/2);
    _dir[0] = _stop + (_t_dir/2);
  } else if (key_RIGHT==1) {
    _esq[0] = _stop + (_t_esq/2);
    _dir[0] = _stop - (_t_dir/2);
  }
}
void keyPressed() {
  if (key == 'f' || key == 'F')
    if (fan == 1) {
      fan = 2;
    } else {
      fan = 1;
    } else if (key == CODED) {
    if (keyCode  == UP) {
      key_UP = 1;
    } else if (keyCode  == DOWN) {
      key_DOWN = 1;
    } else if (keyCode  == LEFT) {
      key_LEFT = 1;
    } else if (keyCode  == RIGHT) {
      key_RIGHT = 1;
    }
  }

  if (key == 'w' || key == 'W') {
    key_UP = 1;
  } else if (key == 's' || key == 'S') {
    key_DOWN = 1;
  } else if (key == 'a' || key == 'A') {
    key_LEFT = 1;
  } else if (key == 'd' || key == 'D') {
    key_RIGHT = 1;
  }
}
void keyReleased() {
  if (key == CODED) {
    if (keyCode  == UP) {
      key_UP = 0;
    }
    if (keyCode  == DOWN) {
      key_DOWN = 0;
    }
    if (keyCode  == LEFT) {
      key_LEFT = 0;
    }
    if (keyCode  == RIGHT) {
      key_RIGHT = 0;
    }
  }

  if (key == 'w' || key == 'W') {
    key_UP = 0;
  } else if (key == 's' || key == 'S') {
    key_DOWN = 0;
  } else if (key == 'a' || key == 'A') {
    key_LEFT = 0;
  } else if (key == 'd' || key == 'D') {
    key_RIGHT = 0;
  }
}