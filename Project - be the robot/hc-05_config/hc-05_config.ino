/* 

Code to program the HC-05.

Remember to hold the button on the hc-05 and then power it on.
Remember to set the serial monitor to "both NL and CR"

change name: "AT+NAME=Be-the-Robot_115200"
change baudrate: "AT+UART=115200,0,0" //in the tutorial it was wrong. This is the real baud= 115200, 1 stop bit, no parity

http://www.instructables.com/id/Modify-The-HC-05-Bluetooth-Module-Defaults-Using-A/step4/Example-HC-05-AT-Commands/
*/

#include <SoftwareSerial.h>

SoftwareSerial BTSerial(2, 4); // RX | TX

void setup()
{
  //pinMode(9, OUTPUT);  // this pin will pull the HC-05 pin 34 (key pin) HIGH to switch module to AT mode
  //digitalWrite(9, HIGH);
  Serial.begin(9600);
  Serial.println("Enter AT commands:");
  BTSerial.begin(38400);  // HC-05 default speed in AT command more
  
}

//  Serial.print("AT+NAME=Be-the-Robot_115200");
//  Serial.print("AT+UART=115200,0,0");
void loop()
{

  // Keep reading from HC-05 and send to Arduino Serial Monitor
  if (BTSerial.available())
    Serial.write(BTSerial.read());

  // Keep reading from Arduino Serial Monitor and send to HC-05
  if (Serial.available())
    BTSerial.write(Serial.read());
}
