void Mouse_control(int _esq[], int _dir[]) {
  if ( Mouse_Pressed == 1) {
    int mouseY_centered = (invertY)*(mouseY - height/2);
    int mouseX_centered = (invertX)*(mouseX - width/2);


    float mouseY_percentage = (float)mouseY_centered / (height / 2); 
    float mouseX_percentage = (float)mouseX_centered / (width / 2);
    float module = sqrt(pow(mouseY_percentage, 2) + pow(mouseX_percentage, 2));

    if (mouseY_percentage >= 0) {
      _esq[0] = (int)(100*module);
      _dir[0] = (int)(100*module);
    } else {
      _esq[0] = -(int)(100*module);
      _dir[0] = -(int)(100*module);
    }
    _esq[0] = _esq[0] + (int)(200 * mouseX_percentage);
    _dir[0] = _dir[0] - (int)(200 * mouseX_percentage);
    /*if (mouseX_centered > 0) {
     _dir[0] = (int)(_dir[0] * (1.0 - mouseX_percentage));
     } else if (mouseX_centered < 0) {
     _esq[0] = (int)(_esq[0] * (1.0 + mouseX_percentage));
     }  */
    _esq[0] += 125;
    _dir[0] += 125;
    if (_esq[0] > 255) 
      _esq[0] = 254;
    else if (_esq[0] < 0)
      _esq[0] = 0;
    if (_dir[0] > 255) 
      _dir[0] = 254;
    else if (_dir[0] < 0)
      _dir[0] = 0;
  } else {
    _esq[0] = 125;
    _dir[0] = 125;
  }
}
void mousePressed() {
  Mouse_Pressed = 1;
  
  if (on_button.MouseIsOver()) {
    if (fan == 1) {
      fan = 2;
    } else {
      fan = 1;
    } 
  }
  
}
void mouseReleased() {
  Mouse_Pressed = 0;
}