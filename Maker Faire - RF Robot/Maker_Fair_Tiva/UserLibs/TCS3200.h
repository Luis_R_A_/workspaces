/*
 * TCS3200.h
 *
 *	This API was made to easily interface with the sensors TCS3200 and TCS230.
 *	It requires the use of the API TimerCapture.
 *
 *	V1.0
 *
 *  Created on: 30/05/2015
 *      Author: Lu�s Afonso
 */

#ifndef TEST_TCS3200_TCS3200_H_
#define TEST_TCS3200_TCS3200_H_

#include <stdint.h>
#include <stdbool.h>
#include "stdlib.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_uart.h"
#include "inc/hw_gpio.h"
#include "inc/hw_timer.h"
#include "inc/hw_types.h"

#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/udma.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/timer.h"

#include "driverlib/fpu.h"
#include <math.h>

#include "TimerCapture.h"
#include "ColorMath.h"

#define RED_FILTER 0
#define GREEN_FILTER 1
#define BLUE_FILTER 2
#define WHITE_FILTER 3

extern uint32_t RGBWraw[4];
extern uint32_t RGBWwhite[4];
extern uint32_t RGBW[4];

uint32_t CapPin;
uint32_t Neutral;



void TCS3200_Init(uint32_t _CapPin,uint32_t _Mhz);
void TCS3200_ColorReading(uint32_t M[4]);

void TCS3200_Calibrate();
uint32_t TCS3200_readTime();
void TCS3200_SetNeutralLimit(uint32_t _value);
uint32_t TCS3200_GetNeutralLimit();
bool TCS3200_Neutral();
void TCS3200_ShowColors();

bool TCS3200_StartColorReading(uint32_t Color);
uint32_t TCS3200_GetColorReading(uint32_t Color);

#endif /* TEST_TCS3200_TCS3200_H_ */
