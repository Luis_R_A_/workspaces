/*
 * ColorMath.h
 *
 *  Created on: 01/06/2015
 *      Author: Lu�s Afonso
 */

#ifndef TEST_TCS3200_COLORMATH_H_
#define TEST_TCS3200_COLORMATH_H_

#include <stdint.h>
#include <stdbool.h>
#include "stdlib.h"
#include <math.h>

void ColorMath_Calibrate(uint32_t M[4]);
void ColorMath_ConvertRGB(uint32_t M[4],uint32_t _invert);
bool ColorMath_IsNeutral(uint32_t M[4]);
void ColorMath_SetNeutralLimit(uint32_t _value);
uint32_t ColorMath_GetNeutralLimit();


#endif /* TEST_TCS3200_COLORMATH_H_ */
