/*
 * SRF04.h
 *
 *  Created on: 07/06/2015
 *      Author: Lu�s Afonso
 */

#ifndef SRF04_H_
#define SRF04_H_

#include <stdint.h>
#include <stdbool.h>
#include "stdlib.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_uart.h"
#include "inc/hw_gpio.h"
#include "inc/hw_timer.h"
#include "inc/hw_types.h"

#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"



#include "TimerCapture.h"

//extern uint32_t reading,newdistance[],distance_isr[];

uint32_t SRF04_GetDistance(uint32_t _number);
void SRF04_Init();
void SRF04_StopSonars();
void SRF04_StartSonars();

extern uint32_t reading,newdistance[2];
#endif /* SRF04_H_ */
