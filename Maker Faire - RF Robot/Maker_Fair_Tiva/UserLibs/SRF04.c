/*
 * SRF04.c
 *
 *  Created on: 07/06/2015
 *      Author: Lu�s Afonso
 */


#include "SRF04.h"


#define pin1 PC4
#define pin2 PC5

uint32_t reading,newdistance[2],distance_isr[2];
uint32_t temp;
uint32_t period;
void SRF04_GPIODISR(){

	uint32_t flags = GPIOIntStatus(GPIO_PORTD_BASE,1);
	GPIOIntClear(GPIO_PORTD_BASE,flags);

	if(reading == 0){
		reading = 1;
		WideTimer_StartReading(pin1,1);
		WideTimer_StartReading(pin2,1);
	}
	else{
		newdistance[0] = 1;
		newdistance[1] = 1;
		distance_isr[0] = WideTimer_GetMicros(pin1)/58;
		temp = WideTimer_GetMicros(pin1)/58;
		distance_isr[1] = WideTimer_GetMicros(pin2)/58;
		WideTimer_StartReading(pin1,1);
		WideTimer_StartReading(pin2,1);
	}



}

uint32_t SRF04_GetDistance(uint32_t _number){


	_number--;
	if(newdistance[_number]==0)
		return 0;

	newdistance[_number] = 0;
	return distance_isr[_number];
}

void SRF04_Init(){


	//Init sonar 1 capture
	WideTimerInit(pin1,80);

	//Init sonar 2 capture
	WideTimerInit(pin2,80);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	GPIOPinTypeTimer(GPIO_PORTD_BASE,GPIO_PIN_6);
	GPIOPinConfigure(GPIO_PD6_WT5CCP0);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER5);
	TimerConfigure(WTIMER5_BASE,TIMER_CFG_SPLIT_PAIR|TIMER_CFG_A_PWM);
	 period = (80000000/(1/0.05))-1;
	TimerLoadSet(WTIMER5_BASE,TIMER_A,period);
	TimerMatchSet(WTIMER5_BASE,TIMER_A,800);

	GPIOIntTypeSet(GPIO_PORTD_BASE,GPIO_PIN_6,GPIO_RISING_EDGE);
	GPIOIntRegister(GPIO_PORTD_BASE,SRF04_GPIODISR);
	GPIOIntEnable(GPIO_PORTD_BASE,GPIO_INT_PIN_6);

	reading = 0;



}

void SRF04_StopSonars(){
	TimerDisable(WTIMER5_BASE,TIMER_A);
}
void SRF04_StartSonars(){
	HWREG(WTIMER5_BASE+ TIMER_O_TAV)=period;
	TimerEnable(WTIMER5_BASE,TIMER_A);
}
