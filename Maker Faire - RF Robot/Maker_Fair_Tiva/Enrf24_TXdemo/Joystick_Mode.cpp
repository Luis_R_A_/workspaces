/*
 * Joystick_Mode.c
 *
 *  Created on: Sep 10, 2015
 *      Author: LuisRA
 */

#include "main.h"

int Y_Value, X_Value, SpeedAxis_Value;
float OffSet;
volatile int STOP = 0;

void Joystick_Mode(){
	GPIOIntDisable(GPIO_PORTF_BASE,GPIO_PIN_4|GPIO_PIN_3|GPIO_PIN_0);
	GPIOIntRegister(GPIO_PORTF_BASE,PortFIntHandler);
	GPIOIntEnable(GPIO_PORTF_BASE,GPIO_PIN_3);
	GPIOIntDisable(GPIO_PORTF_BASE,GPIO_PIN_4|GPIO_PIN_0);
	while(1){
	  Joystick_Interface();
	  radio.print(str_packet);
	  radio.flush();  // Force transmit (don't wait for any more data)

	  radio.deepsleep();

	  delay(40);
	}
}

void Joystick_Interface(){

	if(STOP == 1){
		Joystick_Standby();
	}

	/*
	 * Get ADC Values
	 */
	X_Value = analogRead(Xaxis_pin);
	Y_Value = analogRead(Yaxis_pin);
	SpeedAxis_Value = analogRead(SpeedAxis_pin);

	X_Value = X_Value - 2047;
	Y_Value = Y_Value - 2047; // convert to a scale of -2047 to 2048
	SpeedAxis_Value = SpeedAxis_Value -2047;

	if(SpeedAxis_Value < SpeedAxis_Offset+20 && SpeedAxis_Value > SpeedAxis_Offset-20 &&
			X_Value < X_Offset+20 && X_Value > X_Offset-20){
		//if(X_Value < X_Offset+20 && X_Value > X_Offset-20){
		Base_Speed = 0;
			RightMotor = 255;
			LeftMotor = 255;
		//}
		/*else{
			Base_Speed =  X_Value * MaxSpeed / 2048;
			RightMotor = -Base_Speed;
			LeftMotor = Base_Speed;
		}*/
	}
	else{

		Base_Speed = (SpeedAxis_Value-SpeedAxis_Offset)*MAXIMUM_SPEED/2048;


		OffSet = 1-( (X_Value-X_Offset) / 2048.0);

		if(X_Value > 0){
			RightMotor = Base_Speed * OffSet;
			LeftMotor = Base_Speed;
		}
		else{
			RightMotor = Base_Speed ;
			LeftMotor = Base_Speed  * (1-(OffSet-1))	;
		}
	}


	LoadBuffer(LeftMotor,RightMotor);
	update_counter++;
	if(update_counter > 3){
		Update_Screen_BaseSpeed();
		update_counter = 0;
	}
}


void Joystick_Standby(){
	RightMotor = 255;
	LeftMotor = 255;
	LoadBuffer(LeftMotor,RightMotor);
	for(int i=0; i <30; i++){
		radio.print(str_packet);
		radio.flush();  // Force transmit (don't wait for any more data)
	}
	radio.deepsleep();
	myScreen.clear();
	myScreen.setFont(0);
	myScreen.text(0, 0, "Stop button");
	myScreen.text(0, 1, "pressed!");
	myScreen.text(0, 3, "Wait!");
	delay(1000);
	STOP = 2;
	myScreen.setBacklight(0);
	myScreen.clear();
	myScreen.text(0, 0, "Standing by");
	myScreen.text(0, 2, "Press the");
	myScreen.text(0, 3, " button again");
	while(STOP == 2){
		    SpeedAxis_Value = analogRead(SpeedAxis_pin);

			X_Value = X_Value - 2047;
			Y_Value = Y_Value - 2047; // convert to a scale of -2047 to 2048
			SpeedAxis_Value = SpeedAxis_Value -2047;

			if(SpeedAxis_Value < SpeedAxis_Offset+20 && SpeedAxis_Value > SpeedAxis_Offset-20){
				Base_Speed = 0;

			}
			else{

				Base_Speed = (SpeedAxis_Value-SpeedAxis_Offset)*MAXIMUM_SPEED/2048;
			}
			update_counter++;
			if(update_counter > 5){
				  myScreen.clear();
				  myScreen.text(0, 0, "Standing by");
				  myScreen.text(0, 2, "Press the");
				  myScreen.text(0, 3, " button again");
				  myScreen.text(0, 4, "Speed:");
					 char a[] = "+999";
					 if(Base_Speed <0){
						 Base_Speed = Base_Speed * (-1);
						 a[0] = '-';
					 }
					 else{
						 a[0] = '+';
					 }

					 for(int i=3; i >= 1; i--){
						a[i]= (Base_Speed%10)+48;
						Base_Speed = Base_Speed /10;
					 }
					 myScreen.text(0, 5, a);
				 update_counter = 0;
			}
			delay(10);

	  }
	  delay(500);
	  myScreen.setBacklight(1);
	  STOP = 0;
}

void Update_Screen_BaseSpeed(){
	  myScreen.clear();

	 myScreen.setFont(0);
	 myScreen.text(0, 0, "Base speed:");


	 char a[] = "+999";
	 if(Base_Speed <0){
		 Base_Speed = Base_Speed * (-1);
		 a[0] = '-';
		 myScreen.text(0, 1, "Backwards");
	 }
	 else{
		 a[0] = '+';
		 myScreen.text(0, 1, "Forward");
	 }
	 myScreen.text(0, 5, "-120 to 120");
	 myScreen.setFont(1);
	 for(int i=3; i >= 1; i--){
		a[i]= (Base_Speed%10)+48;
		Base_Speed = Base_Speed /10;
	 }
	 //itoa(Base_Speed,a,10);
	 //sprintf(a,"Speed: %d",Base_Speed);
	 myScreen.text(0, 3, a);
}

void PortFIntHandler(){
	  //GPIOIntDisable(GPIO_PORTF_BASE, GPIO_INT_PIN_4|GPIO_INT_PIN_0);
	  uint32_t status = GPIOIntStatus(GPIO_PORTF_BASE,true);
	  GPIOIntClear(GPIO_PORTF_BASE,status);

	  if(STOP == 2){
		  STOP = 3;
	  }
	  else
		  STOP = 1;

}
/*
 *
 *  Pot controls maximum top speed
 *  Joystick x controls direction
 *  Joystick y controls current speed
 */
//
//int Y_Value, X_Value, RightMotor, LeftMotor, Base_Speed, SpeedAxis_Value;
//float OffSet;
//const int X_Offset = 54;
//const int Y_Offset = 24;
//const int SpeedAxis_Offset = 44;
//const int MAXIMUM_SPEED = 120;
//void Joystick_Interface(){
//
//
//
//	/*
//	 * Get ADC Values
//	 */
//	X_Value = analogRead(Xaxis_pin);
//	Y_Value = analogRead(Yaxis_pin);
//	SpeedAxis_Value = analogRead(SpeedAxis_pin);
//
//	X_Value = X_Value - 2047;
//	Y_Value = Y_Value - 2047; // convert to a scale of -2047 to 2048
//	//SpeedAxis_Value = SpeedAxis_Value -2047;
//
//	if(Y_Value < Y_Offset+20 && Y_Value > Y_Offset-20 && X_Value < X_Offset+20 && X_Value > X_Offset-20){
//		//if(X_Value < X_Offset+20 && X_Value > X_Offset-20){
//			RightMotor = 255;
//			LeftMotor = 255;
//		//}
//		/*else{
//			Base_Speed =  X_Value * MaxSpeed / 2048;
//			RightMotor = -Base_Speed;
//			LeftMotor = Base_Speed;
//		}*/
//	}
//	else{
//
//		if(SpeedAxis_Value-SpeedAxis_Offset > 0)
//			MaxSpeed = (SpeedAxis_Value-SpeedAxis_Offset)*MAXIMUM_SPEED/4095;
//		else
//			MaxSpeed=0;
//
//		if(Y_Value-Y_Offset < 0)
//			Base_Speed = (-1)*( sqrtf(powf(Y_Value,2) + powf(X_Value,2)) ) *MaxSpeed/2048; // Convert to the motor speed scale
//		else
//			Base_Speed = ( sqrtf(powf(Y_Value,2) + powf(X_Value,2)) ) *MaxSpeed/2048; // Convert to the motor speed scale
//
//
//		OffSet = 1-( (X_Value-X_Offset) / 2048.0);
//
//		if(X_Value > 0){
//			RightMotor = Base_Speed * OffSet;
//			LeftMotor = Base_Speed;
//		}
//		else{
//			RightMotor = Base_Speed ;
//			LeftMotor = Base_Speed  * (1-(OffSet-1))	;
//		}
//	}
//
//
//	LoadBuffer(LeftMotor,RightMotor);
//
//}
