/*
 * main.h
 *
 *  Created on: Sep 10, 2015
 *      Author: LuisRA
 */

#ifndef MAIN_H_
#define MAIN_H_

#include "Energia.h"
#include <Enrf24.h>
#include <nRF24L01.h>
#include <string.h>
#include <SPI.h>
#include <LCD_5110_SPI.h>

#include <stdio.h>
#include "driverlib/timer.h"
#include "driverlib/systick.h"
#include "inc/hw_timer.h"




#define Xaxis_pin A9
#define Yaxis_pin A8
#define SpeedAxis_pin A2
#define Joystick_Button PF_3

#define Tilt_Xpin A7
#define Tilt_Ypin A6
#define Tilt_Zpin A5

const int X_Offset = 54;
const int Y_Offset = 24;
const int SpeedAxis_Offset = 44;
const int MAXIMUM_SPEED = 120;

extern Enrf24 radio;  // P2.0=CE, P2.1=CSN, P2.2=IRQ
const uint8_t txaddr[] = { 0x17, 0x08, 0x07, 0xDE, 0x01 };

/* Global variables */
extern int Base_Speed, RightMotor, LeftMotor;
extern int update_counter;

/* Array of chars holding the packet to be sent */
extern char str_packet[];

/* Important volatile variable that signals the main code when the stop button is pressed */
extern volatile int STOP;

/* LCD object */
extern LCD_5110_SPI myScreen;

/*
 * Pot controls current speed
 * Joystick x controls direction
 * Joystick y does nothing
 * Joystick button stops and re-starts robot.
 */
void dump_radio_status_to_serialport(uint8_t);
void LoadBuffer(int _LeftMotor, int _RightMotor);
void PortFIntHandler();
void Update_Screen_BaseSpeed();
//void Timer_Init();

/* Joystick_Mode.cpp functions */
void Joystick_Mode();
void Joystick_Interface();
void Joystick_Standby();

/* Tilt_Mode.cpp functions */
void Tilt_Mode();
void Tilt_Control();
void Update_Screen_Tilt();

#endif /* MAIN_H_ */
