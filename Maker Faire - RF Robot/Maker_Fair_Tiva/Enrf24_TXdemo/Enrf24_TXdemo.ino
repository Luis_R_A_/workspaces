/*
 *
 *
 * For the accelerometer http://letsmakerobots.com/node/35484
 */

#include "main.h"

LCD_5110_SPI myScreen(PA_3,PA_4,PA_2,PB_2,0);
Enrf24 radio(PF_1, PE_3, PE_2);

int Base_Speed, RightMotor, LeftMotor;
int update_counter = 0;

char str_packet[] = {15, 3, 1, 255, 1, 255, '\0' };

volatile int Mode_Set = 0, Mode_Ready = 0;

void setup() {


 /* Configure acellerometer inputs */
  pinMode(Tilt_Xpin,INPUT);
  pinMode(Tilt_Ypin,INPUT);
  pinMode(Tilt_Zpin,INPUT);

  /*Configure buttons */
  pinMode(PF_4, INPUT_PULLUP);
  pinMode(Joystick_Button, INPUT_PULLUP);
  pinMode(PF_0, INPUT_PULLUP);

  /*Set buttons interrupts */
  GPIOIntTypeSet(GPIO_PORTF_BASE,GPIO_PIN_4,GPIO_FALLING_EDGE);
  GPIOIntTypeSet(GPIO_PORTF_BASE,GPIO_PIN_3,GPIO_FALLING_EDGE);
  GPIOIntTypeSet(GPIO_PORTF_BASE,GPIO_PIN_0	,GPIO_FALLING_EDGE);
  GPIOIntRegister(GPIO_PORTF_BASE,MenuISR);
  uint32_t status = GPIOIntStatus(GPIO_PORTF_BASE,true);
  GPIOIntClear(GPIO_PORTF_BASE,status);
  //GPIOIntEnable(GPIO_PORTF_BASE,GPIO_PIN_4);

  /* Configure joystick inputs */
  pinMode(Xaxis_pin,INPUT);
  pinMode(Yaxis_pin,INPUT);
  pinMode(SpeedAxis_pin,INPUT);

  /* Start Serial */
  //Serial.begin(9600);

  /* Initialize SPI module 2
   *
   * CLK = 4Mhz (limited by nokia 5510 screen - transceivar max = 30Mhz)
   *
   *  */
  SPI.setModule(2);
  SPI.setClockDivider(SPI_CLOCK_DIV128);
  SPI.begin();
  SPI.setDataMode(SPI_MODE0);
  SPI.setBitOrder(MSBFIRST);

  /* Initalize screen */
  myScreen.begin();

  /* Mode Selection */
  Mode_Select();



  radio.begin(1000000,005);  // Defaults 1Mbps, channel 0, max TX power
 // dump_radio_status_to_serialport(radio.radioState());

  radio.setTXaddress((void*)txaddr);

}


void loop() {

	if(Mode_Set == 1)
		Joystick_Mode();
	else
		Tilt_Mode();
}



void  Mode_Select(){
	GPIOIntEnable(GPIO_PORTF_BASE,GPIO_PIN_4|GPIO_PIN_3|GPIO_PIN_0);

	Mode_Ready = 0;
	Mode_Set = 0;

	 myScreen.clear();
	 myScreen.setBacklight(1);
	 myScreen.setFont(1);
	 myScreen.text(0, 0, "Hi!");
	 delay(1000);
	 myScreen.clear();
	 myScreen.setFont(0);
	 int toggle = 0;
	 while(Mode_Ready == 0){
		 myScreen.clear();
		 myScreen.text(0, 0, "Select mode:");
		 if(Mode_Set == 0)
		 {
			 if(toggle == 0)
				 myScreen.text(0, 2, "(Tilt) <--");
			 else
				 myScreen.text(0, 2, " Tilt");
			 toggle ^= 1;
			 myScreen.text(0, 3, " Joystick");
		 }
		 if(Mode_Set == 1)
		 {
			 if(toggle > 0)
				 myScreen.text(0, 3, "(Joystick) <--");
			 else
				 myScreen.text(0, 3, " Joystick");
			 toggle ^= 1;
			 myScreen.text(0, 2, " Tilt");
		 }

		 delay(500);
	 }

	  /* LCD showing starting sequence */
	  myScreen.setBacklight(1);
	  myScreen.clear();
	  if(Mode_Set==1)
		  myScreen.text(0, 0, "Joystick:");
	  else
		  myScreen.text(0, 0, "Tilt:");
	  myScreen.text(0, 1, "Starting in:");
	  myScreen.setFont(1);
	  myScreen.text(2, 2, "3");
	  delay(1000);
	  myScreen.clear();
	  myScreen.setFont(0);
	  if(Mode_Set==1)
		  myScreen.text(0, 0, "Joystick:");
	  else
		  myScreen.text(0, 0, "Tilt:");
	  myScreen.text(0, 1, "Starting in:");
	  myScreen.setFont(1);
	  myScreen.text(2, 2, "2");
	  delay(1000);
	  myScreen.clear();
	  myScreen.setFont(0);
	  if(Mode_Set==1)
		  myScreen.text(0, 0, "Joystick:");
	  else
		  myScreen.text(0, 0, "Tilt:");
	  myScreen.text(0, 1, "Starting in:");
	  myScreen.setFont(1);
	  myScreen.text(2, 2, "1");
	  delay(1000);
	  myScreen.clear();
}
void dump_radio_status_to_serialport(uint8_t status)
{
  Serial.print("Enrf24 radio transceiver status: ");
  switch (status) {
    case ENRF24_STATE_NOTPRESENT:
      Serial.println("NO TRANSCEIVER PRESENT");
      break;

    case ENRF24_STATE_DEEPSLEEP:
      Serial.println("DEEP SLEEP <1uA power consumption");
      break;

    case ENRF24_STATE_IDLE:
      Serial.println("IDLE module powered up w/ oscillators running");
      break;

    case ENRF24_STATE_PTX:
      Serial.println("Actively Transmitting");
      break;

    case ENRF24_STATE_PRX:
      Serial.println("Receive Mode");
      break;

    default:
      Serial.println("UNKNOWN STATUS CODE");
  }
}


void LoadBuffer(int _LeftMotor, int _RightMotor){
	int direction = 1;
	if(_LeftMotor < 0){
		_LeftMotor = _LeftMotor*(-1);
		direction = 2;
	}
	str_packet[2] = direction;
	str_packet[3] = _LeftMotor;

	direction = 1;
	if(_RightMotor < 0){
		_RightMotor = _RightMotor*(-1);
		direction = 2;
	}
	str_packet[4] = direction;
	str_packet[5] = _RightMotor;

}

void MenuISR(){
	  //GPIOIntDisable(GPIO_PORTF_BASE, GPIO_INT_PIN_4|GPIO_INT_PIN_0);
	  uint32_t status = GPIOIntStatus(GPIO_PORTF_BASE,true);
	  GPIOIntClear(GPIO_PORTF_BASE,status);

	  if( (status&GPIO_INT_PIN_3)==GPIO_INT_PIN_3){
		  Mode_Ready=1;
	  }
	  else{
		  Mode_Set ^= 1;
	  }

}






//void WakeUp(){
//	uint32_t status = TimerIntStatus(WTIMER4_BASE,true);
//	TimerIntClear(WTIMER4_BASE,status);
//}
//
//void Timer_Init(){
//
//	  //We set the load value so the timer interrupts each 1ms
//	  uint32_t Period;
//	 // Period = 80000000/25; //every 40ms
//	  Period = 1000000/25; //every 40ms
//
//	  SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER4);
//	  SysCtlDelay(3);
//	  /*
//	    Configure the timer as periodic, by omission it's in count down mode.
//	    It counts from the load value to 0 and then resets back to the load value.
//	  REMEMBER: You need to configure the timer before setting the load and match
//	  */
//	  TimerConfigure(WTIMER4_BASE, TIMER_CFG_A_ONE_SHOT_UP);
//	  TimerLoadSet(WTIMER4_BASE, TIMER_A, Period -1);
//
//	  TimerIntRegister(WTIMER4_BASE, TIMER_A, WakeUp);
//
//	  /*
//	    Enable the timeout interrupt. In count down mode it's when the timer reaches
//	  0 and resets back to load. In count up mode it's when the timer reaches load
//	  and resets back to 0.
//	  */
//	  TimerIntEnable(WTIMER4_BASE, TIMER_TIMA_TIMEOUT);
//
//	  //TimerEnable(WTIMER4_BASE, TIMER_A);
//
//}



/*
 * Test with 2 buttons
 */
/*
if(str_packet[3] ==255)
  str_packet[3]=0;
if(str_packet[5] ==255)
  str_packet[5]=0;
if(digitalRead(PF_4) == 0){
  str_packet[3] += 30;
}
else if(str_packet[3]!=0){
  str_packet[3] -= 10;
}

if(digitalRead(PF_0) == 0){
  str_packet[5] += 30;
}
else if(str_packet[5]!=0){
  str_packet[5] -= 10;
}

if(str_packet[3] > 30)
  str_packet[3]=30;
else if(str_packet[3] <=0)
  str_packet[3]=255;

if(str_packet[5] > 30)
  str_packet[5]=30;
else if(str_packet[5] <=0)
  str_packet[5]=255;


if(str_packet[5] ==255 && str_packet[3] ==255)
idle++;
else
  idle=0;

if(idle > 100){

  for(int i=0; i < 30; i++){
	  radio.print(str_packet);
	  radio.flush();
  }


  radio.deepsleep();
  SysTickIntDisable();
  GPIOIntEnable(GPIO_PORTF_BASE, GPIO_INT_PIN_4|GPIO_INT_PIN_0);
  SysCtlDeepSleep();
  SysTickIntEnable();
  idle=0;
  //while(digitalRead(PF_0) != 0 && digitalRead(PF_4) != 0);

}*/
