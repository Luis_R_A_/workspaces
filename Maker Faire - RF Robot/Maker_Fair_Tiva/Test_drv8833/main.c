/*
 * 	This template is for the TM4C launchpad MCU, the TM4C123GH6PM.
 * 	The clock is alredy configured for 80Mhz
 * 	It has alredy TivaWare included.
 *
 * 	You can use it for any TM4C123 series MCU if you change the PART define and variant in the configs.
 * 	For TM4C1294 series it needs a modification on the CPU clock setup, SysCtlClockSet
 * can't be used, instead use SysCtlClockFreqSet.
 * 	Also since this is for the launchpad, if you use a different debugger/programmer, change that on the
 * configs too
 *
 *
 * 	It comes alredy with various includes for some driverlib source files.
 *
 * 	It comes with the system tick set up to call a interrupt every 1mS.
 * 	The interrupt increments the variable millis by 1 every time it's called.
 * 	A delay function "Wait" is implemented for making more precise delays than SysCtlDelay.
 *
 * 	This was made not only for my personal use but also for my Tiva Workshop.
 *
 * 	Lu�s Afonso
 *
 */

#include "main.h"


int32_t dutyA,dutyB;

void EncoderInit(){
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI0);
	SysCtlDelay(1);

	HWREG(GPIO_PORTD_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
	HWREG(GPIO_PORTD_BASE + GPIO_O_CR) |= 0x80;

	//Set Pins to be PHA0 and PHB0
	GPIOPinConfigure(GPIO_PD6_PHA0);
	GPIOPinConfigure(GPIO_PD7_PHB0);

	GPIOPinTypeQEI(GPIO_PORTD_BASE, GPIO_PIN_6 |  GPIO_PIN_7);


	QEIDisable(QEI0_BASE);
	QEIIntDisable(QEI0_BASE,QEI_INTERROR | QEI_INTDIR | QEI_INTTIMER | QEI_INTINDEX);

	QEIConfigure(QEI0_BASE, (QEI_CONFIG_CAPTURE_A_B  | QEI_CONFIG_NO_RESET 	| QEI_CONFIG_QUADRATURE | QEI_CONFIG_NO_SWAP), 1000);
	QEIVelocityConfigure(QEI0_BASE,	QEI_VELDIV_1,80000000/1);
	QEIEnable(QEI0_BASE);
	QEIVelocityEnable(QEI0_BASE);
	//Set position to a middle value so we can see if things are working
	QEIPositionSet(QEI0_BASE, 500);


	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI1);
	SysCtlDelay(1);

	//Set Pins to be PHA0 and PHB0
	GPIOPinConfigure(GPIO_PC5_PHA1);
	GPIOPinConfigure(GPIO_PC6_PHB1);

	GPIOPinTypeQEI(GPIO_PORTC_BASE, GPIO_PIN_5 |  GPIO_PIN_6);


	QEIDisable(QEI1_BASE);
	QEIIntDisable(QEI1_BASE,QEI_INTERROR | QEI_INTDIR | QEI_INTTIMER | QEI_INTINDEX);

	QEIConfigure(QEI1_BASE, (QEI_CONFIG_CAPTURE_A_B  | QEI_CONFIG_NO_RESET 	| QEI_CONFIG_QUADRATURE | QEI_CONFIG_SWAP), 1000);
	QEIVelocityConfigure(QEI1_BASE,	QEI_VELDIV_1,80000000/1);
	QEIEnable(QEI1_BASE);
	QEIVelocityEnable(QEI1_BASE);
	//Set position to a middle value so we can see if things are working
	QEIPositionSet(QEI1_BASE, 500);
}

int32_t EncoderDir, EncoderEsq, EncoderDirDirection,EncoderEsqDirection,EncoderDirVel,EncoderEsqVel;
void TestA(){
	  DRV8833_InitMotorA();
	  dutyA=0;
	  DRV8833_MotorA(0);

	  int32_t temp = dutyA;
	  while(1)
	  {
		  if(dutyA != temp){
			  DRV8833_MotorA(dutyA);
			  temp = dutyA;
		  }

			EncoderEsq = QEIPositionGet(QEI0_BASE);
			EncoderDir = QEIPositionGet(QEI1_BASE);
			EncoderDirDirection=QEIDirectionGet(QEI1_BASE);
			EncoderEsqDirection=QEIDirectionGet(QEI0_BASE);
			EncoderDirVel=(QEIVelocityGet(QEI1_BASE)*13.188)/48;
			EncoderEsqVel=(QEIVelocityGet(QEI0_BASE)*13.188)/48;

	  }

}
void TestB(){
	  DRV8833_InitMotorB();
	  dutyA=0;
	  DRV8833_MotorB(0);

	  int32_t temp = dutyB;
	  while(1)
	  {
		  if(dutyB != temp){
			  DRV8833_MotorB(dutyB);
			  temp = dutyB;
		  }



	  }

}

void TestBoth(){
	DRV8833_InitMotorsAB();
	  dutyA=0;
	  DRV8833_MotorB(0);

	  int32_t tempA = dutyA;
	  int32_t tempB = dutyB;
	  while(1)
	  {

		  if(dutyA != tempA){
			  DRV8833_MotorsDuty(dutyA,dutyA);
			  tempA=dutyA;
		  }



		  /*if(dutyA != tempA){
			  DRV8833_MotorA(dutyA);
			  tempA = dutyA;
		  }
		  if(dutyB != tempB){
			  DRV8833_MotorB(dutyB);
			  tempB = dutyB;
		  }*/




	  }

}
int main(){

     /*
      * Sets clock to 80Mhz and sets up the systick counter to count in milliseconds.
      */
	  Template2_Init();

	  EncoderInit();
	  TestB();
	 // TestBoth();

	  while(1){

	  }
}

