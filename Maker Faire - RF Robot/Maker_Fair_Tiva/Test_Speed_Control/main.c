/*
 * 	This template is for the TM4C launchpad MCU, the TM4C123GH6PM.
 * 	The clock is alredy configured for 80Mhz
 * 	It has alredy TivaWare included.
 *
 * 	You can use it for any TM4C123 series MCU if you change the PART define and variant in the configs.
 * 	For TM4C1294 series it needs a modification on the CPU clock setup, SysCtlClockSet
 * can't be used, instead use SysCtlClockFreqSet.
 * 	Also since this is for the launchpad, if you use a different debugger/programmer, change that on the
 * configs too
 *
 *
 * 	It comes alredy with various includes for some driverlib source files.
 *
 * 	It comes with the system tick set up to call a interrupt every 1mS.
 * 	The interrupt increments the variable millis by 1 every time it's called.
 * 	A delay function "Wait" is implemented for making more precise delays than SysCtlDelay.
 *
 * 	This was made not only for my personal use but also for my Tiva Workshop.
 *
 * 	Lu�s Afonso
 *
 */

#include "main.h"


int32_t dutyA,dutyB;


void TestA(){
	  DRV8833_InitMotorA();
	  dutyA=0;
	  DRV8833_MotorA(0);

	  int32_t temp = dutyA;
	  while(1)
	  {
		  if(dutyA != temp){
			  DRV8833_MotorA(dutyA);
			  temp = dutyA;
		  }



	  }

}
void TestB(){
	  DRV8833_InitMotorB();
	  dutyA=0;
	  DRV8833_MotorB(0);

	  int32_t temp = dutyB;
	  while(1)
	  {
		  if(dutyB != temp){
			  DRV8833_MotorB(dutyB);
			  temp = dutyB;
		  }



	  }

}

void TestBoth(){
	DRV8833_InitMotorsAB();
	  dutyA=0;
	  DRV8833_MotorB(0);

	  int32_t tempA = dutyA;
	  //int32_t tempB = dutyB;
	  while(1)
	  {

		  if(dutyA != tempA){
			  DRV8833_MotorsDuty(dutyA,dutyA);
			  tempA=dutyA;
		  }



		  /*if(dutyA != tempA){
			  DRV8833_MotorA(dutyA);
			  tempA = dutyA;
		  }
		  if(dutyB != tempB){
			  DRV8833_MotorB(dutyB);
			  tempB = dutyB;
		  }*/




	  }

}



int32_t speedleft = 0, speedright=0, speedboth=70;
int main(){

     /*
      * Sets clock to 80Mhz and sets up the systick counter to count in milliseconds.
      */
	  Template2_Init();


	  SpeedControl_Init();



	  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	  SysCtlDelay(3);
	  GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);
	  GPIOPadConfigSet(GPIO_PORTF_BASE ,GPIO_PIN_4,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);

	  int value =GPIOPinRead(GPIO_PORTF_BASE,GPIO_PIN_4);
	  while(value){
		  value =GPIOPinRead(GPIO_PORTF_BASE,GPIO_PIN_4);
	  }
	 // TestA();
	 // TestBoth();
	  Wait(2000);
	  while(1){
		 // SpeedControl_Set(speedleft,speedright);
		  SpeedControl_Set(speedboth,speedboth);
	  }
}

