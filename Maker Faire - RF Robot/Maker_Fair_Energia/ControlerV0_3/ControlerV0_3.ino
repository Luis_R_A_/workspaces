/*
 *
 *
 * For the accelerometer http://letsmakerobots.com/node/35484
 */


#include "main.h"

LCD_5110_SPI myScreen(12,13,11,19,0);
Enrf24 radio(NRF24_CE_PIN, NRF24_CSN_PIN, NRF24_IRQ_PIN);

int Base_Speed, RightMotor, LeftMotor;
int update_counter = 0;

char str_packet[] = {
  15, 3, 1, 255, 1, 255, '\0' };

volatile int Mode_Set = 0, Mode_Ready = 0;

void setup() {


  /* Configure acellerometer inputs */
  pinMode(Tilt_Xpin,INPUT);
  pinMode(Tilt_Ypin,INPUT);
  pinMode(Tilt_Zpin,INPUT);

  /*Configure buttons */
  pinMode(Button1_pin, INPUT_PULLUP);
  pinMode(Joystick_Button, INPUT_PULLUP);
  pinMode(Button2_pin, INPUT_PULLUP);

  /*Set buttons interrupts */
  MAP_GPIO_clearInterruptFlag(GPIO_PORT_P3, GPIO_PIN5);
  MAP_GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN5);
  MAP_GPIO_interruptEdgeSelect(GPIO_PORT_P3,GPIO_PIN5,GPIO_HIGH_TO_LOW_TRANSITION);
  MAP_GPIO_registerInterrupt(GPIO_PORT_P3,DownButton_ISR);
  // MAP_Interrupt_enableInterrupt(INT_PORT3);
  /*Set buttons interrupts */
  MAP_GPIO_clearInterruptFlag(GPIO_PORT_P5, GPIO_PIN1);
  MAP_GPIO_enableInterrupt(GPIO_PORT_P5, GPIO_PIN1);
  MAP_GPIO_interruptEdgeSelect(GPIO_PORT_P5,GPIO_PIN1,GPIO_HIGH_TO_LOW_TRANSITION);
  MAP_GPIO_registerInterrupt(GPIO_PORT_P5,UpButton_ISR);
  //MAP_Interrupt_enableInterrupt(INT_PORT5);

  MAP_GPIO_clearInterruptFlag(GPIO_PORT_P4, GPIO_PIN1);
  MAP_GPIO_enableInterrupt(GPIO_PORT_P4, GPIO_PIN1);
  MAP_GPIO_interruptEdgeSelect(GPIO_PORT_P4,GPIO_PIN1,GPIO_HIGH_TO_LOW_TRANSITION);
  MAP_GPIO_registerInterrupt(GPIO_PORT_P4,Select_ISR);
  //MAP_Interrupt_enableInterrupt(INT_PORT4);

  /* Configure joystick inputs */
  pinMode(Xaxis_pin,INPUT);
  pinMode(Yaxis_pin,INPUT);
  pinMode(SpeedAxis_pin,INPUT);

  /* Start Serial */
  //Serial.begin(9600);

  /* Initialize SPI module 2
   *
   * CLK = 4Mhz (limited by nokia 5510 screen - transceivar max = 30Mhz)
   *
   *  */
  //SPI.setModule(2);
  //SPI.setClockDivider(SPI_CLOCK_DIV128);
  SPI.begin();
  SPI.setDataMode(SPI_MODE0);
  SPI.setBitOrder(MSBFIRST);

  /* Initalize screen */
  myScreen.begin();

  /* Mode Selection */
  Mode_Select();



  radio.begin(1000000,005);  // Defaults 1Mbps, channel 0, max TX power
  // dump_radio_status_to_serialport(radio.radioState());

  radio.setTXaddress((void*)txaddr);

}


void loop() {

  if(Mode_Set == 0)
    Tilt_Mode();
  else if(Mode_Set == 1)
    Tilt2_Mode();
  else if(Mode_Set == 2)
    Joystick1_Mode();
  else if(Mode_Set == 3)
    Joystick2_Mode();

}



void  Mode_Select(){
  MAP_Interrupt_enableInterrupt(INT_PORT3|INT_PORT4|INT_PORT5);


  Mode_Ready = 0;
  Mode_Set = 0;

  myScreen.clear();
  myScreen.setBacklight(1);
  myScreen.setFont(1);
  myScreen.text(0, 0, "Hi!");
  delay(1000);
  myScreen.clear();
  myScreen.setFont(0);
  int toggle = 0;
  while(Mode_Ready == 0){
    myScreen.clear();
    myScreen.text(0, 0, "Select mode:");
    if(Mode_Set == 0)
    {
      if(toggle == 0)
        myScreen.text(0, 2, "(Tilt) <--");
      else
        myScreen.text(0, 2, " Tilt");
      toggle ^= 1;
      myScreen.text(0, 3, " Tilt2");
      myScreen.text(0, 4, " Joystick1");
      myScreen.text(0, 5, " Joystick2");

    }
    if(Mode_Set == 1)
    {
      if(toggle > 0)
        myScreen.text(0, 3, "(Tilt2) <--");
      else
        myScreen.text(0, 3, " Tilt2");
      toggle ^= 1;
      myScreen.text(0, 2, " Tilt");
      myScreen.text(0, 4, " Joystick1");
      myScreen.text(0, 5, " Joystick2");
    }
    if(Mode_Set == 2)
    {
      if(toggle > 0)
        myScreen.text(0, 4, "(Joystick1) <--");
      else
        myScreen.text(0, 4, " Joystick1");
      toggle ^= 1;
      myScreen.text(0, 2, " Tilt");
      myScreen.text(0, 3, " Tilt2");
      myScreen.text(0, 5, " Joystick2");
    }
    if(Mode_Set == 3)
    {
      if(toggle > 0)
        myScreen.text(0, 5, "(Joystick2) <--");
      else
        myScreen.text(0, 5, " Joystick2");
      toggle ^= 1;
      myScreen.text(0, 2, " Tilt");
      myScreen.text(0, 3, " Tilt2");
      myScreen.text(0, 4, " Joystick1");
    }
    delay(500);
  }
  MAP_Interrupt_disableInterrupt(INT_PORT3|INT_PORT4|INT_PORT5);


  /* LCD showing starting sequence */
  myScreen.setBacklight(1);
  myScreen.clear();
  myScreen.text(0, 1, "Starting in:");
  myScreen.setFont(1);
  myScreen.text(2, 2, "3");
  delay(1000);
  myScreen.clear();
  myScreen.setFont(0);
  myScreen.text(0, 1, "Starting in:");
  myScreen.setFont(1);
  myScreen.text(2, 2, "2");
  delay(1000);
  myScreen.clear();
  myScreen.setFont(0);
  myScreen.text(0, 1, "Starting in:");
  myScreen.setFont(1);
  myScreen.text(2, 2, "1");
  delay(1000);
  myScreen.clear();
}


void LoadBuffer(int _LeftMotor, int _RightMotor){
  int direction = 1;
  if(_LeftMotor < 0){
    _LeftMotor = _LeftMotor*(-1);
    direction = 2;
  }
  str_packet[2] = direction;
  str_packet[3] = _LeftMotor;

  direction = 1;
  if(_RightMotor < 0){
    _RightMotor = _RightMotor*(-1);
    direction = 2;
  }
  str_packet[4] = direction;
  str_packet[5] = _RightMotor;

}

//Port3
void DownButton_ISR(){
  uint32_t status;

  status = MAP_GPIO_getEnabledInterruptStatus(GPIO_PORT_P3);
  MAP_GPIO_clearInterruptFlag(GPIO_PORT_P3, status);
  if(Mode_Set == 0)
    Mode_Set = 3;
  else
    Mode_Set--;

}
//Port4
void Select_ISR(){
  uint32_t status;

  status = MAP_GPIO_getEnabledInterruptStatus(GPIO_PORT_P4);
  MAP_GPIO_clearInterruptFlag(GPIO_PORT_P4, status);

  Mode_Ready=1;

}
//Port5
void UpButton_ISR(){
  uint32_t status;

  status = MAP_GPIO_getEnabledInterruptStatus(GPIO_PORT_P5);
  MAP_GPIO_clearInterruptFlag(GPIO_PORT_P5, status);
  if(Mode_Set == 3)
    Mode_Set = 0;
  else
    Mode_Set++;

}












