/*
 * Readings.cpp
 *
 *  Created on: Sep 12, 2015
 *      Author: LuisRA
 */

#include "main.h"


int Get_Pot_Speed(int _HalfScale){
	int SpeedAxis_Value = analogRead(SpeedAxis_pin);
	int _Base_Speed = 0;
	if(_HalfScale == 0){



		if(SpeedAxis_Value < 0+20){
			return _Base_Speed;
		}
		else{

			_Base_Speed = (SpeedAxis_Value-SpeedAxis_Offset)*MAXIMUM_SPEED/4095;
		}
	}
	else{
		 SpeedAxis_Value = analogRead(SpeedAxis_pin) -2047;

		if(SpeedAxis_Value < SpeedAxis_Offset+20 && SpeedAxis_Value > SpeedAxis_Offset-20){
			return _Base_Speed;
		}
		else{

			_Base_Speed = (SpeedAxis_Value-SpeedAxis_Offset)*MAXIMUM_SPEED/2048;
		}
	}
	return _Base_Speed;

}


float Get_JoyX_Percentage(){
	int _X_Value = analogRead(Xaxis_pin)  - 2047;
	float _OffSet = 1-( (_X_Value-X_Offset) / 2048.0);

	return _OffSet;
}

int Get_JoyY_Speed(int _MaxSpeed){
	int _Y = analogRead(Yaxis_pin) -2047;

	int _Speed = 0;
	if(_Y < Y_Offset+20 && _Y > Y_Offset-20){
		return _Speed;
	}
	else{

		_Speed = (_Y-Y_Offset)*_MaxSpeed/2048;
	}
	return _Speed;
}

int Get_Joy_Radius_Speed(int _MaxSpeed){

	int32_t Y_ = analogRead(Yaxis_pin) - 2047;

	int32_t X_ = analogRead(Xaxis_pin)  - 2047;

	int32_t _Radius = sqrtf((Y_*Y_) + (X_*X_));

	int _Speed = 0;
	if(Y_ < Y_Offset+20 && Y_ > Y_Offset-20 && X_ < X_Offset+20 && X_ > X_Offset-20){
		return _Speed;
	}
	else{

		if(Y_ > 0)
			_Speed = (_Radius)*_MaxSpeed/2048;
		else
			_Speed = (-1)*(_Radius)*_MaxSpeed/2048;
	}

	return _Speed;
}

int Get_Tilt_Speed(){

	int _Tilt_Y_Speed = (analogRead(Tilt_Ypin) - Tilt_Y_Calibrate) * Tilt_MaxSpeed / Tilt_Y_MaxVariation;

	return _Tilt_Y_Speed;
}

float Get_TiltX_Percentage(){

	int _Tilt_X_Value = Tilt_X_Invert*(analogRead(Tilt_Xpin)-Tilt_X_Calibrate);
	float _Tilt_X_OffSet =  (float)_Tilt_X_Value/(float)Tilt_X_MaxVariation;

	return _Tilt_X_OffSet;
}
