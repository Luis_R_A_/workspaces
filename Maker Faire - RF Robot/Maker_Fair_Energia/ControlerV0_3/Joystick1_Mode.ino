/*
 * Joystick_Mode.c
 *
 *  Created on: Sep 10, 2015
 *      Author: LuisRA
 */

#include "main.h"


void _Update_Screen_BaseSpeed();

int Y_Value, X_Value, SpeedAxis_Value;
float OffSet;
volatile int STOP = 0;

void Joystick1_Mode(){
        MAP_Interrupt_disableInterrupt(INT_PORT3|INT_PORT4|INT_PORT5);
        MAP_GPIO_registerInterrupt(GPIO_PORT_P4,Standby_ISR);
	MAP_Interrupt_enableInterrupt(INT_PORT4);
	Joystick1_Starting();
	while(1){
	  Joystick1_Interface();
	  radio.print(str_packet);
	  radio.flush();  // Force transmit (don't wait for any more data)

	  radio.deepsleep();

	  delay(40);
	}
}

void Joystick1_Interface(){

	if(STOP == 1){
		Joystick1_Standby();
	}

	/*
	 * Get ADC Values
	 */


	Base_Speed = Get_Pot_Speed(1);


	if(Base_Speed==0){
			RightMotor = 255;
			LeftMotor = 255;
	}
	else{


		OffSet = Get_JoyX_Percentage();
		if(OffSet > 0){
			RightMotor = Base_Speed * OffSet;
			LeftMotor = Base_Speed;
		}
		else{
			RightMotor = Base_Speed ;
			LeftMotor = Base_Speed  * (1-(OffSet-1))	;
		}
	}


	LoadBuffer(LeftMotor,RightMotor);

	update_counter++;
	if(update_counter > 3){
		_Update_Screen_BaseSpeed();
		update_counter = 0;
	}
}


void Joystick1_Standby(){
	RightMotor = 255;
	LeftMotor = 255;
	LoadBuffer(LeftMotor,RightMotor);
	for(int i=0; i <30; i++){
		radio.print(str_packet);
		radio.flush();  // Force transmit (don't wait for any more data)
	}
	radio.deepsleep();
	myScreen.clear();
	myScreen.setFont(0);
	myScreen.text(0, 0, "Stop button");
	myScreen.text(0, 1, "pressed!");
	myScreen.text(0, 3, "Wait!");
	delay(1000);
	STOP = 2;
	myScreen.setBacklight(0);
	myScreen.clear();
	myScreen.text(0, 0, "Standing by");
	myScreen.text(0, 2, "Press the");
	myScreen.text(0, 3, " button again");
	while(STOP == 2){
		    Base_Speed = Get_Pot_Speed(1);

			update_counter++;
			if(update_counter > 5){
				  myScreen.clear();
				  myScreen.text(0, 0, "Standing by");
				  myScreen.text(0, 2, "Press the");
				  myScreen.text(0, 3, " button again");
				  myScreen.text(0, 4, "Speed:");
					 char a[] = "+999";
					 if(Base_Speed <0){
						 Base_Speed = Base_Speed * (-1);
						 a[0] = '-';
					 }
					 else{
						 a[0] = '+';
					 }

					 for(int i=3; i >= 1; i--){
						a[i]= (Base_Speed%10)+48;
						Base_Speed = Base_Speed /10;
					 }
					 myScreen.text(0, 5, a);
				 update_counter = 0;
			}
			delay(50);

	  }
	  delay(500);
	  myScreen.setBacklight(1);
	  STOP = 0;
}

void _Update_Screen_BaseSpeed(){
	  myScreen.clear();

	 myScreen.setFont(0);
	 myScreen.text(0, 0, "Base speed:");


	 char a[] = "+999";
	 if(Base_Speed <0){
		 Base_Speed = Base_Speed * (-1);
		 a[0] = '-';
		 myScreen.text(0, 1, "Backwards");
	 }
	 else{
		 a[0] = '+';
		 myScreen.text(0, 1, "Forward");
	 }
	 myScreen.text(0, 5, "-120 to 120");
	 myScreen.setFont(1);
	 for(int i=3; i >= 1; i--){
		a[i]= (Base_Speed%10)+48;
		Base_Speed = Base_Speed /10;
	 }
	 myScreen.text(0, 3, a);
}

void Joystick1_Starting(){

	 int _temp = Get_Pot_Speed(1);
	 while(_temp != 0){
		 myScreen.setFont(0);
		 myScreen.text(0, 0, "Set to 0");
		 char a[] = "+999";
			 if(_temp <0){
				 _temp = _temp * (-1);
				 a[0] = '-';
			 }
			 else{
				 a[0] = '+';
			 }
			 myScreen.setFont(1);
			 for(int i=3; i >= 1; i--){
				a[i]= (_temp%10)+48;
				_temp = _temp /10;
			 }
			 myScreen.text(0, 3, a);
			 delay(100);
			 _temp = Get_Pot_Speed(1);
	 }
}
void Standby_ISR(){
          uint32_t status;
        
          status = MAP_GPIO_getEnabledInterruptStatus(GPIO_PORT_P4);
          MAP_GPIO_clearInterruptFlag(GPIO_PORT_P4, status);

	  if(STOP == 2){
		  STOP = 3;
	  }
	  else
		  STOP = 1;

}

