

#define PART_TM4C123GH6PM
#include <Enrf24.h>
#include <nRF24L01.h>
#include <string.h>
#include <SPI.h>
#include "stdlib.h"

#define AIN1 39//37
#define AIN2 40//38
#define BIN1 37//39
#define BIN2 38//40

void Motors_Init();
void MotorEsqSpeed(int Speed);
void MotorDirSpeed(int Speed);
void MotorsSpeed(int Vel_Esq, int Vel_Dir);

Enrf24 radio(PF_1, PE_3, PE_2); // PF_1 = CE, PE_3 = CSN, PE_2 = IRQ
const uint8_t rxaddr[] = { 
  0x17, 0x08, 0x07, 0xDE, 0x01 };

const char *str_on = "ON";
const char *str_off = "OFF";
unsigned char inbuf[33];

void dump_radio_status_to_serialport(uint8_t);



void SetMotors(){

  //if(sizeof(inbuf) !=5)
  //return;
  int LeftMotor_Speed =0;
  int RightMotor_Speed =0;
  int left_direction=1;
  int right_direction=1;
  if(inbuf[0] == 15){

    if(inbuf[1] == 3)
    {
      left_direction = inbuf[2];
      right_direction = inbuf[4];

      if(left_direction == 2)
        left_direction = -1;
      else if(left_direction != 1)
        return;


      if(right_direction == 2)
        right_direction = -1;
      else if(right_direction != 1)
        return;



      if(inbuf[3] == 255)
        LeftMotor_Speed=0;
      else
        LeftMotor_Speed = left_direction * (int)inbuf[3];

      if(inbuf[5] == 255)
        RightMotor_Speed=0;
      else
        RightMotor_Speed = right_direction * (int)inbuf[5];

      /*char buffer[7]; 
       itoa(inbuf[0], buffer,10);
       Serial.print(buffer);
       Serial.print(" ");
       itoa(inbuf[1], buffer,10);
       Serial.print(buffer);
       Serial.print(" ");
       Serial.print(left_direction);
       Serial.print(" ");
       Serial.print(right_direction);
       Serial.print(" ");
       Serial.print(LeftMotor_Speed);
       Serial.print(" ");
       Serial.println(RightMotor_Speed);*/

      MotorsSpeed(LeftMotor_Speed,RightMotor_Speed);
    }

  }

}

int temp=0;
void setup() {
  Serial.begin(9600);
  SPI.setModule(2);
  SPI.begin();
  SPI.setDataMode(SPI_MODE0);
  SPI.setBitOrder(MSBFIRST);

  radio.begin(1000000,005);  // Defaults 1Mbps, channel 0, max TX power
  dump_radio_status_to_serialport(radio.radioState());

  radio.setRXaddress((void*)rxaddr);

  //pinMode(PF_2, OUTPUT);
  //digitalWrite(PF_2, LOW);

  radio.enableRX();  // Start listening

  //SpeedControl_Init();
  Motors_Init();
  /* MotorsSpeed(30,-30);
   delay(2000);*/
  /*MotorsSpeed(30,30);
   delay(2000);
   MotorsSpeed(-30,-30);
   delay(2000);*/
  MotorsSpeed(0,0);
  //delay(2000);
  //while(1);
  /*digitalWrite(40,0);
   digitalWrite(39,0);
   digitalWrite(38,0);
   digitalWrite(37,0);*/
  //while(1);

}

//int total=0;
void loop() {


  //dump_radio_status_to_serialport(radio.radioState());  // Should show Receive Mode

  while (!radio.available(true) && (millis()-temp) < 150);
  // total = millis()-temp;
  if( (millis()-temp) > 150){
    //Serial.println("hi");
    MotorsSpeed(0,0);
    temp = millis();
  }
  else if (radio.read(inbuf)) {
    //Serial.print("Received packet: ");
    //Serial.println(inbuf[0]);

    SetMotors();
    /* if (!strcmp(inbuf, str_on))
     digitalWrite(PF_2, HIGH);
     if (!strcmp(inbuf, str_off))
     digitalWrite(PF_2, LOW);*/
    temp = millis();
  }
  radio.deepsleep();

  delay(2    0);

  radio.enableRX();


}

void dump_radio_status_to_serialport(uint8_t status)
{
  Serial.print("Enrf24 radio transceiver status: ");
  switch (status) {
  case ENRF24_STATE_NOTPRESENT:
    Serial.println("NO TRANSCEIVER PRESENT");
    break;

  case ENRF24_STATE_DEEPSLEEP:
    Serial.println("DEEP SLEEP <1uA power consumption");
    break;

  case ENRF24_STATE_IDLE:
    Serial.println("IDLE module powered up w/ oscillators running");
    break;

  case ENRF24_STATE_PTX:
    Serial.println("Actively Transmitting");
    break;

  case ENRF24_STATE_PRX:
    Serial.println("Receive Mode");
    break;

  default:
    Serial.println("UNKNOWN STATUS CODE");
  }
}

/*
  Chamem isto para configurar os pins.
 */
void Motors_Init() {
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
}

/*
  Muda a velocidade de só do motor da esquerda
 */
void MotorDirSpeed(int Speed) {
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  if (Speed == 0) {
    digitalWrite(AIN1, 1);
    digitalWrite(AIN2, 1);
    return;
  }
  bool forwards = 1;

  if (Speed < 0) {
    Speed = Speed * (-1);
    forwards = 0;
  }
  if(Speed > 255)
    Speed = 255;

  Speed = 255-Speed;

  if (forwards) {
    digitalWrite(AIN1, HIGH);
    analogWrite(AIN2, Speed);
  }
  else {
    analogWrite(AIN1, Speed);
    digitalWrite(AIN2, HIGH);  
  }
}

/*
  Muda a velocidade de só do motor da direita
 */
void MotorEsqSpeed(int Speed) {
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
  if (Speed == 0) {
    digitalWrite(BIN1, 1);
    digitalWrite(BIN2, 1);
    return;
  }
  bool forwards = 1;

  if (Speed < 0) {
    Speed = Speed * (-1);
    forwards = 0;
  }
  if(Speed > 255)
    Speed = 255;

  Speed = 255-Speed;
  if (forwards) {
    digitalWrite(BIN1, HIGH);
    analogWrite(BIN2, Speed);
  }
  else {
    analogWrite(BIN1, Speed);
    digitalWrite(BIN2, HIGH);  
  }
}

/*
  Muda a velocidade de ambos os motores
 */
void MotorsSpeed(int Vel_Esq, int Vel_Dir) {
  /*Serial.print(Vel_Esq);
  Serial.print(" ");
  Serial.println(Vel_Dir);*/
  MotorEsqSpeed(Vel_Esq);
  MotorDirSpeed(Vel_Dir);
}







