#include <Enrf24.h>
#include <nRF24L01.h>
#include <string.h>
#include <SPI.h>

#define NRF24_CE_PIN 30 //P5_5
#define NRF24_CSN_PIN 29 //P5_4
#define NRF24_IRQ_PIN 28 //P4_7
Enrf24 radio(NRF24_CE_PIN, NRF24_CSN_PIN, NRF24_IRQ_PIN);
const uint8_t txaddr[] = { 0x17, 0x08, 0x07, 0xDE, 0x01 };

char str_on[] = {15, 3, 2, 10, 1, 10, '\0' };
char str_off[] = {15, 3, 1, 255, 1, 255, '\0' };
//const char *str_on = "ON";
//const char *str_off = "OFF";

void dump_radio_status_to_serialport(uint8_t);

void setup() {
  //Serial.begin(9600);

//SPI.setModule(2);
  SPI.begin();
  SPI.setDataMode(SPI_MODE0);
  SPI.setBitOrder(MSBFIRST);

  radio.begin(1000000,05);  // Defaults 1Mbps, channel 0, max TX power
 // dump_radio_status_to_serialport(radio.radioState());

  radio.setTXaddress((void*)txaddr);
}

void loop() {
  //Serial.print("Sending packet: ");
 // Serial.println(str_on);
  radio.print(str_on);
  radio.flush();  // Force transmit (don't wait for any more data)
 // dump_radio_status_to_serialport(radio.radioState());  // Should report IDLE
  delay(10);

 /* Serial.print("Sending packet: ");
  Serial.println(str_off);
  radio.print(str_off);
  radio.flush();  //
  dump_radio_status_to_serialport(radio.radioState());  // Should report IDLE
  delay(10);*/
}

void dump_radio_status_to_serialport(uint8_t status)
{
  Serial.print("Enrf24 radio transceiver status: ");
  switch (status) {
    case ENRF24_STATE_NOTPRESENT:
      Serial.println("NO TRANSCEIVER PRESENT");
      break;

    case ENRF24_STATE_DEEPSLEEP:
      Serial.println("DEEP SLEEP <1uA power consumption");
      break;

    case ENRF24_STATE_IDLE:
      Serial.println("IDLE module powered up w/ oscillators running");
      break;

    case ENRF24_STATE_PTX:
      Serial.println("Actively Transmitting");
      break;

    case ENRF24_STATE_PRX:
      Serial.println("Receive Mode");
      break;

    default:
      Serial.println("UNKNOWN STATUS CODE");
  }
}
