/*
 * Readings.cpp
 *
 *  Created on: Sep 12, 2015
 *      Author: LuisRA
 */

#include "main.h"


int32_t Get_Pot_Speed(int _HalfScale, int value){
  int32_t SpeedAxis_Value = (int32_t)4095-(int32_t)analogRead(SpeedAxis_pin);
  int32_t _Base_Speed = 0;
  if(_HalfScale == 0){



    if(SpeedAxis_Value < 0+20){
      return _Base_Speed;
    }
    else{

      _Base_Speed = ((int32_t)SpeedAxis_Value)*(int32_t)MAXIMUM_SPEED/(int32_t)4095;
    }
  }
  else{
    int temp = ((float)(MAXIMUM_SPEED - value)/ (float)(MAXIMUM_SPEED*2)) * 2047;
    //temp = 4095-temp ;
    
    SpeedAxis_Value = (int32_t)SpeedAxis_Value -(int32_t)(temp);
    //Serial.println(temp);
    if(SpeedAxis_Value < 20 && SpeedAxis_Value > -20){
      return _Base_Speed;
    }
    else{
      if(SpeedAxis_Value > temp)
        _Base_Speed = ((int32_t)SpeedAxis_Value)*(int32_t)MAXIMUM_SPEED/(int32_t)(4095-temp);
      else{
        _Base_Speed = ((int32_t)-SpeedAxis_Value)*(int32_t)value/(int32_t)(temp);
      }
    }
  }
  return _Base_Speed;

}


float Get_JoyX_Percentage(){
  int _X_Value = analogRead(Xaxis_pin)  - 2047;
  float _OffSet = ( (_X_Value-X_Offset) / 2048.0);
  if(_OffSet > 1.0)
    _OffSet = 1.0;
  if(_OffSet < -1.0)
    _OffSet = -1.0;


    return _OffSet;
}

int32_t Get_JoyY_Speed(int _MaxSpeed){
  int _Y = analogRead(Yaxis_pin) -2047;

  int _Speed = 0;
  if(_Y < Y_Offset+20 && _Y > Y_Offset-20){
    return _Speed;
  }
  else{

    _Speed = ((int32_t)_Y-(int32_t)Y_Offset)*(int32_t)_MaxSpeed/(int32_t)2048;
  }
  return _Speed;
}

int32_t Get_Joy_Radius_Speed(int _MaxSpeed){

  int32_t Y_ = (int32_t)analogRead(Yaxis_pin) - 2047;

  int32_t X_ = (int32_t)analogRead(Xaxis_pin)  - 2047;

  int32_t _Radius = (int32_t)sqrtf((Y_*Y_) + (X_*X_));

  int32_t _Speed = 0;
  if(Y_ < Y_Offset+20 && Y_ > Y_Offset-20 && X_ < X_Offset+20 && X_ > X_Offset-20){
    return _Speed;
  }
  else{

    if(Y_ > 0)
      _Speed = (int32_t)(_Radius)*(int32_t)_MaxSpeed/(int32_t)2048;
    else
      _Speed = (int32_t)(-1)*(_Radius)*(int32_t)_MaxSpeed/(int32_t)2048;
  }

  return _Speed;
}

int32_t Get_Tilt_Speed(int _Tilt_MaxSpeed){

  int32_t _Tilt_Y_Speed = (int32_t)(analogRead(Tilt_Ypin) - (int32_t)Tilt_Y_Calibrate) *  (int32_t)_Tilt_MaxSpeed /  (int32_t)Tilt_Y_MaxVariation;
  return _Tilt_Y_Speed;
}

float Get_TiltX_Percentage(){

  int _Tilt_X_Value = (int32_t)(analogRead(Tilt_Xpin)-Tilt_X_Calibrate);
  float _Tilt_X_OffSet =  (float)_Tilt_X_Value/(float)Tilt_X_MaxVariation;

  if(_Tilt_X_OffSet > 1.0)
    _Tilt_X_OffSet = 1.0;
  if(_Tilt_X_OffSet < -1.0)
    _Tilt_X_OffSet = -1.0;
  return _Tilt_X_OffSet;
}

void Update_Screen_BaseSpeed(int _speed){
	 // myScreen.clear();

	  
	// myScreen.gText(0, 0, "Base speed:");


	 char a[] = "+999";
	 if(_speed <0){
		 _speed = _speed * (-1);
		 a[0] = '-';
		 //myScreen.gText(0, 1, "Backwards");
	 }
	 else{
		 a[0] = '+';
		// myScreen.gText(0, 1, "Forward");
	 }
	// myScreen.gText(0, 5, "-120 to 120");
	   
	 for(int i=3; i >= 1; i--){
		a[i]= (_speed%10)+48;
		_speed = _speed /10;
	 }
	 myScreen.gText(0, 50, a);
}
