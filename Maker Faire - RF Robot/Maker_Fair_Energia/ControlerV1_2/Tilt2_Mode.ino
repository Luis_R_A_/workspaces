/*
 * Tilt_Mode.c
 *
 *  Created on: Sep 10, 2015
 *      Author: LuisRA
 */


#include "main.h"


int Tilt_Y_Speed2 = 0;
float Tilt_X_OffSet2=0;
int  Tilt_X_Value2=0;



void Tilt2_Mode(){
  //MAP_Interrupt_disableInterrupt(INT_PORT3|INT_PORT4|INT_PORT5);
  detachInterrupt(Joystick_Button);
  myScreen.clear();
  while(1){
    Tilt2_Control();
    radio.print(str_packet);
    radio.flush();  // Force transmit (don't wait for any more data)

    radio.deepsleep();

    delay(40);
  }
}


void Tilt2_Control(){


  int Base_Speed = Get_Pot_Speed(0, 0);
  
  Tilt_Y_Speed2 = Get_JoyY_Speed(Base_Speed);


  if(Tilt_Y_Speed2 == 0){
    LeftMotor=255;
    RightMotor=255;
  }
  else{
    
    Tilt_X_OffSet2 =  Get_TiltX_Percentage();


    
    //If need to turn right
    if(Tilt_X_OffSet2 > 0.0){
      RightMotor = Tilt_Y_Speed2 * (1- Tilt_X_OffSet2) ;
      LeftMotor = Tilt_Y_Speed2;
    }
    //If need to turn left
    else{
      RightMotor = Tilt_Y_Speed2 ;
      LeftMotor = Tilt_Y_Speed2  * (1+Tilt_X_OffSet2)	;
    }
  }
  LoadBuffer(LeftMotor,RightMotor);

  update_counter++;
  if(update_counter > 5){
    Update_Screen_BaseSpeed(Base_Speed);
    update_counter = 0;
  }
}


