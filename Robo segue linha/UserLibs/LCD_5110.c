//
//  LCD_5110.cpp
//  Library C++ code
//  ----------------------------------
//  Developed with embedXcode
//
//  Project LCD 5110
//  Created by Rei VILO on 28/05/12
//  Copyright (c) 2012 http://embeddedcomputing.weebly.com
//  Licence CC = BY SA NC
//

#include "LCD_5110.h"

const uint8_t  _commandLCD = 0x00;     
const uint8_t  _dataLCD    = 0x01;          

// 2015-02-07 Rei Vilo
// Pins numbers instead of pins names
/*
LCD_5110() { // pins names in MSP430G2553
    LCD_5110(10, // P2_2 Chip Select
             12, // P2_4 Serial Clock
             8,  // P2_0 Serial Data
             11, // P2_3 Data/Command
             2,  // P1_0 Reset
             9,  // P2_1 Backlight
             PUSH2);   // Push Button 2
}
 */

void shiftOut(bool bitOrder,uint8_t val){
    uint8_t i;

    for (i = 0; i < 8; i++)  {
        if (bitOrder == 0)
        	GPIOPinWrite(GPIO_BASE,SERIAL_DATA, (!!(val & (1 << i))) ? SERIAL_DATA:0   );
           // digitalWrite(dataPin, !!(val & (1 << i)));
        else
        	GPIOPinWrite(GPIO_BASE,SERIAL_DATA, (!!(val & (1 << (7 - i)))) ? SERIAL_DATA:0   );
           // digitalWrite(dataPin, !!(val & (1 << (7 - i))));

        GPIOPinWrite(GPIO_BASE,SERIAL_CLOCK,SERIAL_CLOCK);
        GPIOPinWrite(GPIO_BASE,SERIAL_CLOCK,0);
        //digitalWrite(clockPin, HIGH);
       //digitalWrite(clockPin, LOW);
    }
}

void  write(uint8_t dataCommand, uint8_t c) {
	GPIOPinWrite(GPIO_BASE,DATA_COMMAND, (dataCommand)==_dataLCD ? DATA_COMMAND:0);
	GPIOPinWrite(GPIO_BASE,CHIP_SELECT,0);
	shiftOut(1, c);
	GPIOPinWrite(GPIO_BASE,CHIP_SELECT,CHIP_SELECT);

	/*
	digitalWrite(_pinDataCommand, dataCommand);
	digitalWrite(_pinChipSelect, LOW);

	digitalWrite(_pinChipSelect, HIGH);
	*/
}

void  setXY(uint8_t x, uint8_t y) {
	write(_commandLCD, 0x40 | y);
	write(_commandLCD, 0x80 | x);
}

void  begin() {


	SysCtlPeripheralEnable(GPIO_PERIPH);
	//GPIOPinTypeGPIOInput();
	GPIOPinTypeGPIOOutput(GPIO_BASE,CHIP_SELECT|RESET|DATA_COMMAND|
			SERIAL_DATA|SERIAL_CLOCK|BACKLIGHT);

	/*pinMode(_pinChipSelect, OUTPUT);
  pinMode(_pinReset, OUTPUT);
  pinMode(_pinDataCommand, OUTPUT);
  pinMode(_pinSerialData, OUTPUT);
  pinMode(_pinSerialClock, OUTPUT);
  pinMode(_pinBacklight, OUTPUT);
	 */
	//pinMode(_pinPushButton, INPUT_PULLUP);


	GPIOPinWrite(GPIO_BASE,DATA_COMMAND,0);
	//digitalWrite(_pinDataCommand, LOW);
	SysCtlDelay(800000);
	//delay(30);
	GPIOPinWrite(GPIO_BASE,RESET,0);
	//digitalWrite(_pinReset, LOW);
	SysCtlDelay(2666666);
	//delay(100); // as per 8.1 Initialisation
	GPIOPinWrite(GPIO_BASE,RESET,RESET);
	//digitalWrite(_pinReset, HIGH);

	write(_commandLCD, 0x21); // chip is active, horizontal addressing, use extended instruction set
	write(_commandLCD, 0xc8); // write VOP to register: 0xC8 for 3V — try other values
	write(_commandLCD, 0x12); // set Bias System 1:48
	write(_commandLCD, 0x20); // chip is active, horizontal addressing, use basic instruction set
	write(_commandLCD, 0x09); // temperature control
	write(_commandLCD, 0x0c); // normal mode
	SysCtlDelay(266666);
	//delay(10);

	clear();
	_font = 0;
	setBacklight(false);
}

void  WhoAmI(char a[], uint32_t size) {
	//return "LCD Nokia 5110";
}

void  clear() {
	setXY(0, 0);
	uint16_t i;
	for (i=0; i<6*84; i++) write(_dataLCD, 0x00);
	setXY(0, 0);
}

void  setBacklight(bool b) {
	GPIOPinWrite(GPIO_BASE,BACKLIGHT,b ? 0 : BACKLIGHT);
	//digitalWrite(_pinBacklight, b ? LOW : HIGH);
}

void  setFont(uint8_t font) {
	_font = font;
}

void  text(uint8_t x, uint8_t y,  char a[]) {
	uint8_t i;
	uint8_t j;
	uint32_t size = strlen(a);

	if (_font==0) {
		setXY(6*x, y);
		for (j=0; j<size; j++) {
			for (i=0; i<5; i++) write(_dataLCD, Terminal6x8[a[j]-' '][i]);
			write(_dataLCD, 0x00);
		}
	}
	else if (_font==1) {
		setXY(6*x, y);
		for (j=0; j<size; j++) {
			for (i=0; i<11; i++) write(_dataLCD, Terminal11x16[a[j]-' '][2*i]);
			write(_dataLCD, 0x00);
		}

		setXY(6*x, y+1);
		for (j=0; j<size; j++) {
			for (i=0; i<11; i++) write(_dataLCD, Terminal11x16[a[j]-' '][2*i+1]);
			write(_dataLCD, 0x00);
		}
	}
}

/*
bool  getButton() {
	if (digitalRead(_pinPushButton)==LOW) {
		while (digitalRead(_pinPushButton)==LOW); // debounce
		return true;
	} else {
		return false;
	}
}
*/
