/*
 * TimerCapture.h
 *
 *
 *	TimerCapture API.
 *	The purpose of this API is to facilitate the use of the Timer mode "Time Capture"
 *	As of now it only works with wide timers.
 *
 *	All wide timers support pulses with periods up to 53s at 80Mhz (more or less)
 *	The maximum period for the pulse is SystemFrequency/(2^32)
 *	It's only implemented methods to read positive or negative pulses. There are none to read a period.
 *
 *
 *  Created on: 28/05/2015
 *      Author: Lu�s Afonso
 */

#ifndef TEST_TCS3200_TIMERCAPTURE_H_
#define TEST_TCS3200_TIMERCAPTURE_H_


#include <stdint.h>
#include <stdbool.h>
#include "stdlib.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_uart.h"
#include "inc/hw_gpio.h"
#include "inc/hw_timer.h"
#include "inc/hw_types.h"

#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/udma.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"

#define POSITIVE_PULSE 1
#define NEGATIVE_PULSE 0
#define TIMER0 0
#define TIMER1 1
#define TIMER2 2
#define TIMER3 3
#define TIMER4 4
#define TIMER5 5
#define PC4 0
#define PC5 1
#define PC6 2
#define PC7 3
#define PD0 4
#define PD1 5
#define PD2 6
#define PD3 7
#define PD4 8
#define PD5 9
#define PD6 10
#define PD7 11


extern uint32_t WidePulseValue[12];
extern uint32_t WideFirstPulse[12];
extern uint32_t WideTimerPolarity[12];
extern uint32_t WideBusy[12];
extern uint32_t SystemFrequency;


bool WideTimerInit(uint32_t _pin, uint32_t _Mhz);
//bool WideTimerInit(uint8_t _Number, uint32_t _TimerHalf, uint32_t _Mhz);

uint32_t WideTimer_MeasureRaw_t(uint32_t _pin, uint8_t _Polarity,uint32_t _timeout);
uint32_t WideTimer_MeasureRaw(uint32_t _pin, uint8_t _Polarity);
uint32_t WideTimer_MeasureNanos_t(uint32_t _pin, uint8_t _Polarity,uint32_t _timeout);
uint32_t WideTimer_MeasureNanos(uint32_t _pin, uint8_t _Polarity);
uint32_t WideTimer_MeasureMicros_t(uint32_t _pin, uint8_t _Polarity,uint32_t _timeout);
uint32_t WideTimer_MeasureMicros(uint32_t _pin, uint8_t _Polarity);

bool WideTimer_StartReading(uint32_t _pin, uint8_t _Polarity);
uint32_t WideTimer_GetReading(uint32_t _pin);
uint32_t WideTimer_GetNanos(uint32_t _pin);
uint32_t WideTimer_GetMicros(uint32_t _pin);

bool WideTimer_IsBusy(uint32_t _pin);



/*
uint32_t WideTimer_GetReading(uint32_t _Number, uint32_t _TimerHalf, uint8_t _Polarity);
uint32_t WideTimer_GetNanos(uint32_t _Number, uint32_t _TimerHalf, uint8_t _Polarity);
uint32_t WideTimer_GetMicros(uint32_t _Number, uint32_t _TimerHalf, uint8_t _Polarity);
uint32_t WideTimer_GetMicros_t(uint32_t _Number, uint32_t _TimerHalf, uint8_t _Polarity,uint32_t _Timeout);
*/

void WideTimerHandler0();
void WideTimerHandler1();
void WideTimerHandler2();
void WideTimerHandler3();
void WideTimerHandler4();
void WideTimerHandler5();


#endif /* TEST_TCS3200_TIMERCAPTURE_H_ */
