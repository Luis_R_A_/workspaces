/*
 * ColorMath.c
 *
 *  Created on: 31/05/2015
 *      Author: Lu�s Afonso
 */


#include "ColorMath.h"

uint32_t WhiteBase[4];
uint32_t RGBW[4];
uint32_t NeutralLimit=10;
uint32_t Neutralradius=0;

void ColorMath_Calibrate(uint32_t M[4]){
	int i;
	for(i=0; i<4; i++){
		WhiteBase[i]=M[i];
	}
}

void ColorMath_ConvertRGB(uint32_t M[4],uint32_t _invert){

	if(_invert){
		int i;
		for(i=0;i<4;i++){
			M[i] = 255*WhiteBase[i]/M[i];
			if(M[i] > 255)
				M[i]=255;
		}
	}
	else{
		int i;
		for(i=0;i<4;i++){
			M[i] = 255*M[i]/WhiteBase[i];
			if(M[i] > 255)
				M[i]=255;
		}
	}
}

bool ColorMath_IsNeutral(uint32_t M[4]){
	uint32_t vR = (255*M[2]) - (255*M[1]);
	uint32_t vG = (255*M[0]) - (255*M[2]);
	uint32_t vB = (255*M[1]) - (255*M[0]);
	vR *=vR; vG*=vG; vB*=vB;
	uint32_t radius_temp = (vR +vG+vB)/ ( (255*255)*3 );
	Neutralradius = sqrtf(radius_temp);

	if(Neutralradius <= NeutralLimit)
		return 1;
	else
		return 0;

}

void ColorMath_SetNeutralLimit(uint32_t _value){
	NeutralLimit=_value;
}
uint32_t ColorMath_GetNeutralLimit(){
	return NeutralLimit;
}
