///
/// @file 	LCD_5110.h
/// @brief	Library for Nokia 5110 display
/// @details Ported to LM4F120 by Bryan Schremp (bajabug@gmail.com) 11/10/2012
/// @n		Uses GLCD fonts
/// @n		Push button 2 to turn backlight on / off
///
/// @a 		Developed with [embedXcode](http://embedXcode.weebly.com)
///
/// @author 	Rei VILO
/// @author 	http://embeddedcomputing.weebly.com
/// @date	Dec 13, 2012
/// @version	release 104
/// @n
/// @copyright 	© Rei VILO, 2010-2012
/// @copyright 	CC = BY NC SA
/// @n		http://embeddedcomputing.weebly.com
///
/// @see
/// *		Ported to LM4F120 by Bryan Schremp (bajabug@gmail.com) 11/10/2012
/// @n		http://forum.stellarisiti.com/topic/330-lcd-5110-lm4f120-sample-sketch-stellarpad-energia-branch/?p=1333
/// * 		Fonts generated with MikroElektronika GLCD Font Creator 1.2.0.0
/// @n		http://www.mikroe.com
///
// 2015-02-07 Rei Vilo
// Pins numbers instead of pins names


// Core library - IDE-based

#ifndef LCD_5110_h
#define LCD_5110_h

#include <stdint.h>
#include <stdbool.h>
#include "stdlib.h"
#include <cstring>

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_uart.h"
#include "inc/hw_gpio.h"
#include "inc/hw_timer.h"
#include "inc/hw_types.h"

#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/udma.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"

#include "Terminal6.h"
#include "Terminal12.h"


#define GPIO_PERIPH SYSCTL_PERIPH_GPIOA
#define GPIO_BASE GPIO_PORTA_BASE
#define CHIP_SELECT GPIO_PIN_5
#define RESET GPIO_PIN_6
#define DATA_COMMAND GPIO_PIN_7
#define SERIAL_DATA GPIO_PIN_2
#define SERIAL_CLOCK GPIO_PIN_3
#define BACKLIGHT GPIO_PIN_4

    //void LCD_5110();
    //void LCD_5110(uint8_t pinChipSelect, uint8_t pinSerialClock, uint8_t pinSerialData, uint8_t pinDataCommand, uint8_t pinReset, uint8_t pinBacklight, uint8_t pinPushButton);
    void begin();
    void WhoAmI(char a[], uint32_t size);
    void clear();
    void setBacklight(bool flag);
    void setFont(uint8_t font);
    uint8_t fontX();
    uint8_t fontY();
    void text(uint8_t x, uint8_t y, char a[]);
   // bool getButton();

    void setXY(uint8_t x, uint8_t y);
    void write(uint8_t dataCommand, uint8_t c);
    uint8_t _font;

    uint8_t _pinReset;
    uint8_t _pinSerialData;
    uint8_t _pinBacklight;
    uint8_t _pinChipSelect;
    uint8_t _pinDataCommand;
    uint8_t _pinSerialClock;
    uint8_t _pinPushButton;

#endif
