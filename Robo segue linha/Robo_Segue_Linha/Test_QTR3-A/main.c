/*
 *
 *	Connect the 555 output to PC5
 *
 * 	Lu�s Afonso
 *
 */


#include "main.h"

void ADCGetValue();
void ADCBegin();
void ADCSet(uint32_t channel);

int32_t speedA = 0, speedB=0;
int main(){

	/*
	 * Sets clock to 80Mhz and sets up the systick counter to count in milliseconds.
	 */
	Template2_Init();
	ADCBegin();


	while(1){

		ADCGetValue();
		Wait(250);
	}

}



#define Num_Sensor 3
uint32_t ADCValues[Num_Sensor];
uint32_t ADC_CH[Num_Sensor] = {ADC_CTL_CH2,ADC_CTL_CH1,ADC_CTL_CH0};

void ADCGetValue(){
	int i;
	for(i=0; i < Num_Sensor; i++){
	ADCSet(ADC_CH[i]);
    //
    // Trigger the ADC conversion.
    //
    ADCProcessorTrigger(ADC0_BASE, 3);

    //
    // Wait for conversion to be completed.
    //
    while(!ADCIntStatus(ADC0_BASE, 3, false))
    {
    }

    //
    // Clear the ADC interrupt flag.
    //
    ADCIntClear(ADC0_BASE, 3);

    //
    // Read ADC Value.
    //
    ADCSequenceDataGet(ADC0_BASE, 3, ADCValues+i);

    //
    // Display the temperature value on the console.
    //
   // UARTprintf(" %d ", ADCValues[i]);
	}
//	UARTprintf("\n");
}


void ADCBegin(){


	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	SysCtlDelay(3);
	GPIOPinTypeADC(GPIO_PORTE_BASE,GPIO_PIN_1);
	GPIOPinTypeADC(GPIO_PORTE_BASE,GPIO_PIN_2);
	GPIOPinTypeADC(GPIO_PORTE_BASE,GPIO_PIN_3);





    //
    // The ADC0 peripheral must be enabled for use.
    //
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    SysCtlDelay(3);

}

void ADCSet(uint32_t channel){




    //
    // Enable sample sequence 3 with a processor signal trigger.  Sequence 3
    // will do a single sample when the processor sends a singal to start the
    // conversion.  Each ADC module has 4 programmable sequences, sequence 0
    // to sequence 3.  This example is arbitrarily using sequence 3.
    //
    ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);

    //
    // Configure step 0 on sequence 3.  Sample the temperature sensor
    // (ADC_CTL_TS) and co50nfigure the interrupt flag (ADC_CTL_IE) to be set
    // when the sample is done.  Tell the ADC logic that this is the last
    // conversion on sequence 3 (ADC_CTL_END).  Sequence 3 has only one
    // programmable step.  Sequence 1 and 2 have 4 steps, and sequence 0 has
    // 8 programmable steps.  Since we are only doing a single conversion using
    // sequence 3 we will only configure step 0.  For more information on the
    // ADC sequences and steps, reference the datasheet.
    //
    ADCSequenceStepConfigure(ADC0_BASE, 3, 0, channel | ADC_CTL_IE |
                             ADC_CTL_END);

    //
    // Since sample sequence 3 is now configured, it must be enabled.
    //
    ADCSequenceEnable(ADC0_BASE, 3);

    //
    // Clear the interrupt status flag.  This is done to make sure the
    // interrupt flag is cleared before we sample.
    //
    ADCIntClear(ADC0_BASE, 3);


}
