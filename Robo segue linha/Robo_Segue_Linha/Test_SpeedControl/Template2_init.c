/*
 * SysTick_counter.c
 *
 * There is a ISR named "SycTickInt" in the startup file.
 *  Created on: 24/05/2015
 *      Author: Lu�s Afonso
 */

#include "main.h"

/*/
 *
 * This configures the system tick timer to interrupt every 1mS
 * It expects a CPU clock to be at 80Mhz. I don't use SysCtlClockGet to be
 * compatible with all CPU clocks because there is a bug currently with it.
 *
 * The math for the value is
 * CPU_CLOCK / ( 1 / time_unit )
 * time_unit is the time in secondsyou want to be counted, in this case
 * it's 1mS = 0,001.
 */
void _SysTickbegin(){
  SysTickPeriodSet(80000);
  //SysTickIntRegister(SycTickInt);
  IntEnable(INT_SYSCTL);
  SysTickIntEnable();
  SysTickEnable();
}

void Template2_Init(){
    SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);
    _SysTickbegin();
}
/*
 * Delay function.
 * Creates a delay milliseconds if you haven't changed the frequency that the systick interrupts.
 */
void Wait(uint32_t time){
    uint32_t temp = millis;
    while( (millis-temp) < time){
    }
}

/*
void InitConsole(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlDelay(3);
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    UARTStdioConfig(0, 115200, 16000000);
}
*/

