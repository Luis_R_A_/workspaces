/*
 * main.h
 *
 *  Created on: 24/05/2015
 *      Author: Lu�s Afonso
 */

#ifndef TEST_TCS3200
#define TEST_TCS3200_MAIN_H


#include <stdint.h>
#include <stdbool.h>
#include "stdlib.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_uart.h"
#include "inc/hw_gpio.h"
#include "inc/hw_timer.h"
#include "inc/hw_types.h"

#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/udma.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/adc.h"

#include "DRV8833.h"
#include "Speed_Control.h"
// Holds the code execution time in milliseconds. Overflows after about 49 days.
extern volatile uint32_t millis;


/*
 * Functions
 */

//Functions for Template2_init.c
void Template2_Init();
void Wait(uint32_t time);



#endif /* TEST_TCS3200_MAIN_H_ */
