/*
 *
 *	pins:
 *
 *	Line sensor left to right: PE_1, PE_2, PE_3
 *===========Motors===============
 * PB6 - AIN1
 * PB7 - AIN2
 *
 * PB4 - BIN1
 * PB5 - BIN2
 * This is to keep always!!
 *
 *
 *
 *================================
 *
 *
 *	Sonars:
 *	Trigger both = PD_6
 *	Sonar 1 echo = PC_4
 *	Sonar 2 echo = PC_5
 *
 * 	Lu�s Afonso
 *
 */


#include "main.h"

int32_t JoinValues();
void ADCGetValue();
void ADCBegin();
void ADCSet(uint32_t channel);

/* Low speed PID test
int32_t Base_speedA = 600, Base_speedB = 600;
float Kp = 1;
float Ki = 0.1;
float Kd = 2;

*/
int32_t Base_speedA = 650, Base_speedB = 650;
float Kp = 1.5;
float Ki = 0.1;
float Kd = 20;


int32_t Target = 1000;
float P = 0;
float I = 0;
float D = 0;
int32_t PID=0;
int32_t Error = 0;
int32_t LastError=0;
int32_t speedA=0,speedB=0;



#define Num_Sensor 3
uint32_t ADCValues[Num_Sensor];
uint32_t ADC_CH[Num_Sensor] = {ADC_CTL_CH2,ADC_CTL_CH1,ADC_CTL_CH0};




int main(){

	/*
	 * Sets clock to 80Mhz and sets up the systick counter to count in milliseconds.
	 */
	Template2_Init();

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
	HWREG(GPIO_PORTF_BASE + GPIO_O_CR)   |= 0x01;
	GPIOPinTypeGPIOInput(GPIO_PORTF_BASE,GPIO_PIN_4|GPIO_PIN_0);
	GPIOPadConfigSet(GPIO_PORTF_BASE,GPIO_PIN_4,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);
	GPIOPadConfigSet(GPIO_PORTF_BASE,GPIO_PIN_0,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);
	while(GPIOPinRead(GPIO_PORTF_BASE,GPIO_PIN_4)==GPIO_PIN_4);

	//if(GPIOPinRead(GPIO_PORTF_BASE,GPIO_PIN_0)!=0)
	//	Mode2();

	ADCBegin();
	Wait(2000);
	//SpeedControl_Init();
	DRV8833_InitMotorsAB();

	//DRV8833_MotorsDuty(100, 0);
	//while(1);

	Wait(1000);

	while(1){

		ADCGetValue();
		Error = Target - JoinValues();

		P = Kp * Error;
		I = Ki*(I+ Error);
		D = (Error-LastError)*Kd;
		LastError = Error;
		PID = P+I+D;
		speedA=Base_speedA+PID;
		speedB=Base_speedB-PID;
		DRV8833_MotorsDuty(speedA, speedB);
		//SpeedControl_Set(speedA,speedB);
		Wait(1);
	}

}


inline int32_t JoinValues(){

	int32_t sum =0;
	int32_t division = 0;
	int32_t i;
	for(i=0; i <Num_Sensor; i++ ){
		sum += (i*1000*ADCValues[i]);

		division += ADCValues[i];
	}

	return (sum/division);


}



inline void ADCGetValue(){
	int i;
	for(i=0; i < Num_Sensor; i++){
	ADCSet(ADC_CH[i]);
    //
    // Trigger the ADC conversion.
    //
    ADCProcessorTrigger(ADC0_BASE, 3);

    //
    // Wait for conversion to be completed.
    //
    while(!ADCIntStatus(ADC0_BASE, 3, false))
    {
    }

    //
    // Clear the ADC interrupt flag.
    //
    ADCIntClear(ADC0_BASE, 3);

    //
    // Read ADC Value.
    //
    ADCSequenceDataGet(ADC0_BASE, 3, ADCValues+i);

    //
    // Display the temperature value on the console.
    //
   // UARTprintf(" %d ", ADCValues[i]);
	}
//	UARTprintf("\n");
}


void ADCBegin(){


	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	SysCtlDelay(3);
	GPIOPinTypeADC(GPIO_PORTE_BASE,GPIO_PIN_1);
	GPIOPinTypeADC(GPIO_PORTE_BASE,GPIO_PIN_2);
	GPIOPinTypeADC(GPIO_PORTE_BASE,GPIO_PIN_3);





    //
    // The ADC0 peripheral must be enabled for use.
    //
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    SysCtlDelay(3);

}

void ADCSet(uint32_t channel){




    //
    // Enable sample sequence 3 with a processor signal trigger.  Sequence 3
    // will do a single sample when the processor sends a singal to start the
    // conversion.  Each ADC module has 4 programmable sequences, sequence 0
    // to sequence 3.  This example is arbitrarily using sequence 3.
    //
    ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);

    //
    // Configure step 0 on sequence 3.  Sample the temperature sensor
    // (ADC_CTL_TS) and co50nfigure the interrupt flag (ADC_CTL_IE) to be set
    // when the sample is done.  Tell the ADC logic that this is the last
    // conversion on sequence 3 (ADC_CTL_END).  Sequence 3 has only one
    // programmable step.  Sequence 1 and 2 have 4 steps, and sequence 0 has
    // 8 programmable steps.  Since we are only doing a single conversion using
    // sequence 3 we will only configure step 0.  For more information on the
    // ADC sequences and steps, reference the datasheet.
    //
    ADCSequenceStepConfigure(ADC0_BASE, 3, 0, channel | ADC_CTL_IE |
                             ADC_CTL_END);

    //
    // Since sample sequence 3 is now configured, it must be enabled.
    //
    ADCSequenceEnable(ADC0_BASE, 3);

    //
    // Clear the interrupt status flag.  This is done to make sure the
    // interrupt flag is cleared before we sample.
    //
    ADCIntClear(ADC0_BASE, 3);


}
