/*
 * Mode2.h
 *
 *  Created on: Mar 17, 2016
 *      Author: LuisRA
 */

#ifndef MODE2_H_
#define MODE2_H_

#include <stdint.h>
#include <stdbool.h>

#include "DRV8833.h"
#include "SRF04.h"
void Mode2();


#endif /* MODE2_H_ */
