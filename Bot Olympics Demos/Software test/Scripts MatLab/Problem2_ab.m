%{
Problem 2 a) and b) of Labwork 4

    This problem will show the required trajectory using the toolbox.
    If show_graphics is set then it will also show the graphics of the
    position, velocity and acceleration in real time (slower and less
    smoothly)
    
    - The 3 positions will be shown as well the join variables in the 
    console:
        - First, position A and waits button press
        - Second, position B and waits button press
        - Thrid, position C and waits button press to start the animation

Made by: Lu�s Rafael dos Santos Afonso
Student number: 2013135191
Year: 2017/2018
%}

clear all;
close all;

%{
    Set this variable to 1 to show position, velocity and acelaration in
    real time. Note that this will run much slower and with a iteration
    time increment of 0.5s.

    When set to 0 only the robot arm with show with a iteration time
    increment of 0.1s (so it's smoother and faster).
%}
show_graphics = 1;

%RR
syms Q1 Q2
% [theta, d, a, alpha, offset, type(revolution?)]
PJ_DH_sym = [Q1         ,  1,  0,  deg2rad(90), deg2rad(0), 1;
             Q2         ,  0,  1,  deg2rad(0), deg2rad(0), 1];
         
PJ_DH = eval(subs(PJ_DH_sym, [Q1 Q2], [0, 0]));
Kinematic_test = simplify(MGD_DH(PJ_DH_sym)); 
Robot = CreateRobot(PJ_DH);
if show_graphics == 1
    subplot(2,3,4:6);
end
Robot.plotopt = {'workspace',[-4,4,-4,4,-0,3]};
Robot.plot([0 0],'reach',2); % home position 



%coordinates
P_A = [0;sqrt(2)/2; (2-sqrt(2))/2];
P_B = [sqrt(2)/2;sqrt(2)/2; 1];
P_C = [sqrt(2)/2;0; (2-sqrt(2))/2];

Q_A = Problem2_inverse(P_A);
Q_B = Problem2_inverse(P_B);
Q_C = Problem2_inverse(P_C);
%eval(subs(Kinematic_test, [Q1 Q2], Q_A));
%eval(subs(Kinematic_test, [Q1 Q2], Q_B));
%eval(subs(Kinematic_test, [Q1 Q2], Q_C));


Robot.plot([Q_A],'reach',2); % home position 
disp('Position A, joint variables:');
disp('theta1 and theta2 (in radians)');
disp(Q_A);
disp('Press any button to continue');
pause;
Robot.plot([Q_B],'reach',2); % home position 
disp('Position B, joint variables:');
disp('theta1 and theta2 (in radians)');
disp(Q_B);
disp('Press any button to continue');
pause;
Robot.plot([Q_C],'reach',2); % home position 
disp('Position C, joint variables:');
disp('theta1 and theta2 (in radians) ');
disp(Q_C);
disp('Press any button to continue');
pause;



dt_total = 10;
t_AtoB = 4;
t_BtoC = 6;
t_atA = 0;
t_atB = t_atA+t_AtoB;
t_atC = t_atB+t_BtoC;

Q_v_A = [0 0];
Q_v_C= [0 0];

times = [t_atA, t_atB , t_atC];
Qs = [Q_A; Q_B; Q_C];
Q_v_B = GetIntermediateSpeeds(times, Qs);

coeficient_1 = GetCoeficients(t_atA, t_atB, Q_A, Q_B, Q_v_A, Q_v_B);
coeficient_2 = GetCoeficients(t_atB, t_atC, Q_B, Q_C, Q_v_B, Q_v_C);




if show_graphics == 1
    dt = 0.5;
else
   dt = 0.1; 
end

if show_graphics == 1
    points = 20; %number of points on the graph at all times
    data_period = dt*1000; %period of sampling in ms
    subplot(2,3,1);
    lh = Generate_RT_Plot(points, data_period, 2, ['theta1'; 'theta2'] );
    axis([-points*data_period, 0, -pi, pi]);
    subplot(2,3,2);
    lh2 = Generate_RT_Plot(points, data_period, 2, ['v theta1'; 'v theta2'] );
    axis([-points*data_period, 0, -1, 1]);
    subplot(2,3,3);
    lh3 = Generate_RT_Plot(points, data_period, 2, ['a theta1'; 'a theta2'] );
    axis([-points*data_period, 0, -1, 1]);
    
    Q = Q_A;
    Q_anterior = Q;
    v_Q_anterior = 0;
end


t_now = 0;
k = 1;
while t_now < t_atB
    t_now = k*dt;
    Q = Plan_pp_g3(coeficient_1, t_now, t_atA);

    if show_graphics == 1
        subplot(2,3,4:6);
    end

    Robot.plot(Q,'reach',2);

    if show_graphics == 1
        subplot(2,3,1);
        Update_RT_Plot(lh, Q); 
        subplot(2,3,2);
        v_Q = (Q-Q_anterior)/dt; 
        Update_RT_Plot(lh2, v_Q); 
        Q_anterior = Q;
        subplot(2,3,3);
        a_Q = (v_Q-v_Q_anterior)/dt; 
        Update_RT_Plot(lh3, a_Q); 
        v_Q_anterior = v_Q;
    end
    
    k = k+1;
end
while t_now < t_atC
    t_now = k*dt;
    Q = Plan_pp_g3(coeficient_2, t_now, t_atB);

    if show_graphics == 1
        subplot(2,3,4:6);
    end

    Robot.plot(Q,'reach',2);

    if show_graphics == 1
        subplot(2,3,1);
        Update_RT_Plot(lh, Q); 
        subplot(2,3,2);
        v_Q = (Q-Q_anterior)/dt; 
        Update_RT_Plot(lh2, v_Q); 
        Q_anterior = Q;
        subplot(2,3,3);
        a_Q = (v_Q-v_Q_anterior)/dt; 
        Update_RT_Plot(lh3, a_Q); 
        v_Q_anterior = v_Q;
    end
    
    k = k+1;
end