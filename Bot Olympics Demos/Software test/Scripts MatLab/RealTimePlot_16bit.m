close all;
clear all;

figure;
dt = 100; %in ms
data_period = 10000;%dt*1000; %period of sampling in ms
points = data_period/dt; %number of points on the graph at all times
lh = Generate_RT_Plot(points, data_period, 1, ['value 1'] );
axis([-points*data_period, 0, 0, 1023]);
pause(0.1);

%b.YData(:,1) = 10;
%   return;
% delete all serial ports from memory 
% important, if you the code is stopped without closing and deleting the
% used COM, you need to do this to open it again
delete(instrfindall);

% Init and open the serial port
s = serial('COM13', 'baudrate', 9600);
fopen(s);

start = 0;
key = 0;
while(start ~= 250) && ( strcmp(key, 's') == 0 ) 
     key = get(gcf,'CurrentKey');
     drawnow;
     if s.BytesAvailable > 0
         start = fread(s,1);
     end
end
if (start ~= 250)
    fclose(s); %close serial port
    delete(s); %remove serial port from memory
    return;
end

key = get(gcf,'CurrentKey'); %get the key currently pressed
%this "while" will stop if you press the "s" key
while ( strcmp(key, 's') == 0 ) 
    key = get(gcf,'CurrentKey'); %get the key currently pressed
    command = 0; size = 0;
    read = [0, 0, 0, 0];
    counter = 1;
    while counter <= 1 && strcmp(key, 's') == 0
        key = get(gcf,'CurrentKey'); %get the key currently pressed
        drawnow;
        if s.BytesAvailable > 1
            read(:,counter) = fread(s,1);
            read(:,counter) = read(:,counter) + (fread(s,1)*2^8);
            counter = counter+1;     
        end
    end
    
    Value = read(:,1)

    Update_RT_Plot(lh, Value); 
    



    drawnow;
 
end

fclose(s); %close serial port
delete(s); %remove serial port from memory