%{


Made by: Lu�s Rafael dos Santos Afonso
Student number: 2013135191
Year: 2017/2018
%}

function y = Update_RT_Plot(lh,y_new)

    s = size(lh,1);
    for i=1:s
        y = get(lh(i), 'YData');
        points = size(y,2);
        for k = 1:1:points-1
           y(k) = y(k+1);
        end       
        y(points) = y_new(i);
        set(lh(i), 'YData', y);
    end

    drawnow;