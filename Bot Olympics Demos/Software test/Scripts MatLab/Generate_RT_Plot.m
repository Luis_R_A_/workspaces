%{


Made by: Lu�s Rafael dos Santos Afonso
Student number: 2013135191
Year: 2017/2018
%}

function lh = Generate_RT_Plot(points, data_period, number_of_plots, names)


x = linspace(-points*data_period, 0, points);
axis([-points*data_period, 0, -0.5, 0.5]);
hold on;
for i = 1:number_of_plots
  y = linspace(0,0,points);
  lh(i,1) = plot(x,y,'DisplayName',names(i,:));
end
legend('show')



