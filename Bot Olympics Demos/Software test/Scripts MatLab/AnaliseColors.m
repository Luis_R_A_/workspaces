close all;
clear all;

figure;
b = bar([1,2,3, 4]);
b.FaceColor = 'flat';
b.CData(1,:) = [1 0 0];
b.CData(2,:) = [0 1 0];
b.CData(3,:) = [0 0 1];
b.CData(4,:) = [0.3 0.3 0.3];
y_max = 1024;
ylim([0 y_max])
pause(0.11);
%b.YData(:,1) = 10;
%   return;
% delete all serial ports from memory 
% important, if you the code is stopped without closing and deleting the
% used COM, you need to do this to open it again
delete(instrfindall);

% Init and open the serial port
s = serial('COM13', 'baudrate', 9600);
fopen(s);

start = 0;
key = 0;
while(start ~= 250) && ( strcmp(key, 's') == 0 ) 
     key = get(gcf,'CurrentKey');
     drawnow;
     if s.BytesAvailable > 0
         start = fread(s,1);
     end
end
if (start ~= 250)
    fclose(s); %close serial port
    delete(s); %remove serial port from memory
    return;
end

key = get(gcf,'CurrentKey'); %get the key currently pressed
%this "while" will stop if you press the "s" key
while ( strcmp(key, 's') == 0 ) 
    key = get(gcf,'CurrentKey'); %get the key currently pressed
    command = 0; size = 0;
    read = [0, 0, 0, 0];
    counter = 1;
    while counter <= 4 && strcmp(key, 's') == 0
        key = get(gcf,'CurrentKey'); %get the key currently pressed
        drawnow;
        if s.BytesAvailable > 1
            read(:,counter) = fread(s,1);
            read(:,counter) = read(:,counter) + (fread(s,1)*2^8);
            counter = counter+1;     
        end
    end
    
    Red =read(:,1)
    Green =read(:,2)
    Blue =read(:,3)
    Clear =read(:,4)
    Average = round((Red+Green+Blue)/3,0)
    
    Red_1 = (Red-Average)^2;
    Green_1 = (Green-Average)^2;
    Blue_1 = (Blue-Average)^2;
    
    Average_1 = round((Red_1+Green_1+Blue_1)/3,0);
    standard_dev = sqrt(Average_1)
    %dev = std([Red, Green, Blue])
    if standard_dev < 15
        disp('Neutral');
        disp('Neutral');
        disp('Neutral');
        disp('Neutral');
    elseif Red > Average && Red > Green && Red > Blue
        disp('Red');
    elseif Green > Average && Green > Red && Green > Blue
        disp('Green');
    elseif Blue > Average && Blue > Red && Blue > Green
        disp('Blue');
    else
        disp('Unknown');
    end
    
    b.YData = read;
    %b.YData(:,2) = 10;
    %b.YData(:,3) = 10;
    %b.YData(:,4) = 10;
    



    drawnow;
 
end

fclose(s); %close serial port
delete(s); %remove serial port from memory