//#define DEBUG
#define SONARS_ON


#define AIN1 5
#define AIN2 3
#define BIN1 9
#define BIN2 6


#define ECHO_PIN_LEFT 12
#define TRIG_PIN_LEFT 11

#define ECHO_PIN_FRONT 4
#define TRIG_PIN_FRONT 2

#define BUTTON_PIN 8

#define RED_LED_PIN 13

#define FAN_PIN A1
#define FAN_ON 0

#define Neutral_Variance 150
#define White_Limit 600
#define Black_Limit 400

void Motors_Init();
void MotorEsqSpeed(int Speed);
void MotorDirSpeed(int Speed);
void MotorsSpeed(int Vel_Esq, int Vel_Dir);


const float kp_crit = 4; //meh.
const float Pcrit = 4;
const float Ti = 0.5 * Pcrit;
const float Td = 0.125 * Pcrit;
const float P_limit = 255;
const float I_limit = 255;
const float D_limit = 255;
const float PID_limit = 255;
const int x = 7;
int maxDistance = 60;
//const float kp = 20, ki = 0, kd = 0;
//const float kp = 0.6 * kp_crit, ki = 1 / Ti, kd = Td;
const float kp = 0.8 * kp_crit, ki = 0, kd = Td; //kp = 0.6 * kp_crit, ki = 1 / Ti, kd = Td;
int error = 0;
int last_error = 0;
int target = 30;
int base_speed = 200;
float P, I, D;
int Last_Distance = 0;

const int n_samples = 3;
int samples [n_samples];
void setup() {
  // put your setup code here, to run once:

  pinMode(ECHO_PIN_LEFT, INPUT);
  pinMode(TRIG_PIN_LEFT, OUTPUT);
  pinMode(ECHO_PIN_FRONT, INPUT);
  pinMode(TRIG_PIN_FRONT, OUTPUT);

  pinMode(BUTTON_PIN, INPUT);
  pinMode(RED_LED_PIN, OUTPUT);
  digitalWrite(RED_LED_PIN, !FAN_ON);

  while (digitalRead(BUTTON_PIN) != 1);

  Motors_Init();
    //MotorsSpeed(125 , 0);
    //while(1);
  //ColorSensorBegin();

  for(int i = 0; i < n_samples; i++)
    samples[i] = 0; 
#ifdef DEBUG
  Serial1.begin(9600);
  Serial1.println("Starting");
  Serial1.print("kp = "); Serial1.print(kp); Serial1.print("ki = "); Serial1.print(ki); Serial1.print("kd = "); Serial1.println(kd);
#endif
  while (digitalRead(BUTTON_PIN) != 0);
  while (digitalRead(BUTTON_PIN) != 1);

  Serial.begin(9600);
}

int sample_c = 0;
void loop() {

  int temp = millis();

#ifdef SONARS_ON
  int distance_L = Get_Left_Distance();
  int distance_F = Get_Front_Distance();
  if (distance_L == 0)
    distance_L = maxDistance;
  if (distance_F == 0)
    distance_F = maxDistance;
#else
  int distance_L = maxDistance;
  int distance_F = maxDistance;
#endif


  uint16_t RGBC[4];
 // Get_Colors(RGBC);
  //Normalize(RGBC);

  //uint32_t variance = Variance(RGBC);
uint32_t variance = 500;
#ifdef DEBUG
  //Serial1.print("Variance: "); Serial1.print(variance); Serial1.print("; Clear: "); Serial1.println(RGBC[4]);
#endif

  if (variance < Neutral_Variance && RGBC[4] < Black_Limit && 1 == 5) {
    //digitalWrite(RED_LED_PIN,FAN_ON);
    MotorsSpeed(0, 0);
    delay(500);
  }
  else {
    //digitalWrite(RED_LED_PIN,!FAN_ON);
    //StateMachine_WallFollower_Left(distance_L, distance_F);
    //MotorsSpeed(50, 50);

    int32_t sum = 0;
    for(int i = 0; i< n_samples; i++){
      sum += samples[i];
    }
    sum += distance_L;
    int distance = sum/(n_samples+1);
    samples[sample_c] = distance;
    sample_c++;
    if(sample_c == n_samples)
      sample_c = 0;
    error = target - distance;
    //int distance = (distance_L + Last_Distance * x) / (x + 1);
    //Last_Distance = distance_L;

    P = error;
    if (P > P_limit)
      P = P_limit;
    else if (P < -1 * P_limit)
      P = -1 * P_limit;
    I += error;
    if (I > I_limit)
      I = I_limit;
    else if (I < -1 * I_limit)
      I = -1 * I_limit;
    int d_error = (error - last_error);
    D = d_error; last_error = error;
    if (D > D_limit)
      D = D_limit;
    else if (D < -1 * D_limit)
      D = -1 * D_limit;
    int PID = kp*P + ki*I + kd*D;
    if (PID > PID_limit)
      PID = PID_limit;
    else if (PID < -1 * PID_limit)
      PID = -1 * PID_limit;
    //Serial1.print(error);Serial1.print(" ");Serial1.println(PID);
    //Serial1.println(distance);
    //Serial1.print(" P: "); Serial1.print(P); Serial1.print(" I: "); Serial1.print(I); Serial1.print(" D: "); Serial1.println(D);
    //Serial1.print(distance_L); Serial1.print(" "); Serial1.print(P); Serial1.print(" "); Serial1.print(I); Serial1.print(" "); Serial1.println(D);
    int error_f = (30-distance_F);
    int teste = 0;
    if(error_f > 0)
      teste = error_f*20;

    int speed_esq = base_speed + PID + teste , speed_dir = base_speed - PID - teste;

    
#ifdef DEBUG
    Serial1.print(speed_esq);Serial1.print(" ");Serial1.println(speed_dir);
#endif
    MotorsSpeed(speed_esq, speed_dir);
      Serial.print(distance_L); Serial.print(" "); Serial.print(distance_F);Serial.print(" ");Serial.print(speed_esq); Serial.print(" "); Serial.println(speed_dir);
  }
  


  while (millis() - temp < 50);
}

int StateMachine_WallFollower_Left(int distance_L, int distance_F) {



  if (distance_F < 15) {
    MotorsSpeed(255, -255);
  }
  else if (distance_L < 30) {
    MotorsSpeed(255, 125);
  }
  else if (distance_L > 15) {
    MotorsSpeed(125, 255);
  }
  else {
    MotorsSpeed(255, 255);
  }

#ifdef DEBUG
  Serial1.println(distance_L);
#endif


}


int Get_Left_Distance() {

  digitalWrite(TRIG_PIN_LEFT, LOW);                   // Set the trigger pin to low for 2uS
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN_LEFT, HIGH);                  // Send a 10uS high to trigger ranging
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_LEFT, LOW);                   // Send pin low again
  int distance = pulseIn(ECHO_PIN_LEFT, HIGH, maxDistance * 58);      // Read in times pulse
  distance = distance / 58;                     // Calculate distance from time of pulse

  return distance;
}

int Get_Front_Distance() {

  digitalWrite(TRIG_PIN_FRONT, LOW);                   // Set the trigger pin to low for 2uS
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN_FRONT, HIGH);                  // Send a 10uS high to trigger ranging
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_FRONT, LOW);                   // Send pin low again
  int distance = pulseIn(ECHO_PIN_FRONT, HIGH, maxDistance * 58);      // Read in times pulse
  distance = distance / 58;                     // Calculate distance from time of pulse

  return distance;
}

#define SENTIDO_MOTOR1 -1
#define SENTIDO_MOTOR2 -1
/*
  Chamem isto para configurar os pins.
*/
void Motors_Init() {
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
}
/*
  Muda a velocidade de só do motor da esquerda
*/
void MotorDirSpeed(int Speed) {
  if (Speed == 0) {
    digitalWrite(AIN1, 1);
    digitalWrite(AIN2, 1);
  }
  bool forwards = 1;

  if (Speed < 0) {
    Speed = Speed * (-1);
    forwards = 0;
  }
  if (Speed > 255)
    Speed = 255;

  Speed = 255 - Speed;

  if (forwards) {
    digitalWrite(AIN1, HIGH);
    analogWrite(AIN2, Speed);
  }
  else {
    analogWrite(AIN1, Speed);
    digitalWrite(AIN2, HIGH);
  }
}

/*
  Muda a velocidade de só do motor da direita
*/
void MotorEsqSpeed(int Speed) {
  if (Speed == 0) {
    digitalWrite(BIN1, 1);
    digitalWrite(BIN2, 1);
  }
  bool forwards = 1;

  if (Speed < 0) {
    Speed = Speed * (-1);
    forwards = 0;
  }
  if (Speed > 255)
    Speed = 255;

  Speed = 255 - Speed;
  if (forwards) {
    digitalWrite(BIN1, HIGH);
    analogWrite(BIN2, Speed);
  }
  else {
    analogWrite(BIN1, Speed);
    digitalWrite(BIN2, HIGH);
  }
}

/*
  Muda a velocidade de ambos os motores
*/
void MotorsSpeed(int Vel_Esq, int Vel_Dir) {
  MotorEsqSpeed(SENTIDO_MOTOR1*Vel_Esq);
  MotorDirSpeed(SENTIDO_MOTOR2*Vel_Dir);
}

