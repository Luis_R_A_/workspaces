//#include "Adafruit_TCS34725.h"
//
//Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_2_4MS, TCS34725_GAIN_4X);
//
//
//uint16_t RGBCmax[] =  { 533, 319, 223, 865 };
//uint32_t scale = 1024; //for 2.4ms integration time
//
//void ColorSensorBegin(){
//
//    tcs.begin();
//}
//void Get_Colors(uint16_t RGBC[]) {
//  //uint16_t r, g, b, c, colorTemp, lux;
//
//  tcs.getRawData(&RGBC[0], &RGBC[1], &RGBC[2], &RGBC[3]);
//
//  //colorTemp = tcs.calculateColorTemperature(r, g, b);
//  //lux = tcs.calculateLux(r, g, b);
//
//  /*Serial.print("Color Temp: "); Serial.print(colorTemp, DEC); Serial.print(" K - ");
//    Serial.print("Lux: "); Serial.print(lux, DEC); Serial.print(" - ");
//    Serial.print("R: "); Serial.print(r, DEC); Serial.print(" ");
//    Serial.print("G: "); Serial.print(g, DEC); Serial.print(" ");
//    Serial.print("B: "); Serial.print(b, DEC); Serial.print(" ");
//    Serial.print("C: "); Serial.print(c, DEC); Serial.print(" ");
//    Serial.println(" ");*/
//}
//
//void Normalize(uint16_t RGBC[]) {
//  for (int i = 0; i < 4; i++) {
//    uint32_t temp = scale * RGBC[i];
//    RGBC[i] = temp / RGBCmax[i];
//  }
//
//}
//
//void SetNormalize(int times) {
//
//  uint16_t RGBC[4];
//
//
//  uint32_t values[4] = {0, 0, 0, 0};
//  for (int i = 0; i < times; i++) {
//    Get_Colors(RGBC);
//    for (int k = 0; k < 4; k++) {
//      values[k] += RGBC[k];
//    }
//  }
//
//
//  for (int k = 0; k < 4; k++) {
//    RGBCmax[k] = values[k]/times;
//  }
//
//  
//}
//
//
//uint16_t Variance(uint16_t RGBC[4]){
//
//  uint32_t sum  = RGBC[0] + RGBC[1] + RGBC[2];
//  uint16_t average = sum/3;
//
//  uint32_t distance = 0;
//  for(int i = 0; i <3; i++){
//      distance += (RGBC[i] - average) * (RGBC[i] - average);
//  }
//  uint32_t variance = distance/3;
//
//  return variance;
//
//}
//
//uint16_t StandardDeviation(uint16_t RGBC[4]){
//  return sqrt( Variance(RGBC) );
//}
//
//
//

