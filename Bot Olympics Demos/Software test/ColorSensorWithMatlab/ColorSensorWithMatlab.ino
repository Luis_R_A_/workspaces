/*
  //SDA->A4 SCL->A5 in Arduino leonardo
  #define SDA_PORT PORTF
  #define SDA_PIN 3 // = A4
  #define SCL_PORT PORTF
  #define SCL_PIN 2 // = A5
  #include <SoftWire.h>
  SoftWire Wire = SoftWire();
*/

#include "Adafruit_TCS34725.h"

/*
  TCS34725_INTEGRATIONTIME_2_4MS  = 0xFF,   <  2.4ms - 1 cycle    - Max Count: 1024
  TCS34725_INTEGRATIONTIME_24MS   = 0xF6,   <  24ms  - 10 cycles  - Max Count: 10240
  TCS34725_INTEGRATIONTIME_50MS   = 0xEB,   <  50ms  - 20 cycles  - Max Count: 20480
  TCS34725_INTEGRATIONTIME_101MS  = 0xD5,   <  101ms - 42 cycles  - Max Count: 43008
  TCS34725_INTEGRATIONTIME_154MS  = 0xC0,   <  154ms - 64 cycles  - Max Count: 65535
    = 0x00    <  700ms - 256 cycles - Max Count: 65535

  Gain 1x, 4x,16x 60x
*/
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_2_4MS, TCS34725_GAIN_4X);

#define BUTTON_PIN 8

uint16_t RGBCmax[] = { 533, 319, 223, 865 };
uint32_t scale = 1024;
void setup() {
  pinMode(BUTTON_PIN, INPUT);
  while (digitalRead(BUTTON_PIN) == 0);

  // put your setup code here, to run once:
  Serial.begin(9600);
  //Serial.println("Starting");
  //delay(1000);
  if (tcs.begin()) {
    //Serial.println("Found sensor");
  } else {
    Serial.println("No TCS34725 found ... check your connections");
    while (1);
  }

  Serial.write(250);
}

void loop() {
  // put your main code here, to run repeatedly:
  uint16_t RGBC[4];
  Get_Colors(RGBC);
  Normalize(RGBC);
  for (int i = 0; i < 4; i++) {
    Serial.write(RGBC[i] & 0xFF);
    Serial.write((RGBC[i] >> 8) & 0xFF);
  }

  delay(250);
}
void Get_Colors(uint16_t RGBC[]) {
  //uint16_t r, g, b, c, colorTemp, lux;

  tcs.getRawData(&RGBC[0], &RGBC[1], &RGBC[2], &RGBC[3]);

  //colorTemp = tcs.calculateColorTemperature(r, g, b);
  //lux = tcs.calculateLux(r, g, b);

  /*Serial.print("Color Temp: "); Serial.print(colorTemp, DEC); Serial.print(" K - ");
    Serial.print("Lux: "); Serial.print(lux, DEC); Serial.print(" - ");
    Serial.print("R: "); Serial.print(r, DEC); Serial.print(" ");
    Serial.print("G: "); Serial.print(g, DEC); Serial.print(" ");
    Serial.print("B: "); Serial.print(b, DEC); Serial.print(" ");
    Serial.print("C: "); Serial.print(c, DEC); Serial.print(" ");
    Serial.println(" ");*/
}

void Normalize(uint16_t RGBC[]) {
  for (int i = 0; i < 4; i++) {
    uint32_t temp = scale * RGBC[i];
    RGBC[i] = temp/RGBCmax[i];
  }

}

