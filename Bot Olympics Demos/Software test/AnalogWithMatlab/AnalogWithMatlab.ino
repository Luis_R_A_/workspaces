#define DEBUG_TO_MATLAB

#define ANALOG_PIN A1
#define ANALOG_PIN2 A4
#define ANALOG_PIN3 A5
#define BUTTON_PIN 8
void setup() {
  pinMode(BUTTON_PIN, INPUT);
  while (digitalRead(BUTTON_PIN) == 0);
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.write(250);
}

void loop() {
  // put your main code here, to run repeatedly:
  int value = analogRead(ANALOG_PIN);
  int value2 = analogRead(ANALOG_PIN2); 
  int value3 = analogRead(ANALOG_PIN3); 

  #ifdef DEBUG_TO_MATLAB
  Serial.write(value3 & 0xFF);
  Serial.write( (value3 >> 8) & 0xFF);
  #else
  Serial.print(value2);Serial.print(" ");Serial.print(value);Serial.print(" ");Serial.print(value2-value);Serial.print(" ");Serial.println(value3);
  //Serial.print(value2-value);Serial.print(" ");Serial.println(value3);
  //Serial.println(value3);
  #endif
  delay(100);
}
